# CCC-UI

A front-end UI for the Caterva control-center

## Development

`npm` is used to package manage this project. Use `npm install` to install all dependencies for development.

As server-side and client-side dependencies are used in this project. Client-side dependencies should be added
as "development" packages (`npm -i package --save-dev`), with server side packages installed as production
dependencies.

### Logging

Logs in the server side should be `log.debug` for anything uninteresting. Info should be logged on start up, with error for errors
and warn for things which are a bit weird.


### Running

A local mysql server is requried for developing the UI. The simpliest way to do this is setup a docker
`mariadb` service. See the stack configuration in `./db` for details on this.

Developing requires a connection to the backend microservices which the UI uses to retreive data.

1. `npm run tunnels` will open tunnel connections to the control-center test micro-services, to easily test local changes
1. `npm start` will build the client and start the server, by default the server log level is debug.

`npm run watch` can also be used to start the client in watch mode, which will automatically compile new changes.

Watch should be run alongside `npm run server`

## Deployment

`docker` is used to build and ship the UI. `swarmToDHUB` will handle this automatically.

## Technical

### Principals

- Stateless: The UI should not hold state. The UI uses external services such as a DB to hold user state, all other
data should be taken from other microservices. So far the UI communicates with `swarmdb`, `swarm-struc`, `fahrplan-api`, `prediction-api`

- Concurrent: User modifications on a page should be instantly reflected on ALL clients

- Secure: The UI should not allow unfiltered access to the backend services. The UI uses `data-access-manager` which
uses user roles (swarms) to allow access to data or to change data.

### Concurrency

In order to acheive concurrent of modifications, WebSockets are used to transmit information changes to all connected clients.
The socket broadcasts global site changes, but the client is desigened to only react to the events which are relevant to the
user.

As the UI needed to remain stateless and horizontally scalable, a single Redis instance is required to sit as a mediator
between all of the UI instances.

### Backend-API

The backend API of the UI is designed to act as a RESTFul API. However, it is NOT restful. Cookies are required on the client
to store the session, and the session is required to make requests to the API. 

The API reflects the user's privilages. Data is filtered for what they are allowed to see, and modification permissions
are enforeced serverside.

#### Style

The has been designed RESTFully. This means **NO** verbs in routes.

- You **MUST NOT**  use verbs such as get, set, put etc in the URL
- You **MUST** use the HTTP protocol verbs to carry out requests on the routes.

See the current routes for how to design an API restfully.

#### Responses

Generally, all API respond with `{"success": true}` for succesful transactions. 

`{"error": string}` is responded for any input errors, or 500 with the same body for server-side errors.

#### Auth

Auth has to only be performed once for a browser. The session for the site is stored
persistently.

Method | Path | Params | Body | Response |Description
--- | --- | --- | --- | --- | ---
POST | /auth | | `{"username": string, "password": string}` | `{success: true}` | Request login
PUT | /auth/password | | `{"old": string: "new": string }` | | Update the current users password
PUT | /auth/preferences | | `{"preferences": {"key": string}}` | | Update the preferences for this user
DELETE | /auth | | | | Remove this session, logout

#### Data

##### Swarms

A typical swarm data structure response

```
{  
   "RealSwarm":{  
      "info":{  
         "managed":true,
         "devices":[  
            "sn000002",
            "sn000004",
            ...
         ],
         "readonly":false,
         "operator":{  
            "id":12,
            "username":"oliver"
         },
         "operating":true
      },
      "data":{  
         "maxDis":[  
            [  
               1535552350347,
               2837601.68
            ]
         ],
         "maxChrg":[  
            [  
               1535552350347,
               2825085.21
            ]
         ],
         "optMaxChrg":[  
            [  
               1535552350347,
               2956489.76
            ]
         ],
         "soc":[  
            [  
               1535552350347,
               47.9034
            ]
         ],
         "maxSoc":[  
            [  
               1535552350347,
               64.8
            ]
         ],
         "minSoc":[  
            [  
               1535552350347,
               35.2
            ]
         ],
         "temp":[  
            [  
               1535552350347,
               32.3366
            ]
         ],
         "pv":[  
            [  
               1535552350347,
               515868.623
            ]
         ],
         "hhCons":[  
            [  
               1535552350347,
               97043.0252
            ]
         ],
         "grid":[  
            [  
               1535552350347,
               -24891.916
            ]
         ],
         "calcFcr":[  
            [  
               1535552350347,
               2258.762
            ]
         ],
         "mesFcr":[  
            [  
               1535552350347,
               4652.0865
            ]
         ]
      },
      "prediction":{  
         "soc":[  

         ],
         "maxPredSoc":[  

         ],
         "minPredSoc":[  

         ],
         "pv":[  

         ],
         "hhCons":[  

         ],
         "maxDis":[  

         ],
         "maxChrg":[  

         ],
         "optMaxChrg":[  

         ],
         "mesFcr":[  

         ]
      },
      "fahrplan":[  

      ]
   }
}
```

This response can be more specific with from and to value

Method | Path | Params | Body | Response | Description
--- | --- | --- | --- | --- | ---
GET | /data/swarms | | | `{"swarms": {"swarmId": {...}}}` | Get a list of this user swarms
GET | /data/swarms/{swarmId} | `{from?: epoch, to?: epoch}` | | `{"swarmId": {...}}` | Return data about a specific swarm, with from and to values being used to get data between a time range
GET | /data/swarms/{swarmId}/fahrplan | `{from?: epoch, to?: epoch}` | | `{"fahrplan": [...]}` | A list of fahrplan events in the specified time range
GET | /data/swarm/{swarmId}/fahrplan/{eventId} | | | `{"eventId": {...}}` | Retreive the given fahrplan event
PATCH | /data/swarm/{swarmId}/fahrplan/{eventId} | | `{...}` | `{...}` | Update the given fahrplan event (Approve/Disapprove). Please see fahrplan API as this request makes a direct request to that API 
DELETE | /data/swarm/{swarmId}/fahrplan/{eventId} | | | `{...}` | Cancel the given fahrplan event. Please see fahrplan API as this endpoint makes a direct request to that API
POST | /data/swarms/{swarmId}/fahrplan | | `{...}` | `{...}` | For requests and responses, please see fahrplan API. This endpoint makes a direct request to the fahrplan API. The API will return a 403 unauthorised if the user is not the swarm operator. Trade requests are denied.
PUT | /data/swarms/{swarmId}/operating | | any | {success: true} | Specify that you are now the swarm operator, can only be done if this swarm is not readonly
POST | /data/swarms/ | | `{"managed": boolean, "name": string }` | 422: (swarm already exists) 200: `{success: true}` | Creates a swarm with the given perameters. Subswarms are created with "." in the names. Requires admin.
POST | /data/swarms/{swarmId}/devices | | `{device: string}` | | Put the given device in the given given managed swarm. This transfers the device from it's previous swarm
PUT | /data/swarms/{swarmId}/devices | | `{devices: string[]}` | | Set the given list of devices in the given swarm view
DELETE | /data/swarms/{swarmId} | | | `{ success: true }` | | Delete the given subswarm or swarmview. Does not work if the subswarm has managed devices


##### Devices

TODO: (Peter)

##### Alarms

TODO: (Peter)

##### Users

TODO: (Peter)



## Credits

A real net buster by Eddie Reader, Oliver Bell, Povilas Vilutis, and Peter Baker.

Copyright Alelion Energy Systems GmbH 2018
