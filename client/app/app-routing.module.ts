import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DevicesComponent } from './home/main/devices/devices.component';
import { EnergyManagementComponent } from './home/main/energy-management/energy-management.component';
import { OverviewComponent } from './home/main/overview/overview.component';

import { AuthGuard } from './shared/guards/auth/auth.guard';
import { SwarmGuard } from './shared/guards/swarm/swarm.guard';
import { ManagedGuard } from './shared/guards/managed/managed.guard';
import { ErrorPageComponent } from './error-page/error-page.component';

const appRoutes: Routes = [
  { path: 'error', component: ErrorPageComponent, canActivate: [AuthGuard, SwarmGuard] },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: ':swarm', component: HomeComponent, canActivate: [AuthGuard, SwarmGuard],
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'overview'},
      { path: 'overview', component: OverviewComponent },
      { path: 'devices', component: DevicesComponent },
      { path: 'energy-management', component: EnergyManagementComponent, canActivate: [ManagedGuard] },
      { path: '**', redirectTo: 'overview' }
    ]
  },
  { path: '**', redirectTo: '!' }
]


@NgModule({
  imports: [ RouterModule.forRoot(appRoutes, { enableTracing: false }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
