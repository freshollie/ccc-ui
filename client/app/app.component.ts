import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  // Has the app first loaded
  loaded = false;

  constructor(private router: Router) {
    router.events.pipe(
      filter((e) => e instanceof NavigationEnd), 
      take(1)
    ).subscribe(() => {
        this.loaded = true;
    });
  }
}
