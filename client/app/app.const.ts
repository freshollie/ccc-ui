export const HTTP_TIMEOUT = 5000;

// Refresh data every 20 seconds
export const DATA_REFRESH_RATE = 20000;

// Refresh alarms every 10 seconds
export const ALARM_REFRESH_RATE = 10000;
