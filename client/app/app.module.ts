import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';

import { UiSwitchModule } from 'ngx-toggle-switch';
import { ParticlesModule } from 'angular-particle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { TreeModule } from 'ng2-tree';

import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './shared/services/auth/auth.service';
import { AuthGuard } from './shared/guards/auth/auth.guard';
import { AlarmService } from './shared/services/alarm/alarm.service';
import { SearchService } from './shared/services/search/search.service';
import { SwarmGuard } from './shared/guards/swarm/swarm.guard';
import { TimeService } from './shared/services/time/time.service';
import { SwarmService } from './shared/services/swarm/swarm.service';
import { DeviceService } from './shared/services/device/device.service';
import { EnergyService } from './shared/services/energy/energy.service';
import { FormatFahrplanPipe } from './shared/pipes/format-fahrplan/format-fahrplan.pipe';
import { ManagedGuard } from './shared/guards/managed/managed.guard';
import { EventListenerService } from './shared/services/events/event-listener.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './home/sidebar/sidebar.component';
import { TopbarComponent } from './home/topbar/topbar.component';
import { MainComponent } from './home/main/main.component';
import { OverviewComponent } from './home/main/overview/overview.component';
import { DevicesComponent } from './home/main/devices/devices.component';
import { DeviceInfoComponent } from './home/main/devices/device-list/device-info/device-info.component';

import { EnergyManagementComponent } from './home/main/energy-management/energy-management.component';
import { SocLineChartComponent } from './home/main/overview/soc-line-chart/soc-line-chart.component';
import { PowerLineChartComponent } from './home/main/overview/power-line-chart/power-line-chart.component';
import { DeviceListComponent } from './home/main/devices/device-list/device-list.component';
import { SocBarChartComponent } from './home/main/overview/soc-bar-chart/soc-bar-chart.component';
import { DevicePowerLineChartComponent } from './home/main/devices/device-list/device-info/device-power-line-chart/device-power-line-chart.component';
import { DeviceSocLineChartComponent } from './home/main/devices/device-list/device-info/device-soc-line-chart/device-soc-line-chart.component';
import { DeviceTempLineChartComponent } from './home/main/devices/device-list/device-info/device-temp-line-chart/device-temp-line-chart.component';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/timeout'
import { ConfirmationDialogComponent } from './shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { DevicePreferencesDialogComponent } from './shared/dialogs/device-preferences-dialog/device-preferences-dialog.component';
import { EventConfirmDialogComponent } from './shared/dialogs/event-confirm-dialog/event-confirm-dialog.component';
import { AlarmListComponent } from './shared/components/alarm-list/alarm-list.component';
import { UserManagementDialogComponent } from './shared/dialogs/user-management-dialog/user-management-dialog.component';
import { UserManagementService } from './shared/services/user-management/user-management.service';
import { ChartComponent } from './shared/components/chart/chart.component';
import { SwarmHealthBarComponent } from './shared/components/swarm-health-bar/swarm-health-bar.component';
import { SwarmManagementDialogComponent } from './shared/dialogs/swarm-management-dialog/swarm-management-dialog.component';
import { TempBarChartComponent } from './home/main/overview/temp-bar-chart/temp-bar-chart.component';
import { ToastrModule } from 'ngx-toastr'
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { ErrorPageComponent } from './error-page/error-page.component';
import { DeviceChartComponent } from './home/main/devices/device-list/device-info/device-chart/device-chart.component';
import { TableModule } from 'primeng/table';  //TEST

declare var require: any;


export function highchartsFactory() {
  const Highcharts = require('highcharts/highstock');
  const dd = require('highcharts/highcharts-more');
  Highcharts.setOptions({
    global: {
      useUTC: false
    }
  });
  dd(Highcharts);
  return Highcharts;
}

// Make owl use 12 hour formats
const CUSTOM_OWL_DATETIME_FORMATS = {
  fullPickerInput: { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: false },
  datePickerInput: { year: 'numeric', month: 'numeric', day: 'numeric', hour12: false },
  timePickerInput: { hour: 'numeric', minute: 'numeric', hour12: false },
  monthYearLabel: { year: 'numeric', month: 'short' },
  dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
  monthYearA11yLabel: { year: 'numeric', month: 'long' },
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SidebarComponent,
    TopbarComponent,
    MainComponent,
    OverviewComponent,
    DevicesComponent,
    EnergyManagementComponent,
    SocLineChartComponent,
    PowerLineChartComponent,
    DeviceListComponent,
    SocBarChartComponent,
    DevicePowerLineChartComponent,
    DeviceSocLineChartComponent,
    DeviceTempLineChartComponent,
    FormatFahrplanPipe,
    ConfirmationDialogComponent,
    DevicePreferencesDialogComponent,
    EventConfirmDialogComponent,
    DeviceInfoComponent,
    AlarmListComponent,
    UserManagementDialogComponent,
    ChartComponent,
    SwarmHealthBarComponent,
    SwarmManagementDialogComponent,
    TempBarChartComponent,
    ErrorPageComponent,
    DeviceChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ChartModule,
    UiSwitchModule,
    ParticlesModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatTooltipModule,
    MatButtonModule,
    MatCheckboxModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TreeModule,
    TableModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      progressBar: true,
      positionClass: 'toast-bottom-center',
      maxOpened: 3,
      autoDismiss: true,
      preventDuplicates: true
    }),
    SelectDropDownModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    HomeComponent,
    AlarmService,
    SearchService,
    SwarmGuard,
    TimeService,
    SwarmService,
    DeviceService,
    EnergyService,
    ManagedGuard,
    EventListenerService,
    UserManagementService,
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: CUSTOM_OWL_DATETIME_FORMATS },
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-GB' },
  ],
  entryComponents: [
    ConfirmationDialogComponent, 
    EventConfirmDialogComponent,
    UserManagementDialogComponent,
    DevicePreferencesDialogComponent,
    SwarmManagementDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
