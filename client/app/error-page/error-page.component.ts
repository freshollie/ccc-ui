import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  public static AUTH_ERROR_KEY = "autherr";
  public static DATA_ERROR_KEY = "dataerr";
  
  public message = "Something went wrong"
  private previousPage: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.previousPage = params["target"];
      if (params["bad"]) {
        this.message = "Bad access rights"
      } else if (params["disconnected"] == true) {
        this.message = "Connection to server lost"
      }
    });
  }

  tryAgain() {
    this.router.navigate([this.previousPage || "/"]);
  }
}
