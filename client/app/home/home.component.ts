import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { ISubscription } from 'rxjs/Subscription';
import { AlarmService } from '../shared/services/alarm/alarm.service';
import { DeviceService } from '../shared/services/device/device.service';
import { SwarmService } from '../shared/services/swarm/swarm.service';
import { TimeService } from '../shared/services/time/time.service';
import { EventListenerService } from '../shared/services/events/event-listener.service';
import { DATA_REFRESH_RATE, ALARM_REFRESH_RATE } from '../app.const';
import { AuthService } from '../shared/services/auth/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  public sidebarIsOpen = false;

  public currentSwarm = "";
  public swarmTrees: Object[];
  public adminUser = false;
  
  private swarmSubscription: ISubscription;
  private minutelySubscription: ISubscription;
  private alarmRefreshSubscription: ISubscription;
  private currentUserSubscription: ISubscription;

  constructor(
    private deviceService: DeviceService,
    private swarmService: SwarmService,
    private timeService: TimeService,
    private alarmService: AlarmService,
    private eventListenerService: EventListenerService,
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.currentUserSubscription = this.authService.currentUserDetails
      .subscribe((auth) => {
        this.adminUser = auth.roles.hasOwnProperty(AuthService.ROLE_TYPE_ADMIN)
      });

    // Subscribe to just change in selected swarm
    this.swarmSubscription = this.swarmService.currentSwarmName.subscribe(swarmName => {
      // Save swarm name
      this.currentSwarm = swarmName;
      // Re-fetch swarm info
      this.swarmService.doSwarmInfoFetch();
      // Re-fetch latest swarm values
      this.swarmService.doLatestSwarmDataFetch(swarmName);
      // Re-fetch latest device values
      this.deviceService.doLatestDeviceDataFetch(swarmName);
      // Re-fetch alarms
      this.alarmService.doAlarmsFetch();

      this.fetchSwarmTrees();
    });

    // Subscribe to an interval of 1 minute
    this.minutelySubscription = Observable.interval(DATA_REFRESH_RATE).subscribe(() => {
      // If watching current values, update to latest time.
      if (this.timeService.isTsCurrent()) {
        //console.log('updating ts start to now');
        this.timeService.updateToCurrent();
      } else {
        // Emit a timestamp update every minute
        this.timeService.emitTime();
      }

      // Re-fetch latest swarm info 
      this.swarmService.doSwarmInfoFetch();

      // Re-fetch latest swarm values
      this.swarmService.doLatestSwarmDataFetch(this.currentSwarm);
      // Re-fetch latest device values
      this.deviceService.doLatestDeviceDataFetch(this.currentSwarm);
    });

    // Subscribe to an interval of 10 seconds
    this.alarmRefreshSubscription = Observable.interval(ALARM_REFRESH_RATE).subscribe(() => {
      // Re-fetch alarms
      this.alarmService.doAlarmsFetch();
    });

    this.eventListenerService.listen();
  }

  ngOnDestroy() {
    this.swarmSubscription.unsubscribe();
    this.minutelySubscription.unsubscribe();
    this.alarmRefreshSubscription.unsubscribe();
    this.currentUserSubscription.unsubscribe();

    this.eventListenerService.close();
  }

  private fetchSwarmTrees() {
    this.swarmService.getSwarmTrees()
      .subscribe((trees) => {
        this.swarmTrees = trees;
      });
  }

  toggleSidebar() {
    this.sidebarIsOpen = !this.sidebarIsOpen;
  }

  closeSidebar() {
    this.sidebarIsOpen = false;
  }
}
