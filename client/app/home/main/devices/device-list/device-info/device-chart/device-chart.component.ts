import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { DeviceDataRange } from '../../../../../../shared/services/device/device.service';
import { ChartComponent } from '../../../../../../shared/components/chart/chart.component';

@Component({
  selector: 'app-device-chart',
  templateUrl: './device-chart.component.html',
  styleUrls: ['./device-chart.component.css']
})
export class DeviceChartComponent extends ChartComponent implements OnInit, OnChanges, OnDestroy {
  @Input() deviceID: string;
  @Input() deviceData: DeviceDataRange;

  @Input() zoom;

  private static STATE_PLOT = 'state-area';
  
  private chart: any;
  public chartVisible = false;

  private requiredData: string[];
  private dataColours: Object;
  private chartName: string;

  private chartOptions = {
    plotOptions: {
      line: {
        animation: false,
        states: {
          hover: {
            enabled: false
          }
        },
        marker: {
          enabled: false,
        }
      }
    },
    series: [],
    xAxis: {
      type: 'datetime',
      plotLines: [],
      plotBands: [],
      events: {
        setExtremes: (e) => {
          this.handleExtremesChanged(e, this.chart, this.deviceData.from, this.deviceData.to);
        }
      },
    },
    yAxis: {
      title: {
        text: ''
      },
      gridLineColor: "rgba(0, 0, 0, 0.2)",
      tickInterval: 5000
    },
    chart: {
      renderTo: '',
      responsive: true,
      zoomType: 'x',
      animation: false
    },
    tooltip: {
      enabled: false
    },
    title: {
      text: ''
    },
    legend: {
      floating: true,
      y: -20,
    }
  };

  constructor() { 
    super();
  }
  
  setup(
    requiredData: string[], 
    dataColours: Object,
    chartName: string,
    additionalOptions: Object
   ) {
    this.requiredData = requiredData;
    this.dataColours = dataColours;
    this.chartName = chartName;

    // Merge the chart options with the base chart options
    this.chartOptions = Object.assign(this.chartOptions, additionalOptions);
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.zoom && this.deviceData != null) {
      this.handleNewZoom(this.chart, this.zoom, this.deviceData.from, this.deviceData.to);
      return;
    } 


    if (this.deviceData == undefined || Object.keys(this.deviceData.data).length < 0) {
      return;
    }

    // If chart not created, initialise
    if (!this.chartVisible) {
      this.initialiseChart();
    // Otherwise, update chart
    } else {
      this.onNewData();
    }
  }

  ngOnDestroy() {
    // Destroy chart
    if (this.chart != undefined) {
      this.chart.destroy();
    }
  }

  /**
   * Initialise the chart by adding the initial
   * data to the chart options and allowing the
   * chart to be drawn
   */
  private initialiseChart() {
    // Set date ranges on x axis
    this.chartOptions.xAxis['min'] = this.deviceData.from;
    this.chartOptions.xAxis['max'] = this.deviceData.to;

    // Assign container to renderTo
    this.chartOptions.chart.renderTo = this.deviceID + "-" + this.chartName;

    this.initialiseFuturePlots(this.chartOptions, this.deviceData.from, this.deviceData.to);
    
    let stateBands = this.getStateBands(this.deviceData.data.state);

    // Mark blocks with status errors.
    for (const band of stateBands) {
      this.chartOptions.xAxis.plotBands.push({
        id: DeviceChartComponent.STATE_PLOT,
        color: ChartComponent.STATE_TO_COLOR[band[2]],
        from: band[0],
        to: band[1],
      }); 
    }


    // Push all starting data to options
    for (let key of this.requiredData) {
      this.chartOptions.series.push({
        id: key,
        name: key,
        color: this.dataColours[key],
        data: this.formatGaps(this.deviceData.data[key])
      });
    }

    // Create chart
    this.chartVisible = true;
  }

  private onNewData() {
    this.redrawFuturePlots(this.chart, this.deviceData.from, this.deviceData.to);

    const stateBands = this.getStateBands(this.deviceData.data.state);

    this.chart.xAxis[0].removePlotBand(DeviceChartComponent.STATE_PLOT);
    for (const band of stateBands) {
      this.chart.xAxis[0].addPlotBand({
        id: DeviceChartComponent.STATE_PLOT,
        color: ChartComponent.STATE_TO_COLOR[band[2]],
        from: band[0],
        to: band[1],
      }); 
    }

    // Update the chart data
    for (let key of this.requiredData) {
      const series = this.chart.get(key);
      // Set data of series to new data array without redraw
      series.setData(this.formatGaps(this.deviceData.data[key]), false);
    }

    if (!this.isZoomed()) {
      // Update the date ranges on x axis
      this.chart.xAxis[0].setExtremes(this.deviceData.from, this.deviceData.to);
    } else {
      this.chart.redraw();
    }
  }

  public onGraphLoad(chartInstance): void {
    // Save original instance of chart
    this.chart = chartInstance;
  }
}