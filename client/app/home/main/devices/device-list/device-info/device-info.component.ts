import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Device, DeviceService, DeviceData, DeviceDataRange } from '../../../../../shared/services/device/device.service';
import { AlarmService, Alarm, AlarmPrioritory } from '../../../../../shared/services/alarm/alarm.service';
import { ISubscription } from 'rxjs/Subscription';
import { TimeService } from '../../../../../shared/services/time/time.service';
import { MatDialog } from '@angular/material';
import { DevicePreferencesDialogComponent } from '../../../../../shared/dialogs/device-preferences-dialog/device-preferences-dialog.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { DevicesComponent } from '../../devices.component';

@Component({
  selector: 'app-device-info',
  templateUrl: './device-info.component.html',
  styleUrls: ['./device-info.component.css']
})
export class DeviceInfoComponent implements OnInit, OnDestroy {
  @Input() alarms: Alarm[];
  @Input() device: Device;
  @Input() readonly: boolean;

  public zoom = { min: null, max: null };

  public graphData: DeviceDataRange;

  // Span 12 hours ahead
  public static DEFAULT_TIME_RANGE = 1000 * 60 * 60 * 13;
  public static ONE_MINUTE = 60000;

  private currentRequestId = 0;

  private static OPPOSITE_DATA = ["hhCons", "calcFcr"];

  private currentGraphStartTs = 0;
  private currentFrom: number = null;
  private currentTo: number = null;

  public alarmsWrapped = true;

  private timeSubsription: ISubscription;
  private timeAdjustedSubscription: ISubscription;

  private deviceDataRequests: ISubscription[] = [];

  constructor( 
    private alarmService: AlarmService, 
    private timeService: TimeService, 
    private deviceService: DeviceService,
    private dialog: MatDialog,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.timeSubsription = this.timeService.currentStartTs.subscribe(newStartTs => {
      this.onTimescroll(newStartTs);
    });

    this.timeAdjustedSubscription = this.timeService.onAdjusted.subscribe(() => {
      // The time has been adjusted manually, so zoom out all charts
      this.onZoomChange({ min: null, max: null});
    });
  }

  ngOnDestroy() {
    this.timeSubsription.unsubscribe();
    this.timeAdjustedSubscription.unsubscribe();

    for (const req of this.deviceDataRequests) {
      req.unsubscribe();
    }
  }

  public onZoomChange(zoom) {
    this.zoom = zoom;
  }

  private handleCancelError(err: HttpErrorResponse) {
    if (err.status != 0) {
      this.toastrService.error("Could not cancel alarm", "Internal server error");
    } else {
      this.toastrService.error("Could not send request to server", "Connection error");
    }
  }

  private handleDevicePreferencesError(err: HttpErrorResponse) {
    if (err.status != 0) {
      this.toastrService.error("Could not save preferences", "Internal server error");
    } else {
      this.toastrService.error("Could not send request to server", "Connection Error");
    }
  }

  private onGetDeviceDataError(err: HttpErrorResponse) {
    if (err.status != 0) {
      this.toastrService.error("Could not get data", "Internal server error");
    } else {
      this.toastrService.error("Could not get data", "Connection Error");
    }
  }

  private onTimescroll(newStartTs) {
    let offset;
    let delta;

    const now = new Date().getTime();

    if (this.currentFrom != null) {
      offset = newStartTs - this.currentFrom;
      delta = offset < 0 ? -(offset) : offset;
    }

    // Don't refresh this data if we already have it
    if (newStartTs == this.currentFrom) {
      return;
    }

    // If not first load AND ranges collide
    if (this.currentFrom != null && (delta < DeviceInfoComponent.DEFAULT_TIME_RANGE)) {
      let from;
      let to;

      // Going back in time
      if (offset < 0) {
        from = newStartTs;
        to = this.currentFrom;

      // Forward in time
      } else {
        from = this.currentTo;
        to = newStartTs + DeviceInfoComponent.DEFAULT_TIME_RANGE;
      }

      // Fetch the latest data
      if (to > now && (to - from) < DeviceInfoComponent.ONE_MINUTE) {
        from = null;
        to = null;
      }

      // Keep track of ongoing requests, and remove the oldest
      if (this.deviceDataRequests.length > 4) {
        this.deviceDataRequests.shift().unsubscribe();
      }

      this.currentRequestId = (this.currentRequestId + 1) % 100;
      const requestId = this.currentRequestId;

      const newDataRequest = this.deviceService.getDeviceData(
          this.device.info.id,
          from,
          to
        ).subscribe(response => {
          // Out of date request, ignore
          if (this.currentRequestId != requestId) {
            return;
          }
          
          this.currentFrom = newStartTs;
          this.currentTo = newStartTs + DeviceInfoComponent.DEFAULT_TIME_RANGE;
          
          if (this.currentTo > now) {
            this.currentTo = now;
          }
          
          // Save latest startTs for new graphs
          this.currentGraphStartTs = newStartTs;

          if ((this.currentGraphStartTs + DeviceInfoComponent.DEFAULT_TIME_RANGE) > now) {
            this.currentGraphStartTs = (now - DeviceInfoComponent.DEFAULT_TIME_RANGE);
          }

          this.mergeNewDeviceData(response, offset, newStartTs);
        }, (err) => {
          this.onGetDeviceDataError(err);
        });
      
      this.deviceDataRequests.push(newDataRequest);
    } else {
      // Get graph data for all open devices
      this.deviceService.getDeviceData(
        this.device.info.id,
        newStartTs,
        newStartTs + DeviceInfoComponent.DEFAULT_TIME_RANGE
      ).subscribe(response => {
        // Save latest startTs for new graphs
        this.currentGraphStartTs = newStartTs;

        this.currentFrom = newStartTs;
        this.currentTo = newStartTs + DeviceInfoComponent.DEFAULT_TIME_RANGE;
        
        if (this.currentTo > now) {
          this.currentTo = now;
        }

        if ((this.currentGraphStartTs + DeviceInfoComponent.DEFAULT_TIME_RANGE) > now) {
          this.currentGraphStartTs = (now - DeviceInfoComponent.DEFAULT_TIME_RANGE);
        }

        // Format any data that comes through
        this.formatGraphData(response.data);
        this.graphData = response;
      }, (err) => {
        this.onGetDeviceDataError(err);
      });
    }
  }

  private mergeNewDeviceData(newData: DeviceDataRange, offset: number, newGraphStartTs: number) {
    // Format any data that comes through
    this.formatGraphData(newData.data);

    let newGraphData: DeviceDataRange = {
      data: {} as DeviceData,
      from: newGraphStartTs,
      to: newGraphStartTs + DeviceInfoComponent.DEFAULT_TIME_RANGE
    }

    if (offset < 0) {
      for (let dataName in this.graphData.data) {

        // Append new data to start of old data
        newGraphData.data[dataName] = newData.data[dataName].concat(this.graphData.data[dataName]);

        // Remove unneeded data
        for (let i = newGraphData.data[dataName].length - 1; i >= 0; i--) {

          if (newGraphData.data[dataName][i][0] < newGraphData.to) {
            newGraphData.data[dataName] = newGraphData.data[dataName].slice(0, i + 1);
            break;
          }
        }
      }
    } else {
      for (let key in this.graphData.data) {
        // Append new data to end of old data
        newGraphData.data[key] = this.graphData.data[key].concat(newData.data[key]);

        // Remove unneeded data
        for (let i = 0; i < newGraphData.data[key].length; i++) {

          if (newGraphData.data[key][i][0] > newGraphData.from) {
            // Slice from 0 if thats all we have
            let index = i ? i - 1 : 0;
            newGraphData.data[key] = newGraphData.data[key].slice(index);
            break;
          }
        }
      }
    }

    this.graphData = newGraphData;
  }

  private formatGraphData(data: DeviceData): void {
    // Make the opposite values negative
    for (const dataName of DeviceInfoComponent.OPPOSITE_DATA) {
      data[dataName] = data[dataName].map(x => [x[0], -(x[1])]);
    }

    // Calculate prl values
    const prlPosValues = data.prlPos.slice();
    const prlNegValues = data.prlNeg.slice();
    const offsetValues = data.offset.slice();

    const calculated = {
      prlPosPlusOff: [],
      prlNegPlusOff: []
    };

    while (true) {
      const prlNeg = prlNegValues.shift();
      const prlPos = prlPosValues.shift();
      let offset = offsetValues.shift();

      // find where offset is inline with either prl
      if ((prlNeg || prlPos) && offset) {
        while (true) {
          if (prlNeg && prlNeg[0] == offset[0]) {
            break;
          }
          if (prlPos && prlPos[0] == offset[0]) {
            break;
          }
          offset = offsetValues.shift();
        }
      }

      if (prlNeg) {
        if (offset && prlNeg[0] == offset[0]) {
          prlNeg[1] = prlNeg[1] + offset[1];
        }
        calculated.prlNegPlusOff.push(prlNeg);
      }

      if (prlPos) {
        if (offset && prlPos[0] == offset[0]) {
          prlPos[1] = -prlPos[1] + offset[1];
        } else {
          prlPos[1] = -prlPos[1];
        }
        calculated.prlPosPlusOff.push(prlPos)
      }

      if (!prlNeg && !prlPos && !offset) {
        break;
      }
    }

    // Assign calculated values to data object
    data.prlPos = calculated.prlPosPlusOff;
    data.prlNeg = calculated.prlNegPlusOff;
  }

  onAlarmsClosing(event) {
    if (event.target.classList[0] == "alarm-container") {
      this.alarmsWrapped = true;
    }
  }

  onAlarmsOpened(event) {
    let elementStyles = getComputedStyle(event.target);
    if (event.target.classList[0] == "alarm-container" && elementStyles.width == "640px") {
      this.alarmsWrapped = false;
    }
  }

  public cancelAlarmClicked(alarm: Alarm) {
    this.alarmService.cancelAlarm(alarm.id).subscribe(() => {}, (err) => {
      this.handleCancelError(err);
    });
  }

  public alarmEmailClicked(alarm: Alarm) {
    window.location.href = "mailto:wartung.caterva@caterva.de?subject=[ AUSFALL " + this.device.info.id.toUpperCase() + " ]&body="
                           + encodeURI("Error sent from the GUI: " + alarm.message.info + "\n\n\n");
    this.toastrService.info("Opening email client");
  }

  public alarmHovered(alarm: Alarm) {
    if (alarm.muted || this.readonly || alarm.priority != AlarmPrioritory.PRIORITY_HIGH) {
      return;
    }

    this.alarmService.muteAlarm(alarm.id)
      .subscribe(() => {}, (err) => {
        if (err.status != 0) {
          this.toastrService.error("Could not mute alarm", "Internal server error");
        } else {
          this.toastrService.error("Could not mute alarm", "Connection error");
        }
      });
  }

  public openSettings() {
    this.dialog.open(
      DevicePreferencesDialogComponent, 
      { data: {
        device: this.device.info.id, 
        preferences: this.device.info.preferences
      } 
    }).afterClosed()
      .subscribe(newPreferences => {
        // Dialog was closed
        if (typeof(newPreferences) == 'undefined') {
          return;
        }

        this.deviceService
          .setDevicePreferences(this.device.info.id, newPreferences)
          .subscribe(success => {
            this.device.info.preferences = newPreferences;
          }, error => {
            this.handleDevicePreferencesError(error);
          });
      });
  }
}
