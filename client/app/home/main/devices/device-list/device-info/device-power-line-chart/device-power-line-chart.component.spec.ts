import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicePowerLineChartComponent } from './device-power-line-chart.component';

describe('DevicePowerLineChartComponent', () => {
  let component: DevicePowerLineChartComponent;
  let fixture: ComponentFixture<DevicePowerLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicePowerLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicePowerLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
