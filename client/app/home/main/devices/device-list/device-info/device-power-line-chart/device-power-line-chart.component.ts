import { Component, OnInit } from '@angular/core';
import { DeviceChartComponent } from '../device-chart/device-chart.component';


@Component({
  selector: 'app-device-power-line-chart',
  templateUrl: '../device-chart/device-chart.component.html',
  styleUrls: ['../device-chart/device-chart.component.css']
})
export class DevicePowerLineChartComponent extends DeviceChartComponent implements OnInit {
  private static INCLUDED_DATA = ['prlPos', 'prlNeg', 'mesFcr', 'calcFcr', 'hhCons', 'grid', 'pv'];
  private static KEY_TO_COLOUR = {
    'prlPos': '#ff6060',
    'prlNeg': '#00ccff',
    'grid': '#359af9',
    'pv': '#fecf75',
    'hhCons': '#d77df0',
    'calcFcr': '#1bf01a',
    'mesFcr': '#39ad61'
  };

  public static NAME = "power"

  public static ADDITIONAL_OPTIONS = {
    yAxis: {
      title: {
        text: ''
      },
      gridLineColor: "rgba(0, 0, 0, 0.2)",
      tickInterval: 5000
    }
  };

  constructor() { 
    super();
  }

  ngOnInit() {
    this.setup(
      DevicePowerLineChartComponent.INCLUDED_DATA, 
      DevicePowerLineChartComponent.KEY_TO_COLOUR, 
      DevicePowerLineChartComponent.NAME,
      DevicePowerLineChartComponent.ADDITIONAL_OPTIONS
    );
  }
}
