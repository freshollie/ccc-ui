import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceSocLineChartComponent } from './device-soc-line-chart.component';

describe('DeviceSocLineChartComponent', () => {
  let component: DeviceSocLineChartComponent;
  let fixture: ComponentFixture<DeviceSocLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceSocLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceSocLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
