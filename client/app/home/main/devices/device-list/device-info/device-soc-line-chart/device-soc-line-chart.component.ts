import { Component } from '@angular/core';
import { DeviceChartComponent } from '../device-chart/device-chart.component';

@Component({
  selector: 'app-device-soc-line-chart',
  templateUrl: '../device-chart/device-chart.component.html',
  styleUrls: ['../device-chart/device-chart.component.css']
})
export class DeviceSocLineChartComponent extends DeviceChartComponent {
  private static INCLUDED_DATA = ['soc'];
  private static KEY_TO_COLOUR = {
    'soc': '#545bfd'
  }

  private static NAME = "soc";

  private static ADDITIONAL_OPTIONS = {
    yAxis: {
      min: 0,
      max: 100,
      gridLineColor: "rgba(0, 0, 0, 0.2)",
      labels: {
        format: '{value}%'
      },
      title: {
        text: ''
      },
      tickInterval: 20
    }
  };

  constructor() { 
    super();
  }

  ngOnInit() {
    this.setup(
      DeviceSocLineChartComponent.INCLUDED_DATA, 
      DeviceSocLineChartComponent.KEY_TO_COLOUR, 
      DeviceSocLineChartComponent.NAME,
      DeviceSocLineChartComponent.ADDITIONAL_OPTIONS
    );
  }
}
