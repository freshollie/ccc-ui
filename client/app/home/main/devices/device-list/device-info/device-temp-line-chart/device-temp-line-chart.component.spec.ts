import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceTempLineChartComponent } from './device-temp-line-chart.component';

describe('DeviceTempLineChartComponent', () => {
  let component: DeviceTempLineChartComponent;
  let fixture: ComponentFixture<DeviceTempLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceTempLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceTempLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
