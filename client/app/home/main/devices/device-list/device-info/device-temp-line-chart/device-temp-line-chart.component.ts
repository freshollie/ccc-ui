import { Component } from '@angular/core';
import { DeviceChartComponent } from '../device-chart/device-chart.component';

@Component({
  selector: 'app-device-temp-line-chart',
  templateUrl: '../device-chart/device-chart.component.html',
  styleUrls: ['../device-chart/device-chart.component.css']
})
export class DeviceTempLineChartComponent extends DeviceChartComponent {
  private static INCLUDED_DATA = ['temp'];
  private static KEY_TO_COLOUR = {
    'temp': '#545bfd'
  }

  private static NAME = "temp";

  private static ADDITIONAL_OPTIONS = {
    yAxis: {
      min: 0,
      max: 70,
      gridLineColor: "rgba(0, 0, 0, 0.2)",
      labels: {
        format: '{value}°C'
      },
      title: {
        text: ''
      },
      tickInterval: 10
    }
  };

  constructor() { 
    super();
  }

  ngOnInit() {
    this.setup(
      DeviceTempLineChartComponent.INCLUDED_DATA, 
      DeviceTempLineChartComponent.KEY_TO_COLOUR, 
      DeviceTempLineChartComponent.NAME,
      DeviceTempLineChartComponent.ADDITIONAL_OPTIONS
    );
  }
}
