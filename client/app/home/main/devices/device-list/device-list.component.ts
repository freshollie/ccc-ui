import { Component, OnInit, OnDestroy, OnChanges, Input, SimpleChanges, AfterViewChecked, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Alarm, AlarmSourceType } from '../../../../shared/services/alarm/alarm.service';
import * as moment from 'moment';
import { DeviceService, Device, EssStates } from '../../../../shared/services/device/device.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { NgModule } from '@angular/core';
import {TableModule} from 'primeng/table';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit, AfterViewChecked, OnDestroy, OnChanges {

  @Input() loading = true;
  @Input() devices: Device[] = [];
  @Input() alarms: Alarm[] = [];
  @Input() readonly = false;

  public sortByType = "info";
  public sortByAttr = "id";
  public sortAsc = true;

  public mergedDevices: Device[] = [];
  public deviceAlarms = {};

  public infoVisableDevices = new Set();
  public pcpChangePendingDevices = new Set();
  public adminChangePendingDevices = new Set();

  public TOOLTIP_SHOW_DELAY = 200;

  public cols: any[];
 
  private static OLD_DATA_TIME = 180000; // 3 minutes
  private static TOO_OLD_DATA_TIME = 900000; // 15 minutes
  private static INITIAL_DATA_COLOUR = 102;
  private static MAX_RED = 255;
  
  private currentTime = new Date().getTime();
  
  constructor(
    private deviceService: DeviceService,
    private toastrService: ToastrService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit() {

    this.cols = [
      { type: 'info', attribute: 'id', header: 'Device' },
      { type: 'other', attribute: 'state', header: 'State' },
      { type: 'other', attribute: 'admin', header: 'Device on/off' },
      { type: 'other', attribute: 'pcp', header: 'FCR on/off' },
      { type: 'info', attribute: 'type', header: 'Generation' },
      { type: 'other', attribute: 'charge', header: 'Soc [kWh]' },
      { type: 'data', attribute: 'soc', header: 'Soc [%]' },
      { type: 'data', attribute: 'temp', header: 'Temperature [°C]' },
      { type: 'data', attribute: 'prlPos', header: 'Positive Limit [kW]' },
      { type: 'data', attribute: 'prlNeg', header: 'Negative Limit [kW]' },
      { type: 'data', attribute: 'offset', header: 'Offset [kW]' },
      { type: 'other', attribute: 'alarms', header: '<i class="fa fa-bell-o" aria-hidden="true"></i>' }
  ];

    this.mergeNewDevices();
    this.mergeNewAlarms();
  }

  ngAfterViewChecked() {
    // Increment the current time only after the view has been checked
    this.currentTime = new Date().getTime();
    this.cdRef.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.devices) {
      this.mergeNewDevices();
    }
    
    if (changes.alarms) {
      this.mergeNewAlarms();
    }
  }

  ngOnDestroy() {

  }

  private handleSwitchError(err: HttpErrorResponse) {
    if (err.status == 403) {
      this.toastrService.error("Your permissions might have changed", "Unauthorised");
    } else if (err.status != 0) {
      this.toastrService.error("", "Internal server error");
    } else {
      this.toastrService.error("", "Connection error");
    }
  }

  /**
   * Perform a comparion of a and b with the given sort options.
   * 
   * Very ugly, very sorry
   */
  private compare(a: Device, b: Device, type: string, attr: string) {
    let data1 = 0;
    let data2 = 0;

    if (type == "data") {
      // Ignore datatype if the device is not active
      if ((this.isActive(a) || attr == "prlNeg" || attr == "prlPos" || attr == "offset")
          && a.data[attr].length > 0 ) {
        data1 = a.data[attr][0][1];
      }

      if ((this.isActive(b) || attr == "prlNeg" || attr == "prlPos" || attr == "offset")
          && b.data[attr].length > 0 ) {
        data2 = b.data[attr][0][1];
      }

    } else if (type == "info") {
      // We are sorting by the devices info
      if (attr == "id") {
        return (a.info.id.slice(2) as any) - (b.info.id.slice(2) as any)

      } else if (attr == "type") {
        return a.info.type.localeCompare(b.info.type);
      }

    } else {
      if (attr == "alarms") {
        data1 = (this.deviceAlarms[a.info.id] || []).length;
        data2 = (this.deviceAlarms[b.info.id] || []).length;

      } else if (attr == "charge") {
        data1 = this.isActive(a) ? this.calculateCharge(a) : 0;
        data2 = this.isActive(b) ? this.calculateCharge(b) : 0;

      } else if (attr == "state") {
        return this.getStateClass(a).localeCompare(this.getStateClass(b));

      } else if (attr == "pcp" || attr == "admin") {
        data1 = a.data[attr].length > 0 ? a.data[attr][0][1]: 0;
        data2 = b.data[attr].length > 0 ? b.data[attr][0][1]: 0;
      }
    }

    return data1 - data2;
  }

  public onSortChanged(type: string, attr: string) {
    this.sortAsc = this.sortAsc != true;
    this.sortByType = type;
    this.sortByAttr = attr;
    this.doListSort();
  }

  private doListSort() {
    // Sort the list in place
    this.mergedDevices.sort((a, b) => {
      if (this.sortAsc) {
        return this.compare(a, b, this.sortByType, this.sortByAttr);
      } else {
        return this.compare(b, a, this.sortByType, this.sortByAttr);
      }
    });
  }

  private mergeNewDevices() {
    // Keep track of current IDs
    const currentIds = new Set();

    // Update current device objects with updated data and alarms
    for (let updatedDevice of this.devices) {

      const deviceId = updatedDevice.info.id;
      currentIds.add(deviceId);

      let exists = false;

      // Update the data object of the
      // previous reference to this device
      for (const device of this.mergedDevices) {
        if (device.info.id == deviceId) {
          device.data = updatedDevice.data;
          device.info = updatedDevice.info;
          exists = true;
          break;
        }
      }

      // Add any devices that didn't exist before
      if (!exists) {
        this.mergedDevices.push(updatedDevice);
      }
    }

    // Remove devices that aren't in the updated list
    let i = this.mergedDevices.length - 1;
    for (; i > -1; i--) {
      // Find device in updated list
      const existingDevice = this.mergedDevices[i];
      let exists = currentIds.has(existingDevice.info.id);

      // Remove device if not in updated array
      if (!exists) {
        this.mergedDevices.splice(i, 1);
      }
    }

    this.doListSort();
  }

  private mergeNewAlarms() {
    this.deviceAlarms = {};

    for (const alarm of this.alarms) {
      if (alarm.sourceType != AlarmSourceType.DEVICE) {
        continue;
      }

      if (!this.deviceAlarms[alarm.source]) {
        this.deviceAlarms[alarm.source] = [];
      }
      this.deviceAlarms[alarm.source].push(alarm);
    }
    
    // Perform sort with the new alarms
    if (this.sortByType == "other" && this.sortByAttr == "alarms") {
      this.doListSort();
    }
  }

  formatState(device: Device) {
    if (device.data.state.length == 0) {
      return '-';
    } else {
      return device.data.state[0][1] == 1 ? 'active' : 'inactive';
    }
  }

  formatDataTimestamp(data: number[][]) {
    if (data.length < 1) {
      return moment(0).format('DD.MM.YYYY HH:mm:ss');
    }

    return moment(data[0][0]).format('DD.MM.YYYY HH:mm:ss');
  }

  isActive(device: Device) {
    return device.data.state.length > 0 
          && device.data.state[0][1] == EssStates.ESS_ALIVE;
  }

  formatLatestData(data: number[][]) {
    if (data.length == 0) {
      return '-';
    } else {
      return data[0][1];
    }
  }

  calcDataColour(data: number[][]) {
    let r = DeviceListComponent.INITIAL_DATA_COLOUR;
    let g = DeviceListComponent.INITIAL_DATA_COLOUR;
    let b = DeviceListComponent.INITIAL_DATA_COLOUR;

    if (data.length == 0) {
      r = 255;
    } else {
      const age = (this.currentTime - data[0][0])

      if (age > DeviceListComponent.OLD_DATA_TIME) {
        const ratio = (age - DeviceListComponent.OLD_DATA_TIME) / DeviceListComponent.TOO_OLD_DATA_TIME;
        
        r = Math.round(DeviceListComponent.INITIAL_DATA_COLOUR + ((DeviceListComponent.MAX_RED - DeviceListComponent.INITIAL_DATA_COLOUR) * ratio));

        if (r > 255) {
          r = 255;
        }

        g = 102 - (r - 102);
        if (g < 0) {
          g = 0;  
        }

        b = 102 - (r - 102);
        if (b < 0) {
          b = 0;  
        }
      }
    }

    return `rgb(${r}, ${g}, ${b})`;
  }

  getFilteredDevices() {
    
  }

  filterAlarmsByPriority(priority: string, device: Device) {
    const alarmsOfCorrectPriority = [];
    const alarms = this.deviceAlarms[device.info.id];

    if (!alarms) {
      return [];
    }

    for (let alarm of alarms) {
      if (alarm.priority == priority) {
        alarmsOfCorrectPriority.push(alarm);
      }
    }

    return alarmsOfCorrectPriority;
  }

  getStateClass(device) {
    if (device.data.state.length < 1 || device.data.admin[0][1] == 0) {
      return 'inactive'
    }

    switch (device.data.state[0][1]) {
      case EssStates.ESS_ALIVE:
      case EssStates.ESS_CHARGER_WAITING:
        return 'active';
      default:
        return 'not-matched';
    }
  }

  // Triggered on device click (the drop down)
  toggleDeviceInfo(device: Device) {
    if (this.infoVisableDevices.has(device.info.id)) {
      this.infoVisableDevices.delete(device.info.id);
    } else {
      this.infoVisableDevices.add(device.info.id);
    }
  }

  round(value, precision) {
    let factor = Math.pow(10, precision);
    return Math.round(value * factor) / factor;
  }

  calculateCharge(device) {
    return (device.info.cap * (device.data.soc[0][1] / 100)) / 1000;
  }

  /**
   * Triggered when the user toggles the "FCR" switch
   */
  onPcpSwitchClicked(device: Device, event: Event): void {
    event.stopPropagation();

    if (this.pcpChangePendingDevices.has(device.info.id) 
        || device.info.preferences.lock 
        || this.readonly) {
      return;
    }

    const newAdminPcp = device.data.pcp[0][1] ? 0: 1;
    this.pcpChangePendingDevices.add(device.info.id);

    this.deviceService
      .setDevicePcpAdmin(device.info.id, newAdminPcp)
      .finally(() => {
        this.pcpChangePendingDevices.delete(device.info.id);
      })
      .subscribe(() => {
        device.data.pcp[0][1] = newAdminPcp;
      }, error => {
        this.handleSwitchError(error);
      });
  }

  /**
   * Called then the user toggles the "On/off" switch
   */
  onAdminSwitchClicked(device: Device, event: Event): void {
    event.stopPropagation();

    if (this.adminChangePendingDevices.has(device.info.id) 
        || device.info.preferences.lock 
        || this.readonly) {
      return;
    }
    
    const newAdminState = device.data.admin[0][1] ? 0: 1;
    this.adminChangePendingDevices.add(device.info.id);

    this.deviceService
      .setDeviceAdminState(device.info.id, newAdminState)
      .finally(() => {
        this.adminChangePendingDevices.delete(device.info.id);
      })
      .subscribe(() => {
        device.data.admin[0][1] = newAdminState;
      }, error => {
        this.handleSwitchError(error);
      });
  }
}
