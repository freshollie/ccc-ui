import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { DeviceService, Device } from '../../../shared/services/device/device.service';
import { SwarmService } from '../../../shared/services/swarm/swarm.service';
import { Alarm, AlarmService } from '../../../shared/services/alarm/alarm.service';
import { SearchService } from '../../../shared/services/search/search.service';


@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit, OnDestroy {
  public filteredDevices: Device[] = [];
  public alarmsData: Alarm[] = [];

  public loading = true;
  public devices: Device[] = [];
  public currentSearch = "";
  public readonlySwarm = false;

  public totalCharge: number;
  public meanSoc: number;
  public maxTemp: number;
  public totalPosLimit: number;
  public totalNegLimit: number;
  public totalOffset: number;

  private deviceDataSubscription: ISubscription;
  private swarmInfoSubscription: ISubscription;
  private searchSubscription: ISubscription;
  private alarmSubscription: ISubscription;
  private swarmChangeSubscription: ISubscription;

  constructor(private deviceService: DeviceService, 
              private alarmService: AlarmService,
              private swarmService: SwarmService,
              private searchService: SearchService) { }

  ngOnInit(): void {
    // When the component initialises, fetch the latest data
    const currentSwarm = this.swarmService.getCurrentSwarmName();
    if (currentSwarm) {
      this.deviceService.doLatestDeviceDataFetch(currentSwarm);
    }

    // When we change swarms, don't allow anything to transition
    this.swarmChangeSubscription = this.swarmService.currentSwarmName.subscribe(() => {
      this.loading = true;
    });

    // Listen to new device data
    this.deviceDataSubscription = this.deviceService.currentLatestDeviceData
      .subscribe(devices => {
        this.loading = false;
        this.devices = devices;
        this.doSearchFilter();
        this.updateTotals();
      });
    
    // Listen for new swarm info, and if the swarm is readonly 
    // (Not managed or readonly for this user)
    this.swarmInfoSubscription = this.swarmService.currentSwarmInfo
      .subscribe((swarmInfo) => {
        this.readonlySwarm = swarmInfo.managed == false || swarmInfo.readonly;
        console.log(this.readonlySwarm);
      });
    
    // Subscribe to latest device search
    this.searchSubscription = this.searchService.currentSearch.subscribe(searchInput => {
      this.currentSearch = searchInput;
      this.doSearchFilter();
    });

    // Subscribe to latest and alarm values
    this.alarmSubscription = this.alarmService.currentAlarms.subscribe(alarms => {
      this.alarmsData = alarms;
    });
  }

  ngOnDestroy() {
    this.deviceDataSubscription.unsubscribe();
    this.swarmInfoSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
    this.alarmSubscription.unsubscribe();
    this.swarmChangeSubscription.unsubscribe();
  }

  private doSearchFilter() {
    if (this.currentSearch == "") {
      this.filteredDevices = this.devices;
      return;
    }

    this.filteredDevices = this.devices.filter((device) => {
      return device.info.id.indexOf(this.currentSearch) > -1;
    });
  }

  /**
   * When we receive device information, update the totals widget
   */
  private updateTotals() {
    let totalSoc = 0;

    this.totalCharge = 0;
    this.maxTemp = 0;
    this.totalPosLimit = 0;
    this.totalNegLimit = 0;
    this.totalOffset = 0;
    // Count up statistics
    const length = this.devices.length;
    let i = 0;

    for (; i < length; i++) {
      const device = this.devices[i];
      // Count states
      if (device.data.state.length < 1) {
        continue;
      }

      // Count stat values
      if (device.data.soc.length > 0) {
        let calculatedCharge = device.info.cap * (device.data.soc[0][1] / 100.0);

        this.totalCharge += calculatedCharge;

        totalSoc += device.data.soc[0][1];
      }
      if (device.data.temp.length > 0) {
        if (device.data.temp[0][1] > this.maxTemp) {
          this.maxTemp = device.data.temp[0][1];
        }
      }
      if (device.data.prlPos.length > 0) {
        this.totalPosLimit += device.data.prlPos[0][1];
      }
      if (device.data.prlNeg.length > 0) {
        this.totalNegLimit += device.data.prlNeg[0][1];
      }
      if (device.data.offset.length > 0) {
        this.totalOffset += device.data.offset[0][1];
      }
    }

    // Caluclate mean values
    if (this.devices.length > 0) {
      this.meanSoc = totalSoc / this.devices.length;
    } else {
      this.meanSoc = 0;
    }

    this.maxTemp = Math.round(this.maxTemp);
    this.meanSoc = Math.round(this.meanSoc);
    this.totalCharge = Math.round(this.totalCharge / 1000);
    this.totalPosLimit = Math.round(this.totalPosLimit / 1000);
    this.totalNegLimit = Math.round(this.totalNegLimit / 1000);
    this.totalOffset = Math.round(this.totalOffset / 1000);
  }
}
