import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { EnergyService, FahrplanEvents, FahrplanType, FahrplanEvent } from '../../../shared/services/energy/energy.service';
import { ISubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs';
import { SwarmService } from '../../../shared/services/swarm/swarm.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmationDialogComponent } from '../../../shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { EventListenerService, FahrplanChangeEvent } from '../../../shared/services/events/event-listener.service';
import { EventConfirmDialogComponent } from '../../../shared/dialogs/event-confirm-dialog/event-confirm-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';


// Taken from the fahrplan API documentation
const INVALID_REQUEST_CODE_TO_MESSAGE = {
  "-1": "Invalid request",
  0: "Swarm must be a top level swarm",
  1: "Requested start does not fall on a quarter", 
  2: "Requested duration does not fall on a quarter, or is too long", 
  3: "CHPP request is not legal", 
  4: "Pending charge requests already exists", 
  5: "The requested contract start is already closed", 
  6: "FCR conflict in given range",
  7: "Invalid type transistion",
  8: "Event type not cancellable",
  9: "FCR request is not legal",
  10: "Illegal charge request",
  11: "Pending FCR requests already exist",
  12: "Event is complete, not cancellable"
}

@Component({
  selector: 'app-energy-management',
  templateUrl: './energy-management.component.html',
  styleUrls: ['./energy-management.component.css']
})
export class EnergyManagementComponent implements OnInit, OnDestroy {

  public fahrplan: FahrplanEvents = [];
  public chargeSource: string = "CHPP";
  public selectedChargePower: number = 0;
  public selectedChargeStart;
  public selectedChargeDuration: number = 0;
  public selectedDischargePower: number = 0;
  public selectedDischargeStart;
  public selectedDischargeDuration: number = 0;
  public selectedFCRPower: number = 0;
  public selectedFCRRange;

  // Default time range is a week in the past
  private defaultTimeRange = 1000*60*60*24*7;
  private swarmName: string;
  private swarmSubscription: ISubscription;
  private intervalSubscription: ISubscription;
  private confirmationDialogRef: MatDialogRef<ConfirmationDialogComponent>;
  private fahrplanEventsSubscription: ISubscription;

  private popupShown = false;
  private dialogOpen = false;

  private cancellationDialogOpen = false;

  public cols: any[];

  constructor(
    private energyService: EnergyService,
    private swarmService: SwarmService,
    private dialog: MatDialog,
    private eventListenerService: EventListenerService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {

    this.cols = [
      { width: '4%', minWidth: '30px', header: '' },
      { width: '14%', minWidth: '60px', header: 'Type' },
      { width: '18%', minWidth: '140px', header: 'State' },
      { width: '10%', minWidth: '45px', header: 'Power (MW)' },
      { width: '25%', minWidth: '160px', header: 'Start' },
      { width: '16%', minWidth: '80px', header: 'Duration' },
      { width: '7%', minWidth: '50px', header: 'Source' },
      { width: '6%', minWidth: '50px', header: 'Cancel' },
  ];

    // Subscribe to changes in swarm
    this.swarmSubscription = this.swarmService.currentSwarmName.subscribe(swarmName => {

      // Store swarm name
      this.swarmName = swarmName;

      // Get fahrplan
      this.refreshFahrplan();

      // Re-subscribe to 60 second refresh interval
      if (this.intervalSubscription) {
        this.intervalSubscription.unsubscribe();
      }
      
      // Update fahrplan offen
      this.intervalSubscription = Observable.interval(5000).subscribe(x => {
        // Get fahrplan
        this.refreshFahrplan();
      });
    });

    // Refresh the fahrplan if there were any event changes from other users
    this.fahrplanEventsSubscription = this.eventListenerService.fahrplanEvents.subscribe((event: FahrplanChangeEvent) => {
      if (event.swarm == this.swarmService.getCurrentSwarmName()) {
        this.refreshFahrplan();
      }
    });
  }

  ngOnDestroy() {
    if (this.swarmSubscription) {
      this.swarmSubscription.unsubscribe();
    }

    if (this.intervalSubscription) {
      this.intervalSubscription.unsubscribe();
    }

    this.fahrplanEventsSubscription.unsubscribe();
  }

  private refreshFahrplan(): void {
    this.energyService.getFahrplan(
      this.swarmName,
      Date.now() - this.defaultTimeRange
    ).subscribe(fahrplan => {
      this.fahrplan = fahrplan;
    }, (err) => {
      this.handleRefreshError(err);
    });
  }

  getHourStart() {
    // Store time of previous hour
    let currentHourTime = new Date();
    currentHourTime.setMinutes(0);
    currentHourTime.setSeconds(0);
    currentHourTime.setMilliseconds(0);
    return currentHourTime;
  }

  getMidnightToday() {
    let midnightTody = new Date();
    midnightTody.setHours(0);
    midnightTody.setMinutes(0);
    midnightTody.setSeconds(0);
    midnightTody.setMilliseconds(0);
    return midnightTody;
  }

  private handleRefreshError(err: HttpErrorResponse) {
    if (err.status == 403 || err.status == 401) {
      this.toastrService.error("Could not get fahrplan, please reload this page", "Access error");
    } else if (err.status != 0) {
      this.toastrService.error("Could not get fahrplan", "Internal server error");
    } else {
      this.toastrService.error("Could not get fahrplan", "Connection error");
    }
  }

  private handleRequestError(err: HttpErrorResponse) {
    // Not authorised, probably not operator
    if (err.status == 403) {
      this.toastrService.error("Only the swarm operator can control energy management");
    } else if (err.status == 422) {
      this.toastrService.error(INVALID_REQUEST_CODE_TO_MESSAGE[err.error.error.code]);
    } else if (err.status == 500) {
      this.toastrService.error("Could not perform action", "Internal server error");
    } else {
      this.toastrService.error("Could not send request", "Connection error");
    }
  }

  submitChargeForm(form) {
    if (!this.isChargeValid()) {
      return;
    }

    let startTs = form.value.start.getTime();

    this.energyService.makeChargeRequest(
      this.swarmName,
      this.chargeSource,
      (form.value.power * 1000000),
      startTs,
      (form.value.duration * 1000 * 60)
    ).subscribe(response => {
      this.popupShown = false;

      // Reset power so they can't spam it
      this.selectedChargePower = 0;

      // Re-retrieve fahrplan
      this.refreshFahrplan();
    }, (err: HttpErrorResponse) => {
      this.handleRequestError(err);
    });
  }

  submitDischargeForm(form) {
    if (!this.isDischargeValid()) {
      return;
    }

    let startTs = form.value.start.getTime();

    this.energyService.makeChargeRequest(
      this.swarmName,
      this.chargeSource,
      (form.value.power * 1000000 * -1),
      startTs,
      (form.value.duration * 1000 * 60)
    ).subscribe(response => {
      this.popupShown = false;

      // Reset power so they can't spam it
      this.selectedDischargePower = 0;

      // Re-retrieve fahrplan
      this.refreshFahrplan();
    }, (err: HttpErrorResponse) => {
      this.handleRequestError(err);
    });
  }

  submitControlReserveForm(form) {

    if (!this.isFCRValid()) {
      return;
    }
    
    let startTs = form.value.range[0].getTime();
    let endTs = form.value.range[1].getTime();
    let duration = endTs - startTs;

    this.energyService
      .makeFCRRequest(
        this.swarmName,
        (form.value.power * 1000000),
        startTs,
        duration
      )
      .subscribe(response => {
        this.popupShown = false;
        this.selectedFCRPower = 0;

        this.refreshFahrplan();
      }, (err: HttpErrorResponse) => {
        this.handleRequestError(err);
      });
  }

  private doConfirmationDialog(requests: FahrplanEvent[]) {
    if (this.dialogOpen) {
      return;
    }
    
    this.dialogOpen = true;
    const dialog = this.dialog.open(EventConfirmDialogComponent, {
      data: {
        events: requests
      }
    });

    dialog
      .afterClosed()
      .subscribe(confirmed => {
        this.dialogOpen = false;
        // Dialog was closed
        if (typeof(confirmed) == 'undefined') {
          return;
        }

        this.popupShown = false;

        if (confirmed) {
          this.energyService
            .confirmRequests(this.swarmName, requests)
            .subscribe(response => {
              this.refreshFahrplan();
            }, (err: HttpErrorResponse) => {
              this.handleRequestError(err);
            });

        } else {
          this.energyService
            .cancelRequests(this.swarmName, requests)
            .subscribe(response => {
              this.refreshFahrplan();
            }, (err: HttpErrorResponse) => {
              this.handleRequestError(err);
            });
        }
      });
  }

  public isChargeValid() {
    if (this.selectedChargePower != 0 && this.selectedChargeStart && this.selectedChargeDuration) {
      return true;
    }
  }

  public isDischargeValid() {
    if (this.selectedDischargePower != 0 && this.selectedDischargeStart && this.selectedDischargeDuration) {
      return true;
    }
  }

  public isFCRValid() {
    if (this.selectedFCRPower != 0 && this.selectedFCRRange) {
      return true;
    }
  }

  public fahrplanRowClicked(row) {
    if (row.serverDefinedType != FahrplanType.CHARGE_DESIRED_TYPE && 
        row.serverDefinedType != FahrplanType.FCR_REQUESTED_TYPE) {
      return;
    }

    // Confirm all fahrplan requests of this type
    const events = [];

    for (const event of this.fahrplan) {
      if (row.serverDefinedType == event.type) {
        events.push(event);
      }
    }

    this.doConfirmationDialog(events);
  }

  public openCancelConfirmationDialog(row) {
    if (this.cancellationDialogOpen) {
      return;
    }

    let rowIdentifier = row.englishApplicationType + ' from ' + row.start;
    
    this.cancellationDialogOpen = true;
    const dialog = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        confirmationMessage: 'Are you sure you wish to cancel the ' + rowIdentifier + '?',
        cancelText: 'Dismiss',
        confirmText: 'Cancel Order'
      }
    });

    dialog
      .afterClosed()
      .subscribe(confirmed => {
        this.cancellationDialogOpen = false;
        // Dialog was closed
        if (typeof(confirmed) == 'undefined') {
          return;
        }

        if (confirmed) {
          // Cancel the fcr event
          this.energyService.cancelFCREvent(this.swarmName, row.id).subscribe(success => {
            console.log('Request cancelled successfully');
          }, (err: HttpErrorResponse) => {
            this.handleRequestError(err);
          });
        } else {
          // Dialog dismissed
          return;
        }
      });
  }

}
