import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { AlarmService, Alarm, AlarmPrioritory } from '../../../shared/services/alarm/alarm.service';
import { Observable } from 'rxjs';
import { DeviceService, Device } from '../../../shared/services/device/device.service';
import { SwarmService, DataRange } from '../../../shared/services/swarm/swarm.service';
import { TimeService } from '../../../shared/services/time/time.service';
import { EnergyService } from '../../../shared/services/energy/energy.service';
import { DeviceInfoComponent } from '../devices/device-list/device-info/device-info.component';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit, OnDestroy {

  public mathRound = Math.round;
  public numActive: number;
  
  public numInactive: number;
  public numNotMatched: number;
  public controlReserve: number;
  public pvPower: number;
  public hhConsumption: number;
  public soc: number;

  public zoom = { min: null, max: null };

  public swarmAlarms: Alarm[] = [];
  public swarmData: DataRange;
  public dataLoading: boolean = true;
  public swarmDevices: Device[] = [];
  public readonlySwarm = false;
  public managedSwarm = true;
  public deviceSocVisible = true;
  public deviceTempVisible = false;
  public activeDevices = 0;
  public activeWithFCR = 0;

  private timeAndSwarmSubsciption: ISubscription;
  private timeAdjustedSubscription: ISubscription;
  private swarmChangeSubscription: ISubscription;
  private swarmInfoSubscription: ISubscription;
  private latestSwarmDataSubscription: ISubscription;
  private latestDeviceDataSubscription: ISubscription;
  private paramsAlarmsSubscription: ISubscription;

  private swarmDataRequests: ISubscription[] = [];

  private currentRequestId = 0;
  private lastRecievedRequest = -1;

  private static DATA_RANGE = 1000 * 60 * 60 * 24;

  constructor(
    private deviceService: DeviceService,
    private swarmService: SwarmService,
    private alarmService: AlarmService,
    private timeService: TimeService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.swarmChangeSubscription = this.swarmService.currentSwarmName.subscribe((name: string) => {
      this.dataLoading = true;
    });

    this.timeAdjustedSubscription = this.timeService.onAdjusted.subscribe(() => {
      // When the time is adjusted, we should zoom out all the charts
      this.onZoomChange({ min: null, max: null });
    });

    this.swarmInfoSubscription = this.swarmService.currentSwarmInfo.subscribe((info) => {
      this.readonlySwarm = info.readonly;
      this.managedSwarm = info.managed;
    });

    // Subscribe to time change or swarm change
    this.timeAndSwarmSubsciption = Observable.combineLatest(
      this.timeService.currentStartTs,
      this.swarmService.currentSwarmName
    ).subscribe(latestValues => {
        const startTs = latestValues[0];
        const swarmName = latestValues[1];

        const requestId = this.currentRequestId = this.currentRequestId + 1;

        // If we have made more than 4 requests, remove the oldest
        if (this.swarmDataRequests.length > 4) {
          this.swarmDataRequests.shift().unsubscribe();
        }

        // Retrieve swarm data
        this.swarmDataRequests.push(this.swarmService.getSwarmData(
          swarmName,
          startTs,
          startTs + OverviewComponent.DATA_RANGE
        ).subscribe(values => {
          // Out of date
          if (requestId < this.lastRecievedRequest) {
            return;
          }

          this.lastRecievedRequest = requestId;
          this.swarmData = values;
          this.dataLoading = false;
        }));
      });

    // Subscribe to latest swarm values
    this.latestSwarmDataSubscription = this.swarmService.currentLatestSwarmData.subscribe(data => {
      if (Object.keys(data).length > 0) {
        this.controlReserve = data.mesFcr[0][1] / 1000.0;
        this.pvPower = data.pv[0][1] / 1000.0;
        this.hhConsumption = data.hhCons[0][1] / 1000.0;
        this.soc = data.soc[0][1];
      }
    });

    // Subscribe to latest device data for health bar
    this.latestDeviceDataSubscription = this.deviceService.currentLatestDeviceData.subscribe(devices => {
      this.swarmDevices = devices;
    });

    // Subscribe to the latest alarms for swarm and the current swarm name
    this.paramsAlarmsSubscription = Observable.combineLatest(
      this.swarmService.currentSwarmName,
      this.alarmService.currentAlarms
    ).subscribe(latestValues => {
      let swarmName = latestValues[0];
      let alarms = latestValues[1];
      this.swarmAlarms = [];

      for (let alarm of alarms) {
        if (alarm.source == swarmName) {
          this.swarmAlarms.push(alarm);
        }
      }
    });
  }

  ngOnDestroy() {
    this.timeAndSwarmSubsciption.unsubscribe();
    this.latestSwarmDataSubscription.unsubscribe();
    this.latestDeviceDataSubscription.unsubscribe();
    this.paramsAlarmsSubscription.unsubscribe();
    this.swarmChangeSubscription.unsubscribe();
    this.swarmInfoSubscription.unsubscribe();
    this.timeAdjustedSubscription.unsubscribe();

    // stop all current data requests
    for (const req of this.swarmDataRequests) {
      req.unsubscribe();
    }
  }

  public onZoomChange(zoom) {
    this.zoom = zoom;
  }

  public cancelAlarmClicked(alarm: Alarm) {
    this.alarmService.cancelAlarm(alarm.id)
      .subscribe(() => {}, (err) => {
        if (err.status != 0) {
          this.toastrService.error("Could not cancel alarm", "Internal server error");
        } else {
          this.toastrService.error("Could not send request to server", "Connection error");
        }
      });
  }

  public alarmHovered(alarm: Alarm) {
    if (alarm.muted || this.readonlySwarm || alarm.priority != AlarmPrioritory.PRIORITY_HIGH) {
      return;
    }
    
    this.alarmService.muteAlarm(alarm.id)
      .subscribe(() => {}, (err) => {
        if (err.status != 0) {
          this.toastrService.error("Could not mute alarm", "Internal server error");
        } else {
          this.toastrService.error("Could not mute alarm", "Connection error");
        }
      });
  }

  public viewDeviceSocChart(): void {
    this.deviceTempVisible = false;
    this.deviceSocVisible = true;
  }

  public viewDeviceTempChart(): void {
    this.deviceSocVisible = false;
    this.deviceTempVisible = true;
  }
}
