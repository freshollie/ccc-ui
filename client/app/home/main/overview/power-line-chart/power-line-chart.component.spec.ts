import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerLineChartComponent } from './power-line-chart.component';

describe('SwarmOverviewGraphComponent', () => {
  let component: PowerLineChartComponent;
  let fixture: ComponentFixture<PowerLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
