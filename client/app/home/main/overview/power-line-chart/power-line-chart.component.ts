import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DataRange } from '../../../../shared/services/swarm/swarm.service';
import { FahrplanType } from '../../../../shared/services/energy/energy.service';
import { ChartComponent } from '../../../../shared/components/chart/chart.component';


@Component({
  selector: 'app-power-line-chart',
  templateUrl: './power-line-chart.component.html',
  styleUrls: ['./power-line-chart.component.css']
})
export class PowerLineChartComponent extends ChartComponent implements OnInit, OnChanges, OnDestroy {

  @Input() loading: boolean = true;
  @Input() swarmData: DataRange;

  @Input() zoom;

  @Output() onZoom: EventEmitter<any> = new EventEmitter();

  private chart: any;
  public chartVisible = false;

  private static REQUIRED_DATA = ['maxDis', 'maxChrg', 'optMaxChrg', 'pv', 'hhCons', 'grid', 'calcFcr', 'mesFcr'];

  private static CONFIRMED_FAHRPLAN_TYPES = [
    FahrplanType.CHARGE_CONFIRMED_TYPE, 
    FahrplanType.FCR_APPROVED_TYPE,
    FahrplanType.INTRADAY_TRADE_TYPE
  ];

  private static KEYS_TO_COLOURS = {
    'grid': '#359af9',
    'maxDis': '#ff6060',
    'maxChrg': '#ff6060',
    'optMaxChrg': '#8d8dfb',
    'pv': '#fecf75',
    'hhCons': '#d77df0',
    'calcFcr': '#1bf01a',
    'mesFcr': '#39ad61'
  }

  private static KEY_LINE_WIDTHS = {
    'maxDis': 1.5,
    'maxChrg': 1.5,
    'optMaxChrg': 1.5,
    'fahrplan': 1.5
  }

  public chartOptions = {
    plotOptions: {
      line: {
        animation: false,
        states: {
          hover: {
            enabled: false
          }
        },
        marker: {
          enabled: false,
        }
      }
    },
    series: [],
    chart: {
      renderTo: 'power-line-chart',
      responsive: true,
      zoomType: 'x',
      animation: false,
      marginLeft: 75,
    },
    tooltip: {
      enabled: false
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'datetime',
      plotLines: [],
      plotBands: [],
      events: {
        setExtremes: (e) => {
          this.handleExtremesChanged(e, this.chart, this.swarmData.from, this.swarmData.to);
        }
      },
    },
    yAxis: [{
      gridLineColor: "rgba(0, 0, 0, 0.2)",
      tickPixelInterval: 50,
      labels: {
        formatter: function() {
          return (this.value / 1000.0).toString() + 'kW';
        }
      },
      title: {
        text: ''
      }
    }],
    legend: {
      floating: true,
      y: -20,
    }
  };

  constructor() { 
    super();
  }

  private extractData(dataRange: DataRange, key: string): number[][] {
    let data = this.formatGaps(dataRange.data[key]);

    // If we have prediction data, add it to the data
    if (dataRange.prediction[key]) {
      // Make a gap
      data.push([new Date().getTime(), null]);
      data = data.concat(dataRange.prediction[key]);
    }

    return data;
  } 

  private generateFahrplanDataSet(dataRange: DataRange, negativeFCR: boolean=false): number[][] {
    const fahrplan = dataRange.fahrplan;
    const actions = [];
    const start = new Date().getTime();
    // Iterate through the fahrplan, creating an array of 'actions' ordered by ts
    for (let row of fahrplan) {

      // Not a valid fahrplan type
      if (PowerLineChartComponent.CONFIRMED_FAHRPLAN_TYPES.indexOf(row.type) < 0) {
        continue;
      }

      // Check whether FCR needs to be negative
      let powerOffset = (negativeFCR && row.type == FahrplanType.FCR_APPROVED_TYPE) ? 
                          -1 * row.power : 
                          row.power; 

      // Insert first 'action'
      let newAction = [row.start, powerOffset];
      let inserted = false;
      let i = 0;
      for (; i < actions.length; i++) {
        // Compare timestamps
        if (newAction[0] < actions[i][0]) {
          // Insert at index
          actions.splice(i, 0, newAction);
          inserted = true;
          break;
        }
      }
      if (!inserted) {
        actions.push(newAction);
      }
      // Insert counter 'action' when duration ends
      let counterAction = [row.start + row.dur, (-1*powerOffset)];
      inserted = false;
      // Start loop where last finished
      for (; i < actions.length; i++) {
        // Compare timestampts
        if (counterAction[0] < actions[i][0]) {
          // Insert at index
          actions.splice(i, 0, counterAction);
          inserted = true;
          break;
        }
      }
      if (!inserted) {
        actions.push(counterAction);
      }
    }

    // Iterate through the actions and change value to a running count
    let value = 0;

    for (let i = 0; i < actions.length; i++) {
      value += actions[i][1];
      actions[i][1] = value;
      // If ts is the same as the previous; remove the previous
      if (i > 0 && (actions[i][0] == actions[i - 1][0])) {
        actions.splice(i - 1, 1);
        i -= 1;
      }
    }

    if (actions.length > 0 && actions[0][0] < dataRange.from) {
      actions[0][0] = dataRange.from;
    }

    if (actions.length < 1 || actions[0][0] > dataRange.from) {
      actions.unshift([dataRange.from, 0]);
    }

    if (actions[actions.length - 1][0] < dataRange.to) {
      actions.push([dataRange.to, 0]);
    }

    return actions;
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.zoom && this.swarmData != null) {
      this.handleNewZoom(this.chart, this.zoom, this.swarmData.from, this.swarmData.to);
      return;
    }

    // The swarmname has changed, set the graph to load
    if (this.swarmData) {
      if (this.chartVisible == false) {
        this.initChart(this.swarmData);
      } else {
        this.onDataUpdate(this.swarmData);
      }
    }
  }

  ngOnDestroy() {
    // Destroy chart
    if (this.chart != undefined) {
      this.chart.destroy();
    }
  }

  initChart(dataRange: DataRange): void {
    // Set date ranges on x axis
    this.chartOptions.xAxis['min'] = dataRange.from;
    this.chartOptions.xAxis['max'] = dataRange.to;
    // Loop through keys of data to be in graph
    for (let key of PowerLineChartComponent.REQUIRED_DATA) {
      let series = {
        id: key,
        name: key,
        data: this.extractData(dataRange, key),
        color: PowerLineChartComponent.KEYS_TO_COLOURS[key],
        lineWidth: PowerLineChartComponent.KEY_LINE_WIDTHS[key] || 2
      };

      // Add series to chart options
      this.chartOptions.series.push(series);
    }

    
    // Generate fahrplan data sets
    let posFahrplanDataSet = [];

    if (dataRange.info.managed) {
      posFahrplanDataSet = this.generateFahrplanDataSet(dataRange);
    }

    let posFahrplanSeries = {
      id: 'posFahrplan',
      name: 'fahrplan',
      data: posFahrplanDataSet,
      step: 'left',
      color: '#434348',
      lineWidth: PowerLineChartComponent.KEY_LINE_WIDTHS['fahrplan']
    };
    this.chartOptions.series.push(posFahrplanSeries);

    let negFahrplanDataSet = [];

    if (dataRange.info.managed) {
      negFahrplanDataSet = this.generateFahrplanDataSet(dataRange, true);
    }

    let negFahrplanSeries = {
      id: 'negFahrplan',
      name: 'fahrplan',
      data: negFahrplanDataSet,
      step: 'left',
      color: '#434348',
      lineWidth: PowerLineChartComponent.KEY_LINE_WIDTHS['fahrplan']
    };
    this.chartOptions.series.push(negFahrplanSeries);

    this.initialiseFuturePlots(this.chartOptions, dataRange.from, dataRange.to);
    this.chartVisible = true;
  }

  onGraphLoad(chartInstance): void {
    // Save original instance of chart
    this.chart = chartInstance;
  }

  onDataUpdate(dataRange: DataRange): void {
    this.redrawFuturePlots(this.chart, dataRange.from, dataRange.to);

    // Update the chart data
    for (let key of PowerLineChartComponent.REQUIRED_DATA) {
      const series = this.chart.get(key);

      // Set data of series to new data array without redraw
      series.setData(this.extractData(dataRange, key), false);
    }

    if (dataRange.info.managed) {
      // Update the fahrplan data
      this.chart.get('posFahrplan').setData(this.generateFahrplanDataSet(dataRange), false);
      this.chart.get('negFahrplan').setData(this.generateFahrplanDataSet(dataRange, true), false);
    } else {
      // Otherwise, no fahrplan data
      this.chart.get('posFahrplan').setData([]);
      this.chart.get('negFahrplan').setData([]);
    }

    if (!this.isZoomed()) {
      // Update the date ranges on x axis, as not zoomed in
      this.chart.xAxis[0].setExtremes(dataRange.from, dataRange.to);
    } else {
      this.chart.redraw();
    }
  }
}
