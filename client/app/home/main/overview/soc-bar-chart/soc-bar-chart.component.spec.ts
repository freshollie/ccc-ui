import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocBarChartComponent } from './soc-bar-chart.component';

describe('SocBarChartComponent', () => {
  let component: SocBarChartComponent;
  let fixture: ComponentFixture<SocBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
