import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { Device } from '../../../../shared/services/device/device.service';


@Component({
  selector: 'app-soc-bar-chart',
  templateUrl: './soc-bar-chart.component.html',
  styleUrls: ['./soc-bar-chart.component.css']
})
export class SocBarChartComponent implements OnInit, OnDestroy, OnChanges {

  @Input() loading: boolean = true;
  @Input() devices: Device[];

  private chart: any;

  private static HISTOGRAM_STEP = 10;
  private static MAX_SOC = 60;
  private static MIN_SOC = 40;

  public createChart = false;

  public chartOptions = {
    plotOptions: {
      column: {
        animation: false,
        states: {
          hover: {
            enabled: false
          }
        },
      }
    },
    series: [{
      data: [],
      pointWidth: 15,
      borderWidth: 0,
    }],
    time: {
      useUTC: true,
    },
    chart: {
      renderTo: 'soc-bar-chart',
      responsive: true,
      type: 'column'
    },
    legend: {
      enabled: false
    },
    xAxis: {
      tickInterval: 10,
      min: 0,
      max: 100,
      title: {
        text: 'Percentage (%)'
      },
      plotBands: [
      {
        from: 0,
        to: SocBarChartComponent.MIN_SOC,
        color: '#fff5f5',
      },
      {
        from: SocBarChartComponent.MIN_SOC,
        to: SocBarChartComponent.MAX_SOC,
        color: '#d5ffd9',
      },
      {
        from: SocBarChartComponent.MAX_SOC,
        to: 100,
        color: '#fff5f5',
      }
      ]
    },
    yAxis: {
      title: {
        text: 'Devices'
      }
    },
    tooltip: {
      enabled: false
    },
    title: {
      text: ''
    },
  };

  constructor() { }

  ngOnInit() {
    if (this.chart) {
      this.updateChart(this.devices)
    } else {
      this.initChart(this.devices);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.chart) {
      this.updateChart(this.devices)
    } else {
      this.initChart(this.devices);
    }
  }

  ngOnDestroy() {
    // Destroy chart
    if (this.chart != undefined) {
      this.chart.destroy();
    }
  }

  onGraphLoad(chartInstance): void {
    // Save original instance of chart
    this.chart = chartInstance;
    setTimeout(x => {
      this.chart.reflow();
    }, 0);
  }

  private initChart(devices: Device[]) {
    this.chartOptions.series[0].data = this.generateData(devices);
    
    // Finished updating options object, create chart.
    this.createChart = true;
  }

  private generateData(devices: Device[]) {
    const chartData = [];

    // Filter for devices which are active
    for (let device of devices) {
      if (device.data.state.length > 0 
          && device.data.state[0][1] == 1
          && device.data.soc.length > 0) {
        chartData.push(device.data.soc[0][1]);
      }
    }

    return this.generateHistogramData(chartData, SocBarChartComponent.HISTOGRAM_STEP);
  }

  private generateHistogramData(data, step) {
    let histo = {};
    let arr = [];

    // Group down
    for (let value of data) {
      let x = Math.floor(value / step) * step;
      if (!histo[x]) {
        histo[x] = 0;
      }
      histo[x]++;
    }

    // Make the histo group into an array
    for (let value in histo) {
      if (histo.hasOwnProperty(value)) {
        arr.push([parseFloat(value) + 5, histo[value]]);
      }
    }

    // Finally, sort the array
    arr.sort(function (a, b) {
      return a[0] - b[0];
    });

    return arr;
  }

  private updateChart(devices) {
    // Update series data
    this.chart.series[0].setData(this.generateData(devices));
    setTimeout(x => {
      this.chart.reflow();
    }, 0);
  }
}
