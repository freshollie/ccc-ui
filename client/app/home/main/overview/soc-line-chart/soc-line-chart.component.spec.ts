import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocLineChartComponent } from './soc-line-chart.component';

describe('SwarmOverviewGraphComponent', () => {
  let component: SocLineChartComponent;
  let fixture: ComponentFixture<SocLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
