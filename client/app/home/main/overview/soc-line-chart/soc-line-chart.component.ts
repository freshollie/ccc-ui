import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DataRange } from '../../../../shared/services/swarm/swarm.service';
import { ChartComponent } from '../../../../shared/components/chart/chart.component';

@Component({
  selector: 'app-soc-line-chart',
  templateUrl: './soc-line-chart.component.html',
  styleUrls: ['./soc-line-chart.component.css']
})
export class SocLineChartComponent extends ChartComponent implements OnInit, OnChanges, OnDestroy {

  @Input() loading: boolean = true;
  @Input() swarmData: DataRange;
  @Input() zoom;

  @Output() onZoom: EventEmitter<any> = new EventEmitter();

  private chart: any;
  public chartVisible = false;

  private static PRED_SOC_PLOT = 'pred-soc-area';

  private static INCLUDED_DATA = ['minSoc', 'maxSoc', 'soc'];

  private static DEFAULT_MAX_SOC = 60;
  private static DEFAULT_MIN_SOC = 40;

  private static MAX_SOC_KEY = "maxSoc";
  private static MIN_SOC_KEY = "minSoc";

  private static KEYS_TO_COLOURS = {
    'maxSoc': '#ff6060',
    'minSoc': '#ff6060',
    'soc': '#545bfd'
  }

  private static KEY_LINE_WIDTHS = {
    'maxSoc': 1.5,
    'minSoc': 1.5
  }

  public chartOptions = {
    plotOptions: {
      line: {
        animation: false,
        states: {
          hover: {
            enabled: false
          }
        },
        marker: {
          enabled: false,
        }
      },
      arearange: {
        animation: false,
        states: {
          hover: {
            enabled: false
          }
        },
        marker: {
          enabled: false,
        }
      }
    },
    series: [],
    chart: {
      renderTo: 'soc-line-chart',
      responsive: true,
      zoomType: 'x',
      animation: false,
      marginLeft: 75,
    },
    tooltip: {
      enabled: false
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'datetime',
      plotLines: [],
      plotBands: [],
      events: {
        setExtremes: (e) => {
          this.handleExtremesChanged(e, this.chart, this.swarmData.from, this.swarmData.to);
        }
      },
    },
    yAxis: [{
      min: 0,
      max: 100,
      gridLineColor: "rgba(0, 0, 0, 0.2)",
      tickInterval: 20,
      labels: {
        format: '{value}%'
      },
      title: {
        text: ''
      }
    }]
  };

  constructor() {
    super();
  }

  /**
   * Extract the data from the past data, and add the 
   * prediction data for that key, with a null space
   * @param dataRange 
   * @param key 
   */
  private extractData(dataRange: DataRange, key: string): number[][] {
    let data = [];

    // Real swarm past data
    if (dataRange.data[key]) {
      data = this.formatGaps(dataRange.data[key]);
    }

    const now = new Date().getTime();

    // Create future or current MAX and MIN soc bounds for prediction.
    // TODO: Prediction max and min soc
    if (key == SocLineChartComponent.MAX_SOC_KEY && dataRange.to > now) {
      let maxSoc = SocLineChartComponent.DEFAULT_MAX_SOC;

      const lastIndex = dataRange.data[SocLineChartComponent.MAX_SOC_KEY].length - 1;
      if (lastIndex > -1) {
        maxSoc = dataRange.data[SocLineChartComponent.MAX_SOC_KEY][lastIndex][1];
      }

      data.push([now, null]);
      data.push([now + 1, maxSoc]);
      data.push([dataRange.to, maxSoc]);

    } else if (key == SocLineChartComponent.MIN_SOC_KEY && dataRange.to > now) {
      
      let minSoc = SocLineChartComponent.DEFAULT_MIN_SOC;

      const lastIndex = dataRange.data[SocLineChartComponent.MIN_SOC_KEY].length - 1;
      if (lastIndex > -1) {
        minSoc = dataRange.data[SocLineChartComponent.MIN_SOC_KEY][lastIndex][1];
      }

      data.push([now, null]);
      data.push([now + 1, minSoc]);
      data.push([dataRange.to, minSoc]);

    } else if (dataRange.prediction[key]) {
      data.push([now, null]);
      data = data.concat(dataRange.prediction[key]);

    }

    return data;
  }

  /**
   * Create a prediction SOC area for the graph
   */
  private generatePredictionSocArea(data: DataRange): number[][] {
    let areaRange = [];

    const maxPredData = data.prediction.maxPredSoc;
    const minPredData = data.prediction.minPredSoc
    if (!minPredData ||
        !maxPredData ||
        maxPredData.length < 0) {
      return areaRange;
    }

    areaRange = minPredData.map((point, i) => {
      return [point[0], point[1], maxPredData[i][1]];
    });

    return areaRange;
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.zoom && this.swarmData != null) {
      this.handleNewZoom(this.chart, this.zoom, this.swarmData.from, this.swarmData.to);
      return;
    }

    if (this.swarmData) {
      if (this.chartVisible == false) {
        this.initChart(this.swarmData);
      } else {
        this.onNewData(this.swarmData);
      }
    }
  }

  ngOnDestroy() {
    // Destroy chart
    if (this.chart != undefined) {
      this.chart.destroy();
    }
  }

  private initChart(dataRange: DataRange) {
    // Set date ranges on x axis
    this.chartOptions.xAxis['min'] = dataRange.from;
    this.chartOptions.xAxis['max'] = dataRange.to;

    this.initialiseFuturePlots(this.chartOptions, dataRange.from, dataRange.to);

    this.chartOptions.series.push({
      showInLegend: false,
      id: SocLineChartComponent.PRED_SOC_PLOT,
      type: "arearange",
      lineWidth: 1.5,
      color: "#f95bfc",
      fillColor: "#daf3ff",
      fillOpacity: 0.01,
      data: this.generatePredictionSocArea(dataRange)
    });

    // Insert specified data
    for (let key of SocLineChartComponent.INCLUDED_DATA) {
      const series = {
        showInLegend: false,
        id: key,
        name: key,
        data: this.extractData(dataRange, key),
        color: SocLineChartComponent.KEYS_TO_COLOURS[key],
        lineWidth: SocLineChartComponent.KEY_LINE_WIDTHS[key] || 2
      };
      // Add series to chart options
      this.chartOptions.series.push(series);
    }

    this.chartVisible = true;
  }

  private onNewData(dataRange: DataRange): void {
    this.redrawFuturePlots(this.chart, dataRange.from, dataRange.to);
    this.chart.get(SocLineChartComponent.PRED_SOC_PLOT).setData(this.generatePredictionSocArea(dataRange));

    // Update chart data
    for (let key of SocLineChartComponent.INCLUDED_DATA) {
      let series = this.chart.get(key);
      // Set data of series to new data array without redraw
      series.setData(this.extractData(dataRange, key), false);
    }

    if (!this.isZoomed()) {
      // Update the date ranges on x axis
      this.chart.xAxis[0].setExtremes(dataRange.from, dataRange.to);
    } else {
      this.chart.redraw();
    }
  }

  onGraphLoad(chartInstance): void {
    // Save original instance of chart
    this.chart = chartInstance;
  }
}
