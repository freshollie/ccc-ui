import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempBarChartComponent } from './temp-bar-chart.component';

describe('TempBarChartComponent', () => {
  let component: TempBarChartComponent;
  let fixture: ComponentFixture<TempBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
