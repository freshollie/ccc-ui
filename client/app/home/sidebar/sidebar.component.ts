import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SwarmService, SwarmsObject } from '../../shared/services/swarm/swarm.service';
import { MatDialog } from '@angular/material';
import { SwarmManagementDialogComponent } from '../../shared/dialogs/swarm-management-dialog/swarm-management-dialog.component';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

  @Input() swarms: Object[];
  @Input() currentSwarm = "";
  @Input() manageEnabled = false;
  @Output() closeClicked = new EventEmitter<void>();

  constructor(
    private router: Router,
    private dialog: MatDialog,
  ) { }

  ngOnInit() { }

  ngOnChanges() {
    this.onNewSwarmTrees()
  }

  ngOnDestroy() { }

  private onNewSwarmTrees() {
    let currentSwarm;

    //while (true)
  }

  private close() {
    this.closeClicked.emit();
  }

  public toggleChildrenVisibility(swarm) {
    swarm.childrenVisible = !(swarm.childrenVisible);
  }

  public openSwarmManagement() {
    this.close();
    this.dialog.open(SwarmManagementDialogComponent);
  }
}
