import { Component, OnInit, Input, HostListener, OnDestroy } from '@angular/core';
import { AuthService } from '../../shared/services/auth/auth.service';
import { AlarmService, Alarm, AlarmSourceType, AlarmPrioritory } from '../../shared/services/alarm/alarm.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeComponent } from '../home.component';
import { ISubscription } from 'rxjs/Subscription';
import { forkJoin } from "rxjs/observable/forkJoin";
import * as moment from 'moment';

import { SearchService } from '../../shared/services/search/search.service';
import { TimeService } from '../../shared/services/time/time.service';
import { SwarmService, SwarmInfo, SwarmsObject } from '../../shared/services/swarm/swarm.service';
import { EventListenerService, OperatorChangeEvent } from '../../shared/services/events/event-listener.service';
import { MatDialog } from '@angular/material';
import { UserManagementDialogComponent } from '../../shared/dialogs/user-management-dialog/user-management-dialog.component';
import { Location } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit, OnDestroy {

  private static ALARM_NOISE_CRITICAL = "/assets/alarms/critical.mp3"

  public swarmsArray = [];
  public username: string = "";
  public alarmsVisible = false;
  public settingsVisible = false;
  
  public alarms: Alarm[] = [];

  public alarmRinging = false;
  public currentSwarmName: string;
  public online = true;

  public swarmManaged: boolean = false;
  public swarmReadonly: boolean = true;
  public topSwarm: boolean;
  public swarmVsn: string;

  public selectedDate = new Date();

  public currentSearch: string = "";
  public operator: string;
  public userOperating: boolean;
  public adminUser: boolean;
  public userManagementVisible = false;
  public language = "en";
  public defaultSwarm = "";

  private alarmPlayback: any = null;

  private alarmsSubscription: ISubscription;
  private swarmNameSubscription: ISubscription;
  private swarmInfoSubscription: ISubscription;
  private userDetailsSubscription: ISubscription;
  private operatorEventSubscription: ISubscription;

  private connectionStateSubscription: ISubscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private home: HomeComponent,
    private alarmService: AlarmService,
    private searchService: SearchService,
    private swarmService: SwarmService,
    private timeService: TimeService,
    private eventListenerService: EventListenerService,
    private dialog: MatDialog,
    private locationService: Location,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.online = true;

    this.userDetailsSubscription = this.authService.currentUserDetails.subscribe(userDetails => {
      this.username = userDetails.username;
      // Check for admin role
      this.adminUser = userDetails.roles[AuthService.ROLE_TYPE_ADMIN] != null;
    });

    this.alarmsSubscription = this.alarmService.currentAlarms.subscribe(newAlarms => {
      // Filter only the alarms we want (swarm alarms and high priority alarms that are unmuted)
      this.alarms = newAlarms.filter((alarm: Alarm) => {
        return alarm.priority == AlarmPrioritory.PRIORITY_HIGH
      });

      // Sort the alarms so that the unmuted alarms are at the top
      this.alarms.sort((a: Alarm, b: Alarm) => {
        return (a.muted == true ? 0 : -Infinity) || (b.time - a.time);
      });

      this.checkCriticalAlarms();
    });

    this.swarmNameSubscription = this.swarmService.currentSwarmName.subscribe(swarmName => {
      // Assign new swarm name
      this.currentSwarmName = swarmName;
      this.home.closeSidebar();
    });

    this.swarmInfoSubscription = this.swarmService.currentSwarmInfo.subscribe((info: SwarmInfo) => {
      this.swarmManaged = info.managed;
      this.swarmReadonly = info.readonly;
      this.topSwarm = info.vsn == null;
      this.swarmVsn = info.vsn;

      if (info.operator) {
        this.operator = info.operator.username;
      } else {
        this.operator = null;
      }

      this.userOperating = info.operating == true;
    });

    this.operatorEventSubscription = this.eventListenerService.operatorEvents.subscribe((event: OperatorChangeEvent) => {
      // The operator has changed in this swarm, update the swarm info
      if (event.swarms.indexOf(this.currentSwarmName) > -1) {
        this.swarmService.doSwarmInfoFetch();
      }
    });

    this.connectionStateSubscription = this.eventListenerService.connectionState.subscribe(connected => {
      this.online = connected;
    });

    this.timeService.currentStartTs.subscribe((time) => {
      this.selectedDate = new Date(time);
    });
  }

  ngOnDestroy() {
    if (this.alarmsSubscription) {
      this.alarmsSubscription.unsubscribe();
    }

    if (this.swarmNameSubscription) {
      this.swarmNameSubscription.unsubscribe();
    }

    if (this.swarmInfoSubscription) {
      this.swarmInfoSubscription.unsubscribe();
    }

    if(this.userDetailsSubscription) {
      this.userDetailsSubscription.unsubscribe();
    }

    if (this.operatorEventSubscription) {
      this.operatorEventSubscription.unsubscribe();
    }

    if (this.connectionStateSubscription) {
      this.connectionStateSubscription.unsubscribe();
    }
  }

  private checkCriticalAlarms() {
    this.swarmService.getSwarms()
      .subscribe((swarms) => {
        this.handleAlarmSound(swarms);
      });
  }

  private async handleAlarmSound(swarms: SwarmsObject) {
    let playSound = false;
    const operatingSwarms = new Set();
    let devices = [];

    for (const swarmName in swarms) {
      if (swarms[swarmName].info.operating == true) {
        operatingSwarms.add(swarmName);
        devices = devices.concat(swarms[swarmName].info.devices);
      }
    }

    const operatingDevices = new Set(devices);

    for (const alarm of this.alarms) {
      if (alarm.muted) {
        continue
      }

      if (alarm.sourceType == AlarmSourceType.SWARM) {
        if (operatingSwarms.has(alarm.source)) {
          playSound = true;
          break;
        }
      } else if (operatingDevices.has(alarm.source)) {
        playSound = true;
        break;
      }
    }

    if (!playSound && this.alarmPlayback != null) {
      this.alarmPlayback.pause();
      this.alarmRinging = false;
      this.alarmPlayback = null;

    } else if (playSound && this.alarmPlayback == null) {
      this.alarmRinging = true;
      this.alarmPlayback = new Audio()
      this.alarmPlayback.src = this.locationService.prepareExternalUrl(TopbarComponent.ALARM_NOISE_CRITICAL);
      this.alarmPlayback.load();

      this.alarmPlayback.addEventListener('ended', () => {
        this.alarmRinging = false;
        setTimeout(()=> {
          this.alarmPlayback = null;
          this.checkCriticalAlarms();
        }, 5000);
      }, false);

      // Handle browser rejections
      try {
        await this.alarmPlayback.play();
      } catch (e) {
        this.alarmPlayback = null;
        this.alarmRinging = true;
        setTimeout(() => {
          this.alarmRinging = false;
        }, 5000);
        setTimeout(() => {
          this.alarmPlayback = null;
          this.checkCriticalAlarms();
        }, 10000);
      }
    }
  }

  toggleSidebar() {
    this.home.toggleSidebar();
  }

  setSearch(search: string) {
    this.currentSearch = search;
    this.onSearchChange(search);
  }

  onSearchChange(input: string) {
    // If the input is not empty
    if (this.currentSearch != "") {
      // If we are not currently on the device page
      if (this.router.url.split("/")[2] != "devices") {
        // Navigate to the device page
        let url = "/" + this.router.url.split("/")[1] + "/devices";
        this.router.navigate([url]);
      }
    }

    // Update the current search in the search service
    this.searchService.updateSearch(this.currentSearch);
  }

  clearSearchClicked(event) {
    this.setSearch("");
  }

  toggleAlarms() {
    this.alarmsVisible = !this.alarmsVisible;
  }

  toggleSettings() {

    // If we are about to display settings get swarms.
    if (this.settingsVisible == false) {

      let swarmsRequest = this.swarmService.getSwarms();
      let preferencesRequest = this.authService.getUserPreferences();

      forkJoin([swarmsRequest, preferencesRequest])
      .subscribe((results: any[]) => {
        this.swarmsArray = Object.keys(results[0]);

        // If user has preferences use them instead of defaults.
        if(Object.keys(results[1]).length > 1) {
          this.language = results[1].language;
          this.defaultSwarm = results[1].defaultSwarm;
        } else {
          this.defaultSwarm = this.swarmsArray[0];
        }
      });
    }
    this.settingsVisible = !this.settingsVisible;
  }

  saveSettings() {
    let newLanguage = this.language;
    let newDefaultSwarm = this.defaultSwarm;
    this.authService.setUserPreferences(newLanguage, newDefaultSwarm)
      .subscribe(response => {}, 
        (err) => {
          if (err.status != 0) {
            this.toastrService.warning("Settings not saved", "Internal server error");
          } else {
            this.toastrService.warning("Settings not saved", "Connection error");
          }
      });
  }

  alarmClicked(alarm: Alarm) {
    // Hide the alarms dropdown
    this.alarmsVisible = false;

    // If it is a swarm alarm, navigate to that swarm's overview
    if (alarm.sourceType == AlarmSourceType.SWARM) {
      let swarmUrl = '/' + alarm.source + '/overview';
      this.router.navigate([swarmUrl]);

    } else if (alarm.sourceType == AlarmSourceType.DEVICE) {
      // Check swarms for device
      this.swarmService.getSwarms().subscribe(swarms => {
        for (let swarmName in swarms) {
          if (swarms[swarmName].info.managed == true) {
            for (let device of swarms[swarmName].info.devices) {
              if (device == alarm.source) {
                const swarmDevicesUrl = '/' + swarmName + '/devices';
                console.log(swarmDevicesUrl);
                this.router.navigate([swarmDevicesUrl]);
                this.setSearch(device);
                break;
              }
            }
          }
        }
      });
    }
  }

  alarmHovered(alarm: Alarm) {
    if (alarm.muted) {
      return 
    }

    // Mute the alarm
    this.alarmService.muteAlarm(alarm.id)
      .subscribe(() => {}, (err) => {
        if (err.status != 0) {
          this.toastrService.error("Could not mute alarm", "Internal server error");
        } else {
          this.toastrService.error("Could not mute alarm", "Connection error");
        }
      });
  }

  onLogoutClicked() {
    this.authService.logout().subscribe(() => {
      this.router.navigate(['/login']);
    }, (err: HttpErrorResponse) => {
      if (err.status == 403) {
        this.router.navigate(['/logout']);
      } else if (err.status == 500) {
        this.toastrService.error("Please try again", "Internal server error");
      } else {
        this.toastrService.error("Could not send logout request", "Connection error");
      }
    });
  }

  onLeftJumpClicked(): void {
    this.timeService.jumpBackByHour();
  }

  onRightJumpClicked(): void {
    this.timeService.jumpForwardByHour();
  }

  onCurrentButtonClicked(): void {
    this.timeService.updateToCurrent();
  }

  onDateSelected(): void {
    // Update time service
    let ts = this.selectedDate.getTime();
    this.timeService.setStartTs(ts);
  }

  takeControlOfSwarm(): void {
    this.swarmService.takeControlOfSwarm(this.currentSwarmName)
      .subscribe(() => {
        // Re-fetch swarm info
        this.swarmService.doSwarmInfoFetch();
      }, error => {
        if (error.status == 500) {
          this.toastrService.error("Could not take control", "Internal server error");
        } else {
          this.toastrService.error("Could not take control", "Connection error");
        }
      });
  }

  public openUserManagement() {
    this.dialog.open(UserManagementDialogComponent);
  }
}
