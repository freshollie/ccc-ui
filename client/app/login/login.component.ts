import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../shared/services/auth/auth.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationCancel } from '@angular/router';
import { ParticlesModule } from 'angular-particle';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { ISubscription } from 'rxjs/Subscription';
import { SwarmService, SwarmsObject } from '../shared/services/swarm/swarm.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public errorMessage: String;
  public loading = false;

  public width = 100;
  public height = 100;

  public particlesStyle =  {
    'background': '#003450',
    'position': 'fixed',
    'width': '100%',
    'height': '100%',
    'z-index': -1,
    'top': 0,
    'left': 0,
    'right': 0,
    'bottom': 0,
  };

  public particlesParams = {
    "particles": {
      "number": {
        "value": 80,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.5,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 3,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 1,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": false,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  };
  
  public fancy = true;

  private firstLoad = true;
  private previousPage: string;

  constructor(
    private auth: AuthService, 
    private router: Router, 
    private route: ActivatedRoute,
    private swarmsService: SwarmService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.previousPage = params["target"];

      if (params["bad"]) {
        this.onBadAccountConfig();
      } else {
        this.errorMessage = "";
      }

      this.loading = false;
    });
  }

  onBadAccountConfig() {
    this.errorMessage = "Account configuration error. Contact admin";
  }

  submitLogin(form) {
    this.loading = true;
    // Login, and then test if this account can access any swarms
    // If not, we refuse their login.
    this.auth.login(form.value.username, form.value.password)
      .mergeMap(() => this.swarmsService.getSwarms())
      .map((swarmsObject: SwarmsObject) => Object.keys(swarmsObject))
      .subscribe((swarms) => {
        if (swarms.length < 1) {
          this.auth.logout().subscribe();
          this.loading = false;
          this.onBadAccountConfig();
          return;
        }
        this.router.navigate([this.previousPage || '/']);
      }, error => {
        if (error.status == 401) {
          this.errorMessage = "Invalid username or password";
        } else if (error.status == 500) {
          this.errorMessage = "Internal server error";
        } else {
          this.errorMessage = "Could not connect to server";
        }
        this.loading = false;
      });
  }

  public toggleFancy() {
    this.fancy = !this.fancy;
  }
}
