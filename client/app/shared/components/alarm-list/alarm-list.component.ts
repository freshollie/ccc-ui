import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Alarm, AlarmSourceType, AlarmPrioritory } from '../../services/alarm/alarm.service';
import * as moment from 'moment';

@Component({
  selector: 'app-alarm-list',
  templateUrl: './alarm-list.component.html',
  styleUrls: ['./alarm-list.component.css']
})
export class AlarmListComponent implements OnInit {

  @Input() noAlarmsMessage: string;
  @Input() tooltip: string;
  @Input() clickable: boolean;
  @Input() readonly: boolean;

  @Input() showSource: boolean;

  @Input() wrapped: boolean;

  @Input() alarms: Alarm[];

  @Output() alarmClicked: EventEmitter<Alarm> = new EventEmitter();
  @Output() alarmHovered: EventEmitter<Alarm> = new EventEmitter();
  @Output() alarmCancelClicked: EventEmitter<Alarm> = new EventEmitter();
  @Output() alarmEmailClicked: EventEmitter<Alarm> = new EventEmitter();

  readonly AlarmPriority: typeof AlarmPrioritory = AlarmPrioritory;
  readonly AlarmSourceType: typeof AlarmSourceType = AlarmSourceType;

  public alarmsUnwrapped = false;

  constructor() {}

  ngOnInit() { }

  onAlarmClicked(alarm: Alarm) {
    this.alarmClicked.emit(alarm); 
  }

  onAlarmHovered(alarm: Alarm) {
    this.alarmHovered.emit(alarm);
  }

  onAlarmCancelClicked(alarm: Alarm) {
    this.alarmCancelClicked.emit(alarm); 
  }

  onEmailClicked(alarm: Alarm) {
    this.alarmEmailClicked.emit(alarm); 
  }

  formatTs(alarm: Alarm) {
    return moment(alarm.time).fromNow();
  }
}
