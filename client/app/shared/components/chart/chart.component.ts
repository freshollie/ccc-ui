import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DeviceData, EssStates } from '../../services/device/device.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  @Output() onZoom: EventEmitter<any> = new EventEmitter();

  private static CURRENT_TIME_PLOT = 'current-time';

  private static FUTURE_PLOT = 'future-area';

  // Max gap is 5 minutes
  private static MAX_GAP = 300000;

  public static STATE_TO_COLOR = {
    [EssStates.ESS_DOWN]: "rgba(255, 0, 0, 0.15)", // Red
    [EssStates.ESS_ALIVE_WITH_COMM_ERROR]: "rgba(255, 216, 0, 0.2)", // Yellow
    [EssStates.ESS_HB_ERROR]: "rgba(255, 216, 0, 0.2)", // Yellow
    [EssStates.ESS_ALIVE_PCP_NOT_POSSIBLE]: "rgba(0, 0, 255, 0.15)", // Yellow
    [EssStates.ESS_ADMINISTERED_DOWN]: "rgba(0, 0, 0, 0.1)", // Grey
    [EssStates.ESS_CHARGER_WAITING]: "rgba(0, 255, 0, 0.15)", // Blue
  }

  private currentZoom = { min: null, max: null };

  constructor() { }

  ngOnInit() {}

  /**
   * Add gaps to the given data, returning the same array with gaps if any
   * points are more than 5 minutes apart
   */
  formatGaps(data: number[][]) {
    if (!data || data.length < 2) {
      return (data);
    }

    let copy = null;
    let i = 1;
    let previousTs = data[0][0];
    let previousValue = data[0][1];
    const length = data.length;
    
    for (i ; i < length; i++) {
      const point = data[i];

      if ((point[0] - previousTs) > ChartComponent.MAX_GAP && 
          previousValue as any != "null") {

        if (copy == null) {
          copy = data.slice(0, i);
        }
        
        copy.push([previousTs + 100, null]);
      }
      
      if (copy != null)
        copy.push(point);
      
      previousTs = point[0];
      previousValue = point[1];
    }
    
    if (copy != null) {
      return copy;
    }

    return data;
  }

  getStateBands(states: number[][]) {
    if(states.length < 1) {
      return [];
    }

    let badBlocks = [];
    let currentBlock = [0, 0, 1];
    let lastTime = states[0][0];

    // Count blocks with status != 1 and store start/end times in an array.
    for (const state of states) {
      if(state[1] != currentBlock[2] && state[1] as any != "null") {
        let saveBlock = [0, 0, 0];

        if(currentBlock[2] != 1) {
          saveBlock[0] = currentBlock[0];
          saveBlock[1] = lastTime;
          saveBlock[2] = currentBlock[2];
          badBlocks.push(saveBlock);
        }

        currentBlock[0] = state[0];
        currentBlock[2] = state[1];

      }
      lastTime = state[0];
    }

    if (currentBlock[2] != 1) {
      currentBlock[1] = lastTime;
      badBlocks.push(currentBlock);
    }

    return badBlocks;
  }

  initialiseFuturePlots(options: any, dataFrom: number, dataTo: number) {
    const now = new Date().getTime();

    // Remove and re-add the plot band
    if (now <= dataFrom) {
      // Remove and re-add the plot line
      options.xAxis.plotLines.push({
        id: ChartComponent.CURRENT_TIME_PLOT,
        color: '#f5afaf',
        value: now,
        width: 2,
        zIndex: 1,
      });

      options.xAxis.plotBands.push({
        id: ChartComponent.FUTURE_PLOT,
        color: '#f5f5f5',
        from: dataFrom,
        to: dataTo,
      });

    } else if (now <= dataTo) {
      // Remove and re-add the plot line
      options.xAxis.plotLines.push({
        id: ChartComponent.CURRENT_TIME_PLOT,
        color: '#f5afaf',
        value: now,
        width: 2,
        zIndex: 1,
      });

      options.xAxis.plotBands.push({
        id: ChartComponent.FUTURE_PLOT,
        color: '#f5f5f5',
        from: (now),
        to: dataTo,
      });
    }
  }

  redrawFuturePlots(chart, dataFrom, dataTo) {
    const now = new Date().getTime();
    
    // Remove and re-add the plot line
    chart.xAxis[0].removePlotLine(ChartComponent.CURRENT_TIME_PLOT);

    // Remove and re-add the plot band
    chart.xAxis[0].removePlotBand(ChartComponent.FUTURE_PLOT);
    
    if (now <= dataFrom) {
      chart.xAxis[0].addPlotLine({
        id: ChartComponent.CURRENT_TIME_PLOT,
        color: '#f5afaf',
        value: now,
        width: 2,
        zIndex: 1,
      });

      chart.xAxis[0].addPlotBand({
        id: ChartComponent.FUTURE_PLOT,
        color: '#f5f5f5',
        from: dataFrom,
        to: dataTo,
      });
    } else if (now <= dataTo) {
      chart.xAxis[0].addPlotLine({
        id: ChartComponent.CURRENT_TIME_PLOT,
        color: '#f5afaf',
        value: now,
        width: 2,
        zIndex: 1,
      });
      
      chart.xAxis[0].addPlotBand({
        id: ChartComponent.FUTURE_PLOT,
        color: '#f5f5f5',
        from: (now),
        to: dataTo,
      });
    }
  }

  isZoomed() {
    return this.currentZoom.min != null;
  }

  /**
   * Chart extremes have been changed. Handle notifying
   * the zoom handler.
   */
  public handleExtremesChanged(event, chart, dataFrom, dataTo): any {
    const zoom = { min: event.min, max: event.max };

    // Zoomed out
    if (typeof event.min == 'undefined' || typeof event.max == 'undefined') {
      
      // We will handle out our zoom out
      event.preventDefault();

      // Save the zoom out, emit the result for other charts to listen for
      this.currentZoom = zoom;
      this.onZoom.emit({ zoom: zoom });

      chart.xAxis[0].setExtremes(dataFrom, dataTo);

    // Otherwise, if we zoomed then emit the new zoom and let the function continue
    } else if (event.trigger == "zoom" && (event.min != this.currentZoom.min || event.max != this.currentZoom.max)) {
      this.currentZoom = zoom;
      this.onZoom.emit({ zoom: zoom });
      chart.xAxis[0].setExtremes(event.min, event.max);
      if (!chart.resetZoomButton) {
        chart.showResetZoom();
      }
      event.preventDefault();
    }
  }

  public handleNewZoom(chart, zoom, dataFrom, dataTo) {
    // If there is no chart, don't bother
    if (!chart) {
      return;
    }

    // Otherwise, only handle the zoom if something has actually changed
    if (this.currentZoom.min != zoom.min || this.currentZoom.max != zoom.max) {
      this.currentZoom = zoom;

      // This is a zoom out
      if (zoom.min == null) {
        chart.xAxis[0].setExtremes(dataFrom, dataTo);
        if (chart.resetZoomButton) {
          chart.resetZoomButton.hide();
          chart.resetZoomButton = null;
        }
      } else {
        // Other wise, we have zoomed in so show the reset button
        if (!chart.resetZoomButton) {
          chart.showResetZoom();
        }

        chart.xAxis[0].setExtremes(this.currentZoom.min, this.currentZoom.max);
      }
    };
  }
}
