import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwarmHealthBarComponent } from './swarm-health-bar.component';

describe('SwarmHealthBarComponent', () => {
  let component: SwarmHealthBarComponent;
  let fixture: ComponentFixture<SwarmHealthBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwarmHealthBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwarmHealthBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
