import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Device, EssStates } from '../../services/device/device.service';

@Component({
  selector: 'app-swarm-health-bar',
  templateUrl: './swarm-health-bar.component.html',
  styleUrls: ['./swarm-health-bar.component.css']
})
export class SwarmHealthBarComponent implements OnInit, OnChanges {

  @Input() devices: Device[] = [];

  public numDevices: number;

  public numActive: number;
  public numActiveFcr: number;

  public numInactive: number;
  public numNotMatched: number;
  public noDevices: boolean;
  
  constructor() { }

  ngOnInit() {
    this.onDataChanged();
  }

  ngOnChanges() {
    this.onDataChanged();
  }

  onDataChanged() {
    this.numActive = 0;
    this.numActiveFcr = 0;
    this.numInactive = 0;
    this.numNotMatched = 0;

    let i = 0;
    const length = this.devices.length;

    this.numDevices = length;
    
    if (length < 1) {
      this.numInactive = 1;
      this.noDevices = true;
      return;
    }

    this.noDevices = false;

    // Count up statistics
    for (; i < length; i++) {
      const device = this.devices[i];
      
      // Ess is supposed to be active, but it's state is not active
      if (device.data.state.length > 0 && 
          device.data.state[0][1] != EssStates.ESS_ALIVE && device.data.admin[0][1] == 1) {
        this.numNotMatched++;
      } else {
        // Ess is active
        if (device.data.state.length > 0 && device.data.state[0][1] == EssStates.ESS_ALIVE) {
          this.numActive++;
          
          // Ess also has pcp enabled
          if (device.data.pcp.length && device.data.pcp[0][1] == 1) {
            this.numActiveFcr++;
          }
        } else {
          this.numInactive++;
        }
      }
    }
  }

}
