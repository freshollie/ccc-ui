import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ConfirmationDialogComponent>
  ) { }

  ngOnInit() {
    if (!this.data.cancelText) this.data.cancelText = "Cancel";
    if (!this.data.confirmText) this.data.confirmText = "Confirm";
  }

  public clickedConfirm(event) {
    this.dialogRef.close(true);
  }

  public clickedCancel(event) {
    this.dialogRef.close(false);
  }
}

