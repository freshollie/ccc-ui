import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicePreferencesDialogComponent } from './device-preferences-dialog.component';

describe('DevicePreferencesDialogComponent', () => {
  let component: DevicePreferencesDialogComponent;
  let fixture: ComponentFixture<DevicePreferencesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicePreferencesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicePreferencesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
