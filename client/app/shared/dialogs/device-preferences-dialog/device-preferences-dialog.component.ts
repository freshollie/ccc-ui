import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DevicePreferences } from '../../services/device/device.service';

@Component({
  selector: 'app-device-preferences-dialog',
  templateUrl: './device-preferences-dialog.component.html',
  styleUrls: ['./device-preferences-dialog.component.css']
})
export class DevicePreferencesDialogComponent implements OnInit {
  public preferences: DevicePreferences;
  public device: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<DevicePreferencesDialogComponent>
  ) { 
    this.preferences = Object.assign({}, data.preferences);
    this.device = data.device;
  }

  ngOnInit() {
  }

  public onSaveClicked() {
    this.dialogRef.close(this.preferences);
  }

  public onCancelClicked() {
    this.dialogRef.close(null);
  }

}
