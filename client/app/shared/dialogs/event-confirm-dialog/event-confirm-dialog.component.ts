import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FahrplanEvent } from '../../services/energy/energy.service';

@Component({
  selector: 'app-event-confirm-dialog',
  templateUrl: './event-confirm-dialog.component.html',
  styleUrls: ['./event-confirm-dialog.component.css']
})
export class EventConfirmDialogComponent implements OnInit {
  public events: FahrplanEvent[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<EventConfirmDialogComponent>
  ) {
    this.events = data.events;
   }

  ngOnInit() {
  }

  public clickedConfirm() {
    this.dialogRef.close(true);
  }

  public clickedCancel() {
    this.dialogRef.close(false);
  }
}
