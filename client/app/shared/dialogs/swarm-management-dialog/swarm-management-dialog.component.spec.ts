import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwarmManagementDialogComponent } from './swarm-management-dialog.component';

describe('SwarmManagementDialogComponent', () => {
  let component: SwarmManagementDialogComponent;
  let fixture: ComponentFixture<SwarmManagementDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwarmManagementDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwarmManagementDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
