import { Component, OnInit, OnDestroy } from '@angular/core';
import { SwarmService } from '../../services/swarm/swarm.service';
import { ISubscription } from 'rxjs/Subscription';
import { DeviceService, DevicesObject } from '../../services/device/device.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-swarm-management-dialog',
  templateUrl: './swarm-management-dialog.component.html',
  styleUrls: ['./swarm-management-dialog.component.css']
})
export class SwarmManagementDialogComponent implements OnInit {

  public swarms: any[] = [];
  public selectedSwarm: any = null;
  public availableDevices: string[] = [];
  public swarmDevices: string[] = [];
  public selectedDevice: string = null;
  public managerOpen: boolean = false;
  public loading: boolean = false;
  public selectingParentSwarm: boolean = false;

  public selectorConfig = {
    search: true, //true/false for the search functionlity defaults to false,
    height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
    placeholder:'Select', // text to be displayed when no item is selected defaults to Select,
    customComparator: ()=>{}, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
    limitTo: 5
  }

  // Create swarm form fields
  public createSwarmNameField: string = "";
  public createSwarmParentSwarmName: string = null;
  public createSwarmViewField: boolean = false;

  constructor(
    private swarmService: SwarmService,
    private deviceService: DeviceService
  ) { }

  ngOnInit() {

    this.refreshSwarmTree();
  }

  /**
   * Method to refresh the swarm tree
   */
  refreshSwarmTree(): void {

    this.loading = true;

    this.swarmService.getSwarmTrees().subscribe(swarms => {
      this.swarms = swarms;
      this.loading = false;
    })
  }

  /**
   * Method to refresh the device lists with latest data from the selected swarm
   */
  refreshDeviceLists(): void {

    if (this.selectedSwarm) {

      this.loading = true;

      Observable.forkJoin(
        this.deviceService.getDevices(),
        this.swarmService.getSwarmDevices(this.selectedSwarm.fullName)
      ).subscribe(values => {
        let allDevices: DevicesObject = values[0];
        let swarmDevices: string[] = values[1];
        let availableDevices: string[] = [];

        // Create a list of devices which are not already in the swarm
        // and are not virtual
        for (let device in allDevices) {
          if (swarmDevices.indexOf(device) < 0 && 
            allDevices[device].info.virt != true) {
            availableDevices.push(device);
          }
        }

        availableDevices.sort((a, b) => {
          return (a.slice(2) as any) - (b.slice(2) as any)
        });

        this.availableDevices = availableDevices;
        this.swarmDevices = swarmDevices;
        // Reset the selected device as we have new lists
        this.selectedDevice = null;
        this.loading = false;
      });
    }
  }

  /**
   * Method to handle the clicking of a swarm
   * @param swarm 
   */
  public swarmClicked(swarm): void {

    if (this.selectingParentSwarm) {
      this.selectParentSwarm(swarm);
    } else {
      this.viewEditSwarm(swarm);
    }
  }

  /**
   * Method to switch the manager view to create
   */
  public viewCreateSwarm(): void {

    this.resetCreateSwarmFields();
    this.selectedSwarm = null;
    this.managerOpen = true;
  }

  /**
   * Method to exit the manager view
   */
  public exitManagerView(): void {
    this.selectedSwarm = null;
    this.managerOpen = false;
  }

  /**
   * Method to toggle whether we're selecting a parent swar
   */
  public toggleParentSwarmSelection(): void {

    this.selectingParentSwarm = !this.selectingParentSwarm;
  }

  /**
   * Method to select a parent swarm and store it in the form field value
   * @param swarm 
   */
  public selectParentSwarm(swarm): void {

    this.createSwarmParentSwarmName = swarm.fullName;
    this.toggleParentSwarmSelection();
  }

  /**
   * Method to create a swarm from form details
   */
  public createSwarm(): void {

    if (this.createSwarmValid()) {

      this.loading = true;

      this.swarmService.createSwarm(
        this.createSwarmNameField,
        this.createSwarmParentSwarmName,
        this.createSwarmViewField
      ).subscribe(response => {

        // Refresh swarm tree
        this.refreshSwarmTree();
        // Close manager
        this.exitManagerView();
      });
    }
  }

  /**
   * Method returns whether the create swarm form is valid
   */
  public createSwarmValid(): boolean {

    return (
      !this.loading &&
      this.createSwarmNameField != ""
    );
  }

  /**
   * Method to easily reset the form's fields
   */
  public resetCreateSwarmFields(): void {

    this.createSwarmNameField = "";
    this.createSwarmParentSwarmName = null;
    this.createSwarmViewField = false;
  }

  /**
   * Method to switch the manager view to edit the given swarm
   * @param swarm
   */
  public viewEditSwarm(swarm: any): void {

    // Reset selected device
    this.selectedDevice = null;

    // Set the selected swarm
    this.selectedSwarm = swarm;

    // Reset lists of devices
    this.availableDevices = [];
    this.swarmDevices = [];
    this.refreshDeviceLists();

    this.managerOpen = true;
  }

  /**
   * Method called on click of an available device.
   * Device is handled depending on whether the swarm is managed.
   * @param device 
   */
  public availableDeviceClicked(device): void {

    if (this.selectedSwarm.managed) {
      this.selectedDevice = device;
    } else {
      // Transfer device from available devices to swarm devices
      this.swarmDevices.push(device);
      this.availableDevices.splice(this.availableDevices.indexOf(device), 1);
    }
  }

  /**
   * Method called on click of a device owned by the swarm
   * @param device 
   */
  public swarmDeviceClicked(device): void {
    
    if (!this.selectedSwarm.managed) {
      // Transfer the device from swarm devices to available devices
      this.availableDevices.push(device);
      this.swarmDevices.splice(this.swarmDevices.indexOf(device), 1);
    }
  }

  /**
   * Method to save changes to a managed swarm
   */
  public saveManagedSwarm(): void {

    if (this.saveManagedSwarmValid()) {
      // Start loading
      this.loading = true;
      // Add device to managed swarm
      this.swarmService
        .addDeviceToManagedSwarm(this.selectedSwarm.fullName, this.selectedDevice)
        .subscribe(response => {
          // Refresh swarm tree and device lists
          this.refreshDeviceLists();
        });
      }
  }

  /**
   * Method to save changes to an unmanaged swarm or view
   */
  public saveUnmanagedSwarm(): void {

    if (this.saveUnmanagedSwarmValid()) {
      // Start loading
      this.loading = true;
      // Set unmanaged swarm devices to currently selected devices
      this.swarmService
        .setUnmanagedSwarmDevices(this.selectedSwarm.fullName, this.swarmDevices)
        .subscribe(response => {
          // Refresh swarm tree and device lists
          this.refreshDeviceLists();
        });
    }
  }

  /**
   * Method to check the validity of changes to a managed swarm
   */
  public saveManagedSwarmValid(): boolean {

    return (this.selectedDevice != null && !this.loading);
  }

  /**
   * Method to check the validity of changes to an unmanaged swarm
   */
  public saveUnmanagedSwarmValid(): boolean {

    return (!this.loading);
  }

  /**
   * Method to delete the currently selected swarm
   */
  public deleteSelectedSwarm(): void {

    this.swarmService
      .deleteSwarm(this.selectedSwarm.fullName)
      .subscribe(response => {
        // Exit the edit swarm manager
        this.exitManagerView();
        // Refresh swarm tree
        this.refreshSwarmTree();
    })
  }

}
