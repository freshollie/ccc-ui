import { Component, OnInit, ViewChild } from '@angular/core';
import { UserManagementService, User } from '../../services/user-management/user-management.service';
import { SwarmService } from '../../services/swarm/swarm.service';
import { TreeModel, Ng2TreeSettings, TreeModelSettings, Tree } from 'ng2-tree';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-user-management-dialog',
  templateUrl: './user-management-dialog.component.html',
  styleUrls: ['./user-management-dialog.component.css']
})
export class UserManagementDialogComponent implements OnInit {

  @ViewChild('treeComponent') treeComponent;

  public users: User[] = [];
  public selectedUser: User;
  public treeSettings: Ng2TreeSettings = {
    rootIsVisible: false,
    showCheckboxes: true
  };
  public treeModelSettings: TreeModelSettings = {
    static: true,
    isCollapsedOnInit: false,
  }
  public tree: TreeModel = {
    settings: this.treeModelSettings,
    value: 'All Swarms',
    id: 0,
    children: [],
  };
  public usernameField: string = "";
  public passwordField: string = "";
  public demoField: boolean = false;
  public adminField: boolean = false;
  public swarmsField: string[] = [];

  public creatingUser: boolean;

  constructor(
    private userManagementService: UserManagementService,
    private swarmService: SwarmService,
  ) { }

  ngOnInit() {

    this.refreshUsers();

    // Get a list of swarms for editing roles
    this.swarmService.getSwarmTrees().subscribe(swarmTrees => {
      this.tree.children = this.formatSwarmTrees(swarmTrees);
    });

  }

  /**
   * Method to refresh the list of user objects used
   */
  private refreshUsers(): void {
    // Get a list of users to manage
    this.userManagementService.getUsers().subscribe(response => {

      let users: User[] = [];
      let usersObject: object = response['users'];

      for (let userId in usersObject) {
        let user: User = usersObject[userId];
        user.id = parseInt(userId);
        users.push(user);
        
        // If we find the currently selected user, update it
        if (this.selectedUser && this.selectedUser.id == user.id) {
          this.selectedUser = user;
        }
      }

      this.users = users;
    });
  }

  /**
   * Method that takes the swarm trees object and formats into a readable Tree model
   * @param swarmTrees
   */
  private formatSwarmTrees(swarmTrees: Object[]): TreeModel[] {

    if (swarmTrees.length == 0) {
      return [];
    }

    return swarmTrees.map(swarmTree => {
      return {
        value: swarmTree['name'],
        children: this.formatSwarmTrees(swarmTree['children']),
      };
    });
  }

  /**
   * Method checks whether a user is demo
   * @param user 
   */
  public isDemo(user: User): boolean {

    for (let role in user.roles) {
      if (user.roles[role]['readonly']) {
        return true;
      }
    }
    return false;
  }

  /**
   * Method checks whether a user is admin
   * @param user 
   */
  public isAdmin(user: User): boolean {
    return (user.roles.hasOwnProperty(AuthService.ROLE_TYPE_ADMIN));
  }

  /**
   * Method switches to the create user view
   */
  public showCreateUser(): void {
    this.resetForm();
    this.selectedUser = null;
    this.creatingUser = true;
  }

  /**
   * Method switches to the edit user view
   * @param user 
   */
  public selectUser(user: User): void {
    this.resetForm();
    this.creatingUser = false;
    this.selectedUser = user;
    // Match current demo state
    this.demoField = this.isDemo(user);
    // Match current admin state
    this.adminField = this.isAdmin(user);

    // Check all accessible swarms in the tree
    let accessibleSwarms = Object.keys(user.roles);

    if (this.treeComponent) {
      this.findAndSelectSwarms(accessibleSwarms, this.treeComponent.tree);
      this.updateSwarmsFieldFromTree(this.treeComponent.tree);
    } else {
      setTimeout(() => {
        this.findAndSelectSwarms(accessibleSwarms, this.treeComponent.tree);
        this.updateSwarmsFieldFromTree(this.treeComponent.tree);
      }, 0)
    }
  }

  /**
   * Should select swarms from a given list in the form [swarm1, swarm1.child, swarm2]
   * @param swarms 
   */
  private findAndSelectSwarms(swarms: string[], tree: Tree) {

    // Uncheck tree
    tree.checked = false;

    // Go through the object, uncheck if not matched, otherwise check node and all children
    for (let child of tree.children) {

      // If child exists in swarms list
      if (swarms.indexOf(this.generateSwarmNameFromTree(child).toLowerCase()) > -1) {
        child.checked = true;
        this.checkTreeChildren(child);

      } else {
        child.checked = false;
        this.findAndSelectSwarms(swarms, child);
      }
    }
  }

  /**
   * Method recursively loops through children and checks them
   * @param tree 
   */
  private checkTreeChildren(tree: Tree): void {
    for (let child of tree.children) {
      child.checked = true;
      this.checkTreeChildren(child);
    }
  }

  /**
   * Method recursively loops through children and unchecks them
   * @param tree 
   */
  private uncheckTreeChildren(tree: Tree): void {
    for (let child of tree.children) {
      child.checked = false;
      this.uncheckTreeChildren(child);
    }
  }

  /**
   * Method recursively loops up the parents of the tree and unchecks all of them
   * @param tree 
   */
  private uncheckTreeParents(tree: Tree): void {
    if (tree.parent != null) {
      tree.parent.checked = false;
      this.uncheckTreeParents(tree.parent);
    }
  }

  /**
   * Method generates a server friendly swarm name from a tree
   * @param tree
   */
  private generateSwarmNameFromTree(tree: Tree): string {
    let swarmName = tree.value;

    while (tree.parent != null) {
      tree = tree.parent;
      // Ignore root tree also
      if (tree.parent != null) {
        swarmName = tree.value + '.' + swarmName;
      }
    }
    return swarmName;
  }

  /**
   * Method recursively loops through an array of trees and generates a list of swarm names.
   * If a parent swarm is selected/checked then it will ignore the children
   * @param treeArray 
   */
  private generateSwarmListFromChildren(treeArray: Tree[]): string[] {
    let checkedSwarms = [];

    for (let tree of treeArray) {
      if (tree.checked) {
        checkedSwarms.push(this.generateSwarmNameFromTree(tree));
      } else {
        checkedSwarms = checkedSwarms.concat(this.generateSwarmListFromChildren(tree.children));
      }
    }
    return checkedSwarms;
  }

  private updateSwarmsFieldFromTree(tree: Tree): void {

    // Go to top of tree
    while (tree.parent != null) {
      tree = tree.parent
    }
    // Update list
    let swarmList = this.generateSwarmListFromChildren(tree.children);
    this.swarmsField = swarmList;
  }

  /**
   * Method called on click of a swarm
   * @param event 
   */
  public onSwarmClicked(event) {

    this.selectSwarmNode(event.node);
  }

  /**
   * Method operates selection logic for the swarm view
   * @param node 
   */
  public selectSwarmNode(node): void {

    // If tree currently unchecked
    if (node.checked == false) {
      // Check itself
      node.checked = true;
      // Recursively check all children
      this.checkTreeChildren(node);
    }

    // If tree currently checked
    else {
      // Uncheck itself
      node.checked = false;
      // Recursively uncheck all children
      this.uncheckTreeChildren(node);
      // Recursively uncheck all parents
      this.uncheckTreeParents(node);
    }

    this.updateSwarmsFieldFromTree(node);
  }

  /**
   * Method returns whether the create user form is valid
   */
  public createUserValid(): boolean {

    if (this.usernameField && this.passwordField && (this.swarmsField.length > 0) || this.adminField) {
      return true
    }
    return false;
  }

  /**
   * Method submits the create user form then handles the response
   */
  public saveCreateUserClicked(): void {
    if (!this.createUserValid()) {
      return;
    }

    this.userManagementService.createUser(
      this.usernameField,
      this.passwordField,
      this.demoField,
      this.adminField,
      this.swarmsField
    ).subscribe(() => {
      this.creatingUser = false;
      this.refreshUsers();
    });
  }

  /**
   * Method checks whether fields have had changes made
   */
  public editUserValid(): boolean {
    let selectedSwarms = this.swarmsField.map(x => x.toLowerCase());
    
    if (this.adminField) {
      selectedSwarms.push("admin");
    } else if (selectedSwarms.length < 1) {
      // Cannot have no selected swarms
      return false;
    }

    let roles = Object.keys(this.selectedUser.roles);

    return (
      this.demoField != this.isDemo(this.selectedUser) ||
      this.adminField != this.isAdmin(this.selectedUser) ||
      this.passwordField ||
      !this.swarmListsEqual(selectedSwarms, roles)
    ) ? true: false;
  }

  /**
   * Method resets the variables for both the create user form and the edit user form
   */
  private resetForm(): void {
    this.usernameField = "";
    this.passwordField = "";
    this.demoField = false;
    this.adminField = false;
    this.swarmsField = [];
  }

  /**
   * Method deletes the given user after click then resets the selected user
   */
  public deleteUserClicked(user): void {

    this.userManagementService.deleteUser(user.id).subscribe(response => {
      this.refreshUsers();
      this.selectedUser = null;
    });
  }

  /**
   * Method saves the edited users information
   */
  public saveEditUserClicked() {
    if (!this.editUserValid()) {
      return 
    }

    const updates = [];

    // Update password if a password has been input
    if (this.passwordField) {
      updates.push(
        this.userManagementService
          .updateUserPassword(this.selectedUser.id, this.passwordField)
      );
    }

    updates.push(
      this.userManagementService
        .updateUserRoles(
          this.selectedUser.id,
          this.demoField,
          this.adminField,
          this.swarmsField
        )
    )

    forkJoin(updates).subscribe(() => {
      this.passwordField = null;
      this.refreshUsers();
    }, (err) => {
      // TODO: error updating user
    });
  }

  /**
   * Method compares two lists basically.
   * @param a 
   * @param b 
   */
  private swarmListsEqual(a: string[], b: string[]): boolean {

    if (a.length != b.length) {
      return false;
    }

    // Only check the lists in one direction as cant have duplicate swarms
    for (let x of a) {
      if (b.indexOf(x) < 0) {
        return false;
      }
    }
    return true;
  }

}
