import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService, AuthDetails } from '../../services/auth/auth.service';
import { Observable, Subject } from 'rxjs';
import { LoginComponent } from '../../../login/login.component';
import { ErrorPageComponent } from '../../../error-page/error-page.component';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(public authService: AuthService, public router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authService
      .getAuth()
      .map((auth: AuthDetails) => {
        // We have auth!
        this.authService.saveUserDetails(auth);

        if (route.component == LoginComponent) {
          // Re-direct to dashboard
          this.router.navigate(['/']);
          console.log('auth guard returns false as already logged in');
          return false;
        } else if (route.component == ErrorPageComponent) {
          if (route.queryParams[ErrorPageComponent.AUTH_ERROR_KEY] != true) {
            console.log('auth guard returns true, as going to error screen with a non auth related error');
            return true;
          } else {
            console.log('auth guard returns false, as going to error screen but auth error is gone');
            this.router.navigate(['/']);
            return false;
          }

        }
        console.log('auth guard returns true');
        return true;
      }).catch((err) => {
        if (err.status != 401) {
          if (route.component != ErrorPageComponent) {
            this.router.navigate(['/error'], { 
              queryParams: { 
                target: state.url, 
                disconnected: err.status != 500, 
                [ErrorPageComponent.AUTH_ERROR_KEY]: true 
              }
            });
            console.log("Auth guard got error, going to error screen");
            return Observable.of(false);
          }
        } else {
          if (route.component != LoginComponent) {
            // Re-direct to login page
            this.router.navigate(['/login'], { queryParams: { target: state.url }});
            console.log('auth guard returns false');
            return Observable.of(false);
          }
        }
        return Observable.of(true);
      });
  }
}
