import { TestBed, async, inject } from '@angular/core/testing';

import { ManagedGuard } from './managed.guard';

describe('ManagedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagedGuard]
    });
  });

  it('should ...', inject([ManagedGuard], (guard: ManagedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
