import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SwarmService } from '../../services/swarm/swarm.service';

@Injectable()
export class ManagedGuard implements CanActivate {

  constructor(
    private swarmService: SwarmService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    // Get list of swarms
    return this.swarmService
      .getSwarms()
      .map(swarms => {

        let urlSwarm = state.url.split('/')[1];

        for (let swarm in swarms) {
          if (urlSwarm == swarm) {
            // Found requested swarm, check whether managed
            if (swarms[swarm].info.managed == true) {
              console.log('managed guard returns true');
              return true;
            }
          }
        }
        console.log('managed guard returns false');
        return false;
      })
      .catch(error => {
        // If not authorized, redirect to login
        if (error.status == 401) {
          console.log("Managed guard got unauthorised, going to login screen");
          this.router.navigate(['/login'], { queryParams: { target: state.url }});
        } else {
          console.log("Swarm guard got error, going to error screen");
          this.router.navigate(['/error'], { queryParams: { target: state.url, disconnected: error.status != 500 }});
        }
        console.log('manage guard returns false');
        return Observable.of(false);
      });
  }
}
