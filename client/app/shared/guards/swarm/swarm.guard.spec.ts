import { TestBed, async, inject } from '@angular/core/testing';

import { SwarmGuard } from './swarm.guard';

describe('SwarmGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SwarmGuard]
    });
  });

  it('should ...', inject([SwarmGuard], (guard: SwarmGuard) => {
    expect(guard).toBeTruthy();
  }));
});
