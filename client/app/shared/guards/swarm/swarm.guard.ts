import { AuthService, AuthPreferences } from '../../../shared/services/auth/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SwarmService, SwarmsObject } from '../../services/swarm/swarm.service';
import { forkJoin } from "rxjs/observable/forkJoin";
import { ErrorPageComponent } from '../../../error-page/error-page.component';

@Injectable()
export class SwarmGuard implements CanActivate {

  constructor(public swarmService: SwarmService, private router: Router, private authService: AuthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    let swarmsRequest = this.swarmService.getSwarms();
    let preferencesRequest = this.authService.getUserPreferences();

    return forkJoin([swarmsRequest, preferencesRequest])
      .map((results: any[]) => {
        const swarms: SwarmsObject = results[0];

        const swarmList = Object.keys(swarms);
        if (swarmList.length < 1) {
          this.authService.logout().subscribe(() => {
            this.router.navigate(['/login'], { queryParams: { bad: true }});
          }, (err) => {
            this.router.navigate(['/error'], { queryParams: { [ErrorPageComponent.AUTH_ERROR_KEY]: true }});
          });
          return false;
        }

        const preferences: AuthPreferences = results[1];

        // Check whether requested swarm is in list
        const targetSwarm = state.url.split('/')[1];
        
        if (swarmList.indexOf(targetSwarm) > -1) {
          // Swarm found, update latest swarm name and continue
          console.log("Swarm guard returns true")
          this.swarmService.updateSwarmName(targetSwarm);
          return true;
        }

        // Navigate to the swarm preferred by the user if it is set.
        if (preferences.defaultSwarm && swarmList.indexOf(preferences.defaultSwarm) > -1) {
          this.router.navigate([preferences.defaultSwarm]);
        } else {
          this.router.navigate([swarmList[0]]);
        }

        return false;

      }).catch(error => {
        // If not authorized, redirect to login
        if (error.status == 401) {
          console.log("Swarm guard got unauthorised, going to login screen");
          this.router.navigate(['/login'], {queryParams: { target: state.url }});
        } else {
          if (next.component != ErrorPageComponent) {
            console.log("Swarm guard got error, going to error screen");
            this.router.navigate(['/error'], {
              queryParams: { 
                target: state.url, 
                disconnected: error.status != 500, 
                [ErrorPageComponent.DATA_ERROR_KEY]: true 
              }
            });
          } else {
            return Observable.of(true);
          }
        }
        console.log('Swarm guard returns false');
        return Observable.of(false);
      });
  }
}
