import { Pipe, PipeTransform } from '@angular/core';
import { FahrplanEvents, FahrplanType } from '../../services/energy/energy.service';
import * as moment from 'moment';


@Pipe({
  name: 'formatFahrplan'
})
export class FormatFahrplanPipe implements PipeTransform {

  private englishApplicationTypes = {
    0: "Charge",
    1: "Discharge",
    2: "Control Reserve"
  };
  private englishStates = {
    0: "Confirmation required",
    1: "Requested",
    2: "Pending",
    3: "Approved",
    4: "Disapproved",
    5: "Cancelled"
  };

  transform(fahrplan: FahrplanEvents): any {

    let formattedFahrplan = [];
  
    for (let event of fahrplan.reverse()) {
      
      // Don't insert trades into the energy management list
      if (event.type == FahrplanType.INTRADAY_TRADE_TYPE || 
          event.type == FahrplanType.INTRADAY_VERSION_TYPE) {
        continue;
      }

      let applicationType;
      let state;

      switch (event.type) {
        case FahrplanType.CHARGE_DESIRED_TYPE:
          applicationType = event.power > 0 ? 0 : 1;
          state = 0;
          break;
        case FahrplanType.CHARGE_REQUESTED_TYPE:
          applicationType = event.power > 0 ? 0 : 1;
          state = 1;
          break;
        case FahrplanType.CHARGE_PENDING_TYPE:
          applicationType = event.power > 0 ? 0 : 1;
          state = 2;
          break;
        case FahrplanType.CHARGE_CONFIRMED_TYPE:
          applicationType = event.power > 0 ? 0 : 1;
          state = 3;
          break;
        case FahrplanType.CHARGE_DISAPPROVED_TYPE:
          applicationType = event.power > 0 ? 0 : 1;
          state = 4;
          break;
        case FahrplanType.CHARGE_CANCELLED_TYPE:
          applicationType = event.power > 0 ? 0 : 1;
          state = 5;
          break;
        case FahrplanType.FCR_REQUESTED_TYPE:
          applicationType = 2;
          state = 0;
          break;
        case FahrplanType.FCR_APPROVED_TYPE:
          applicationType = 2;
          state = 3;
          break;
        case FahrplanType.FCR_DISAPPROVED_TYPE:
          applicationType = 2;
          state = 4;
          break;
        case FahrplanType.FCR_CANCELLED_TYPE:
          applicationType = 2;
          state = 5;
          break;
      }

      let formattedRow = {
        applicationType: applicationType,
        serverDefinedType: event.type,
        state: state,
        englishApplicationType: this.englishApplicationTypes[applicationType],
        englishState: this.englishStates[state],
        id: event.id,
        start: moment(event.start).format("YYYY-MM-DD HH:mm"),
        dur: moment.duration(event.dur).humanize(),
        // Change W to MW
        power: (event.power / 1000000.0),
        reqStart: event.reqStart,
        reqDur: event.reqDur,
        source: event.source
      }

      formattedFahrplan.push(formattedRow);
    }

    // Sort by ID
    formattedFahrplan.sort((a, b) => {
      return b.id - a.id;
    });

    return formattedFahrplan;
  }

}
