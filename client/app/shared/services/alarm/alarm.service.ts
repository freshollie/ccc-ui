import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { Location } from '@angular/common';
import { EventListenerService, AlarmChangeEvent } from '../events/event-listener.service';
import { HTTP_TIMEOUT } from '../../../app.const';


/**
 * Represents the object
 * returned from the server side /alarms endpoint
 */
interface AlarmsAndSpecData {
  spec: Object;
  alarms: Map<string, Alarm>;
}

export enum AlarmType {
  CONTROL_CENTER_ALARM = 'CcAlm',
  SAFETY_ALARM = "SafAlm",
  WARNING_ALARM = 'WarAlm',
  MACHINE_ALARM = 'MacAlm',
  BUISNESS_LOGIC_ALARM = 'BusAlm',
  INFO_ALARM = "InoAlm",
  INTRADAY_ALARM = "IntradayAlm"
}

export enum AlarmSourceType {
  SWARM = "swarm",
  DEVICE = "device"
}

export enum AlarmPrioritory {
  PRIORITY_HIGH = "high",
  PRIORITY_MEDIUM = "medium",
  PRIORITY_LOW = "low"
}

/**
 * Used to provide info an instructions to
 * the front end of alarms
 */
export class AlarmMessage {
  info: string;
  instructions: string;

  constructor(info: string, instructions: string) {
    this.info = info;
    this.instructions = instructions;
  }

  static fromMessages(messages: string[]): AlarmMessage {
    return new AlarmMessage(messages[0], messages[1])
  }
}

/**
 * Represents a generated alarm, after messages have been added
 */
export interface Alarm {
  id: string;
  muted: boolean;
  source: string;
  sourceType: AlarmSourceType;
  time: number;
  type: AlarmType;
  val: number;
  priority: AlarmPrioritory;
  message: AlarmMessage;
};

@Injectable()
export class AlarmService {
  private static ALARMS_AND_SPEC_URL = '/data/alarms';

  // Define priority groups for known alarms
  private static TYPE_TO_PRIORITY = {
    [AlarmType.INFO_ALARM]: AlarmPrioritory.PRIORITY_LOW,
    [AlarmType.MACHINE_ALARM]: AlarmPrioritory.PRIORITY_MEDIUM,
    [AlarmType.WARNING_ALARM]: AlarmPrioritory.PRIORITY_MEDIUM,
    [AlarmType.BUISNESS_LOGIC_ALARM]: AlarmPrioritory.PRIORITY_MEDIUM,
    [AlarmType.CONTROL_CENTER_ALARM]: AlarmPrioritory.PRIORITY_MEDIUM,
    [AlarmType.SAFETY_ALARM]: AlarmPrioritory.PRIORITY_HIGH,
    [AlarmType.INTRADAY_ALARM]: AlarmPrioritory.PRIORITY_MEDIUM
  };

  private latestAlarms: Alarm[] = [];
  private alarmsSource = new ReplaySubject<Alarm[]>(1);
  public currentAlarms = this.alarmsSource.asObservable();

  constructor(private http: HttpClient, 
              private location: Location, 
              eventListenerService: EventListenerService) {
    // listen for alarm changes from the server, and react accordingly
    eventListenerService.alarmEvents.subscribe((event: AlarmChangeEvent) => {
      if (event.muted != null) {
        this.onAlarmMuteChanged(event.id, event.muted);
      } else if (event.removed != null) {
        this.onAlarmRemoved(event.id);
      } 
    });
  }

  /**
   * Returns a genereric alarm not found message for the 
   * @param alarmType 
   * @param alarmVal 
   */
  private generateMissingAlarmMessage(alarmData: Alarm) : AlarmMessage {
    // No translation preferences yet, use english by default
    let en = true;
    if (en) {
      return AlarmMessage.fromMessages([
        `<${alarmData.type}:${alarmData.val}> Description missing!`,
        ""
      ]);
    } else {
      return AlarmMessage.fromMessages([
        `<${alarmData.type}:${alarmData.val}> Beschreibung fehlt!`,
        ""
      ]);
    }
  }

  /**
   * Find the alarm message using the alarm
   * spec of this alarm type and the info of this alarm
   * @param alarmData 
   * @param alarmSpec 
   */
  private findAlarmMessage(alarmData: Alarm, alarmSpec: Object) : AlarmMessage {
    let alarmType = alarmData.type;
    let alarmVal = alarmData.val;
    
    if (!alarmSpec[alarmType]) {
      // Convert alarms without a correct type
      alarmType = alarmSpec['conversion'][alarmType][0];
      alarmVal = alarmSpec['conversion'][alarmData.type][1];
    }

    if (!alarmSpec[alarmType][alarmVal]) {
      return null;
    }

    // Since we dont have a preference for language yet, use English
    // todo
    const messages: string[] = alarmSpec[alarmType][alarmVal]["en"]

    return AlarmMessage.fromMessages(messages)
  }

  /**
   * Ask the server to for any alarm data, and put those alarms
   * in the current alarms observable
   */
  public doAlarmsFetch(): void {
    const url = this.location.prepareExternalUrl(AlarmService.ALARMS_AND_SPEC_URL);

    this.http.get(url)
      .timeout(HTTP_TIMEOUT)
      .subscribe((alarmsAndSpec: AlarmsAndSpecData) => {
        const activeAlarms = this.parseAlarms(alarmsAndSpec);

        this.latestAlarms = activeAlarms;
        this.alarmsSource.next(activeAlarms);
      });
  }
  
  private parseAlarms(alarmsAndSpec: AlarmsAndSpecData): Alarm[] {
    // Using the spec, create an alarm object for each alarm
    const activeAlarms: Alarm[] = [];

    const alarmsData = alarmsAndSpec.alarms;
    const alarmsSpec = alarmsAndSpec.spec;

    for (const alarmId in alarmsData) {
      const alarm = alarmsData[alarmId];

      // Assign ID to object
      alarm.id = alarmId;

      const sourceSpec = alarmsSpec[alarm.sourceType];

      let alarmType = alarm.type;

      if (!sourceSpec[alarmType]) {
        alarmType = sourceSpec['conversion'][alarmType][0];
      }

      // Decide the priority for the alarm
      if (alarm.sourceType == AlarmSourceType.SWARM && 
          alarmType == AlarmType.CONTROL_CENTER_ALARM) {
        // Priority always high for control center swarm alarms
        alarm.priority = AlarmPrioritory.PRIORITY_HIGH;
      } else {
        alarm.priority = AlarmService.TYPE_TO_PRIORITY[alarmType];  
      }

      let alarmMessage = this.findAlarmMessage(alarm, sourceSpec);
      if (!alarmMessage) {
        alarmMessage = this.generateMissingAlarmMessage(alarm);
      }

      alarm.message = alarmMessage;  
      activeAlarms.push(alarm);
    }

    // Order by newest first
    activeAlarms.sort((a: Alarm, b: Alarm) => {
      // Sort by the time, and if the same sort by the message to make sure they always stay in the same order
      return (b.time - a.time) || (a.message.info.localeCompare(b.message.info));
    });

    return activeAlarms;
  }

  private onAlarmRemoved(alarmId: string): void {
    const alarmList = this.latestAlarms;

    const alarmIndex = alarmList.findIndex((alarm: Alarm) => {
      return alarm.id == alarmId;
    });

    if (alarmIndex > -1) {
      console.log("On alarm removed");
      const newList = alarmList.slice();
      newList.splice(alarmIndex, 1);

      // Notify list change
      this.alarmsSource.next(newList);
      this.latestAlarms = newList;
    }
  }

  private onAlarmMuteChanged(alarmId: string, mute: boolean): void {
    console.log("Alarm mute changed: " + mute);
    const alarmList = this.latestAlarms;
    const alarm = alarmList.find((alarm: Alarm) => {
      return alarm.id == alarmId;
    });

    if (alarm) {
      alarm.muted = mute; 
      
      // Notify list change
      this.alarmsSource.next(alarmList);
    }
  }

  public getCurrentAlarms(): Alarm[] {
    return this.latestAlarms;
  }

  /**
   * Send a PATCH request to mute the alarm corresponding to the ID given.
   * @param alarm 
   */
  public muteAlarm(alarmId: string) {
    let url = this.location.prepareExternalUrl(AlarmService.ALARMS_AND_SPEC_URL + '/' + alarmId);

    return this.http.patch(url, {muted: true})
      .timeout(HTTP_TIMEOUT)
      .map((r: Response) => {
        this.onAlarmMuteChanged(alarmId, true);
        return true;
      })
      .catch((e: Response) => {
        if (e.status == 404) {
          return Observable.of(true);
        }
        return Observable.throw(e);
      });
  }

  /**
   * Send a DELETE request to cancel the alarm corresponding to the ID given.
   * @param alarmId
   */
  public cancelAlarm(alarmId: string): Observable<object> {
    const url = this.location.prepareExternalUrl(AlarmService.ALARMS_AND_SPEC_URL + '/' + alarmId);
    return this.http.delete(url)
      .timeout(HTTP_TIMEOUT)
      .map((r: Response) => {
        this.onAlarmRemoved(alarmId);
        return true;
      })
      .catch((e: Response) => {
        if (e.status == 404) {
          return Observable.of(true);
        }
        return Observable.throw(e);
      });
  }
}
