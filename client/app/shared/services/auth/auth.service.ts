import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { Location } from '@angular/common';
import { HTTP_TIMEOUT } from '../../../app.const';

export interface AuthRole {
  readonly: boolean;
}

export interface AuthRoles {
  [name: string]: AuthRole;
}

export interface AuthPreferences {
  language: string;
  defaultSwarm: string;
  alarmSounds: boolean;
}

export interface AuthDetails {
  username: string;
  roles: AuthRoles;
  preferences: AuthPreferences;
}

@Injectable()
export class AuthService {
  public static ROLE_TYPE_ADMIN = "admin";
  
  private static AUTH_ROUTE = "/auth";
  private static AUTH_PREFERENCES_ROUTE = "/auth/preferences";

  private userDetailsSource = new ReplaySubject<AuthDetails>(1);
  public currentUserDetails = this.userDetailsSource.asObservable();

  constructor(private http: HttpClient, private location: Location) { }

  public login(username, password) {
    const url = this.location.prepareExternalUrl(AuthService.AUTH_ROUTE);
    return this.http.post(url, { username: username, password: password }).timeout(HTTP_TIMEOUT);
  }

  public saveUserDetails(userDetails: AuthDetails): void {
    this.userDetailsSource.next(userDetails);
  }

  public setUserPreferences(language: string, defaultSwarm: string) {
    const body = {
      preferences: {
        language: language, 
        defaultSwarm: defaultSwarm
      }
    };

    const url = this.location.prepareExternalUrl(AuthService.AUTH_PREFERENCES_ROUTE);
    return this.http.put(url, body)
      .timeout(HTTP_TIMEOUT);
  }

  public getAuth(): Observable<AuthDetails> {
    const url = this.location.prepareExternalUrl(AuthService.AUTH_ROUTE);
    return this.http.get(url)
      .timeout(HTTP_TIMEOUT)
      .map((response: AuthDetails) => { 
        return response 
      });
  }

  public getUserPreferences(): Observable<AuthPreferences> {
    const url = this.location.prepareExternalUrl(AuthService.AUTH_PREFERENCES_ROUTE);
    return this.http.get(url)
      .timeout(HTTP_TIMEOUT)
      .map((response) => { 
        return response["preferences"]
      });
  }

  public logout() {
    const url = this.location.prepareExternalUrl(AuthService.AUTH_ROUTE);
    return this.http.delete(url)
      .timeout(HTTP_TIMEOUT);
  }
}
