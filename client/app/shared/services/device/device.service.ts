import { Injectable } from '@angular/core';
import { Alarm } from '../alarm/alarm.service';
import { BehaviorSubject, ReplaySubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { DeviceChangeEvent, EventListenerService } from '../events/event-listener.service';
import { HTTP_TIMEOUT } from '../../../app.const';


// Taken directly from CC-Logic
export enum EssStates {
  ESS_DOWN                   = 0, // init. value, ESS not operational (ESS in "undefined", probably error state)
  ESS_ALIVE                  = 1, // ESS up and running
  ESS_ALIVE_WITH_COMM_ERROR  = 2, // ESS is alive but shows communication errors
  ESS_HB_ERROR               = 3, // ESS should be active, but heartbeat has been dropped out for a long time
  ESS_ALIVE_PCP_NOT_POSSIBLE = 4, // frequency measurement failed, all other ESS functionality available
  ESS_ADMINISTERED_DOWN      = 5, // ESS is set to admin state "down" via cmd
  ESS_CHARGER_WAITING        = 6  // charger only and no forklift docked
}

export interface DeviceData {
  admin: number[][];
  pcp: number[][];
  state: number[][];
  soc: number[][];
  temp: number[][];
  prlPos: number[][];
  prlNeg: number[][];
  offset: number[][];
  grid: number[][];
  hhCons: number[][];
  mesFcr: number[][];
  calcFcr: number[][];
  pv: number[][];
}

// If more preferences are added
// device preferences must be extended
export interface DevicePreferences {
  lock: boolean;
  note: string;
}

export interface DeviceInfo {
  id: string;
  type: string;
  cap: number;
  preferences: DevicePreferences;
  virt: boolean; 
}

export interface Device {
  info: DeviceInfo;
  data: DeviceData;
}

export interface DeviceDataRange {
  data: DeviceData;
  from: number;
  to: number;
}

export interface DevicesObject {
  [id: string]: Device 
}

export interface DevicesJSON {
  devices: DevicesObject
}

@Injectable()
export class DeviceService {

  public static DEVICES_DATA = "/data/devices";

  // Latest device values
  private currentLatestData: Device[] = [];
  private latestDeviceDataSource = new ReplaySubject<Device[]>();
  public currentLatestDeviceData = this.latestDeviceDataSource.asObservable();

  constructor(private http: HttpClient, 
              private location: Location, 
              eventListenerService: EventListenerService) {
    
    // Listen for device changes from the server
    // and update accordingly
    eventListenerService.deviceEvents.subscribe((event) => {
      this.onDeviceChangeReceived(event);
    });
  }

  /**
   * Something has changed with a device on the server side
   * notify a new set of data, if it is relevant
   */
  private onDeviceChangeReceived(event: DeviceChangeEvent) {
    const currentDevicesData = this.currentLatestData;

    for (const device of currentDevicesData) {
      if (device.info.id == event.id) {
        // Assign the new data to the original object that
        // we have, if we have it
        if (event.newData != null) {
          device.data = Object.assign(device.data, event.newData);
        } else if (event.newInfo != null) {
          device.info = Object.assign(device.info, event.newInfo);
        }

        this.latestDeviceDataSource.next(currentDevicesData);

        break;
      }
    }
  }

  /**
   * Method to retrieve latest data for all devices in specified Swarm and update observable.
   * @param swarmName
   */
  public doLatestDeviceDataFetch(swarmName: string): void {
    const url = this.location.prepareExternalUrl(DeviceService.DEVICES_DATA);

    this.getDevices(swarmName)
      .subscribe((devicesObject) => {
        this.onDeviceDataRetrieved(devicesObject);
      }, error => {
        //TODO: error handling
        console.log(error);
      });
  }

  /**
   * Method takes server device data and updates deviceData list with Device objects.
   * @param data
   */
  private onDeviceDataRetrieved(devices: DevicesObject): void {
    const deviceList: Device[] = [];
    let deviceIds = Object.keys(devices);

    for (let i = 0; i < deviceIds.length; i++) {
      const deviceId = deviceIds[i];

      const device: Device = devices[deviceId];
      device.info.id = deviceId;

      deviceList.push(device);
    }

    this.currentLatestData = deviceList;
    this.latestDeviceDataSource.next(deviceList);
  }

  /**
   * Method to get device data from tsFrom and tsTo.
   * @param device 
   */
  public getDeviceData(device: string, tsFrom: number | null, tsTo?: number | null): Observable<DeviceDataRange> {
    console.log('getting device data for ' + device);

    const url = this.location.prepareExternalUrl(DeviceService.DEVICES_DATA + '/' + device);
    

    const params = {};

    if (tsFrom != null) {
      params['from'] = tsFrom;
    }

    if (tsTo != null) {
      params['to'] = tsTo;
    }

    return this.http.get(url, { params: params })
      .timeout(HTTP_TIMEOUT)
      .map(response => {
        return {
          data: response[device]['data'],
          from: tsFrom,
          to: tsTo
        };
      })
  }

  public setDevicePcpAdmin(deviceID: string, value: number) {
    const url = this.location.prepareExternalUrl(DeviceService.DEVICES_DATA + '/' + deviceID);
    const body = { data: { pcp: value ? 1: 0 }};

    return this.http.patch(url, body)
      .timeout(HTTP_TIMEOUT)
      .map(response => {
        return response["success"];
      });
  }

  public setDeviceAdminState(deviceID: string, value: number) {
    const url = this.location.prepareExternalUrl(DeviceService.DEVICES_DATA + '/' + deviceID);
    const body = { data: { admin: value ? 1: 0 }};

    return this.http.patch(url, body)
      .timeout(HTTP_TIMEOUT)
      .map(response => {
        return response["success"];
      });
  }

  public setDevicePreferences(deviceID: string, newPreferences: DevicePreferences) {
    const url = this.location.prepareExternalUrl(DeviceService.DEVICES_DATA + '/' + deviceID + '/preferences');
    const body = { preferences: newPreferences };

    return this.http.put(url, body)
      .timeout(HTTP_TIMEOUT)
      .map(response => {
        return response["success"];
      });
  }

  public getCurrentLatestDevicesData(): Device[] {
    return this.currentLatestData;
  }

  public getDevices(swarm?: string): Observable<DevicesObject> {
    const url = this.location.prepareExternalUrl(DeviceService.DEVICES_DATA);
    const params = {};

    if (swarm) {
      params['swarm'] = swarm;
    }

    return this.http.get(url, { params: params })
      .timeout(HTTP_TIMEOUT)
      .map(response => {
        return response["devices"];
      });
  }
}
