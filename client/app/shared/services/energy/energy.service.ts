import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SwarmService } from '../swarm/swarm.service';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Location } from '@angular/common';
import { HTTP_TIMEOUT } from '../../../app.const';

export enum FahrplanType {
  INTRADAY_TRADE_TYPE = "T",
  INTRADAY_VERSION_TYPE = "IV",

  CHARGE_DESIRED_TYPE = "UR",
  CHARGE_CONFIRMED_TYPE = "C",
  CHARGE_REQUESTED_TYPE = "R",
  CHARGE_PENDING_TYPE = "P",
  CHARGE_DISAPPROVED_TYPE = "D",
  CHARGE_CANCELLED_TYPE = "A",

  FCR_REQUESTED_TYPE = "N",
  FCR_APPROVED_TYPE = "M",
  FCR_DISAPPROVED_TYPE = "d",
  FCR_CANCELLED_TYPE = "a"
}

export interface FahrplanEvent {
  type: FahrplanType;
  id: number;
  start: number;
  dur: number;
  power: number;
  reqStart: number;
  reqDur: number;
  source: string;
}

export interface FahrplanEvents extends Array<FahrplanEvent> {}

@Injectable()
export class EnergyService {

  constructor(private http: HttpClient, private location: Location) { }

  /**
   * Method to get the fahrplan from the server between the specified timestamps
   * @param swarmName 
   * @param tsFrom 
   * @param tsTo 
   */
  public getFahrplan(swarmName: string, tsFrom: number, tsTo: number = null) {

    console.log('getting fahrplan');
    let url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/fahrplan');

    let params = {from: tsFrom.toString()};
    if (tsTo != null) {
      params['to'] = tsTo.toString();
    }

    return this.http.get(url, {params: params})
      .timeout(HTTP_TIMEOUT)
      .map(response => {
        return response['fahrplan'];
      });
  }

  /**
   * Method to send a charge request to the server, positive power is charge, negative is discharge.
   * @param swarmName 
   * @param source 
   * @param power 
   * @param startTs 
   * @param duration 
   */
  public makeChargeRequest(swarmName: string, source: string, power: number, startTs: number, duration: number) {

    console.log('making charge request');
    let url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/fahrplan');

    let body = {
      charge: true,
      source: source,
      reqStart: startTs,
      reqDur: duration,
      reqPower: power
    };

    return this.http.post(url, body).timeout(HTTP_TIMEOUT);
  }

  /**
   * Confirm multiple charge requests by Fahrplan Row
   */
  public confirmRequests(swarmName: string, events: FahrplanEvent[]): Observable<any> {
    console.log('confirming fahrplan requests')
    const baseUrl = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/fahrplan/');

    const requests = [];

    for (let event of events) {
      const body = { type: event.type == FahrplanType.CHARGE_DESIRED_TYPE ?
                           FahrplanType.CHARGE_REQUESTED_TYPE :
                           FahrplanType.FCR_APPROVED_TYPE 
                };
      
      requests.push(this.http.patch(baseUrl + event.id, body).timeout(HTTP_TIMEOUT));
    }

    return forkJoin(requests);
  }

  
  /**
   * 
   */
  public cancelFCREvent(swarmName: string, id: number): Observable<any> {
    console.log('Cancelling fcr event');

    const baseUrl = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/fahrplan/');
    
    return this.http.delete(baseUrl + id).timeout(HTTP_TIMEOUT);
  }

  /**
   * Cancel a set of requests
   */
  public cancelRequests(swarmName: string, events: FahrplanEvent[]): Observable<any> {
    console.log('Canceling request(s)');
    const baseUrl = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/fahrplan/');
    const requests = [];

    for (let event of events) {
      if (event.type == FahrplanType.FCR_REQUESTED_TYPE) {
        requests.push(this.http.patch(baseUrl + event.id, { type: FahrplanType.FCR_DISAPPROVED_TYPE }).timeout(HTTP_TIMEOUT));
      } else {
        requests.push(this.http.delete(baseUrl + event.id).timeout(HTTP_TIMEOUT));
      }
    }

    return forkJoin(requests);
  }

  /**
   * Method to make an FCR request to the server, positive power (W) is a charge, negative
   * is a discharge.
   * @param swarmName 
   * @param power 
   * @param startTs 
   * @param duration 
   */
  public makeFCRRequest(swarmName: string, power: number, startTs: number, duration: number) {

    console.log('making fcr request');
    let url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/fahrplan');

    let body = {
      fcr: true,
      reqStart: startTs,
      reqDur: duration,
      power: power,
    };

    return this.http.post(url, body).timeout(HTTP_TIMEOUT);
  }

  /**
   * Confirm a FCR request by ID.
   * @param swarmName 
   * @param requestId
   */
  public confirmFCRRequest(swarmName: string, requestId: number): Observable<any> {

    console.log('confirming fcr request')
    let url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/fahrplan/' + requestId);
    let body = { type: FahrplanType.FCR_APPROVED_TYPE };

    return this.http.patch(url, body).timeout(HTTP_TIMEOUT);
  }

  
}
