import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Observable, BehaviorSubject, Observer, Subject } from 'rxjs';
import { ISubscription } from 'rxjs/Subscription';
import { DeviceData, DeviceInfo } from '../device/device.service';



export interface AlarmChangeEvent {
  id: string;
  muted: boolean;
  removed: boolean;
}

export interface DeviceChangeEvent {
  id: string;
  newData: DeviceData;
  newInfo: DeviceInfo;
}

export interface FahrplanChangeEvent {
  swarm: string;
}

export interface OperatorChangeEvent {
  swarms: string[];
}

interface ChangeEventData extends FahrplanChangeEvent, 
                                  DeviceChangeEvent, 
                                  AlarmChangeEvent, 
                                  OperatorChangeEvent {}

interface SocketMessage {
  type: string,
  data: ChangeEventData;
}

/**
 * Listens for event changes from the server and propergates them to the client.
 * 
 * This is done using an authorised websocket
 */
@Injectable()
export class EventListenerService {
  private static SOCKET_PATH = "/socket";
  
  private static ALARM_EVENT_TYPE = "alarm";
  private static FAHRPLAN_EVENT_TYPE = "fahrplan";
  private static DEVICE_EVENT_TYPE = "device";
  private static OPERATOR_EVENT_TYPE = "operator";

  private socket;

  private alarmEventsSubject = new Subject<AlarmChangeEvent>();
  private deviceEventsSubject = new Subject<DeviceChangeEvent>();
  private fahrplanEventsSubject = new Subject<FahrplanChangeEvent>();
  private operatorEventsSubject = new Subject<OperatorChangeEvent>();
  
  private connectionStateSubject = new BehaviorSubject<boolean>(true);

  public alarmEvents = this.alarmEventsSubject.asObservable();
  public fahrplanEvents = this.fahrplanEventsSubject.asObservable();
  public deviceEvents = this.deviceEventsSubject.asObservable();
  public operatorEvents = this.operatorEventsSubject.asObservable();
  
  public connectionState = this.connectionStateSubject.asObservable();


  private socketListener: ISubscription;

  constructor(private location: Location) {}

  private getSocketURI(): string {
    const path = this.location.prepareExternalUrl(EventListenerService.SOCKET_PATH);
    const loc = window.location; 
    
    let uri;

    if (loc.protocol === "https:") {
      uri = "wss://";
    } else {
      uri = "ws://";
    }

    return uri + window.location.host + path;
  }

  public listen(): void {
    if (this.socketListener) {
      this.socketListener.unsubscribe();
    }

    const onConnectedSubject = new Subject();
    const onCloseSubject = new Subject();

    onConnectedSubject.subscribe(() => {
      this.connectionStateSubject.next(true);
    });

    onCloseSubject.subscribe(() => {
      this.connectionStateSubject.next(false);
    });

    this.socket = Observable.webSocket({
      url: this.getSocketURI(), 
      openObserver: onConnectedSubject,
      closeObserver: onCloseSubject
    })

    this.socketListener = this.socket
      .retry()
      .subscribe((msg: SocketMessage) => {
        this.processEvent(msg)
      });
  }

  public close(): void {
    if (this.socketListener) {
      this.socketListener.unsubscribe();
    }  
  }

  private processEvent(message: SocketMessage): void {
    switch(message.type) {
      case EventListenerService.ALARM_EVENT_TYPE:
        this.alarmEventsSubject.next(message.data);
        break;

      case EventListenerService.DEVICE_EVENT_TYPE: 
        this.deviceEventsSubject.next(message.data);
        break;

      case EventListenerService.FAHRPLAN_EVENT_TYPE:
        this.fahrplanEventsSubject.next(message.data);
        break;
      
      case EventListenerService.OPERATOR_EVENT_TYPE:
        this.operatorEventsSubject.next(message.data);
    }
  }
}