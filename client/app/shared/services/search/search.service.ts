import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';

@Injectable()
export class SearchService {

  private searchSource = new ReplaySubject<string>(1);
  currentSearch = this.searchSource.asObservable();

  constructor() { }

  public updateSearch(input: string) {
    this.searchSource.next(input);
  }

}
