import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subscription, ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FahrplanEvents } from '../energy/energy.service';
import { Location } from '@angular/common';
import { HTTP_TIMEOUT } from '../../../app.const';

export interface OperatorInfo {
  id: number;
  username: string;
}

export interface SwarmInfo {
  managed: boolean; // This swarm is a managed swarm
  devices: string[]; // A list of devices this swarm has
  vsn: string; // Virtual serial number of the device this SubSwarm represents
  readonly: boolean; // Is visible, but only for reading by this user
  operator: OperatorInfo; // The info of the person operating this swarm
  operating: boolean; // This user is operating the swarm
}

export interface SwarmData {
  soc: number[][];
  maxSoc: number[][];
  minSoc: number[][];
  optMaxChrg: number[][];
  maxChrg: number[][];
  maxDis: number[][];
  hhCons: number[][];
  mesFcr: number[][];
  calcFcr: number[][];
  grid: number[][];
  pv: number[][];
}

export interface SwarmPrediction {
  soc: number[][];
  maxPredSoc: number[][];
  minPredSoc: number[][];
  optMaxChrg: number[][];
  maxChrg: number[][];
  maxDis: number[][];
  hhCons: number[][];
  mesFcr: number[][];
  grid: number[][];
}

export interface Swarm {
  info: SwarmInfo;
  data: SwarmData;
  prediction: SwarmPrediction;
  fahrplan: FahrplanEvents;
}

export interface SwarmsObject {
  [swarm: string]: Swarm;
}

export interface DataRange {
  info: SwarmInfo;
  data: SwarmData;
  prediction: SwarmPrediction;
  fahrplan: FahrplanEvents;
  from: number;
  to: number;
}

@Injectable()
export class SwarmService {

  public static OPPOSITE_KEYS = [
    "hhCons",
    "calcFcr",
    "maxDis"
  ];
  
  public static PREDICTION_OPPOSITE_KEYS = [
    "mesFcr",
    "hhCons",
    "maxDis"
  ];

  public static SWARMS_DATA = "/data/swarms";

  private currentSwarm = "";

  // Current swarm name
  private swarmNameSource = new ReplaySubject<string>(1);
  public currentSwarmName = this.swarmNameSource.asObservable();

  // Current swarm info
  private swarmInfoSource = new ReplaySubject<SwarmInfo>(1);
  public currentSwarmInfo = this.swarmInfoSource.asObservable();

  // Most recent set of swarm values
  private latestSwarmDataSource = new ReplaySubject<SwarmData>();
  public currentLatestSwarmData = this.latestSwarmDataSource.asObservable();

  constructor(private http: HttpClient, private location: Location) { }

  /**
   * Method to fetch all info about the current swarm
   */
  public doSwarmInfoFetch(): void {
    let url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + this.currentSwarm);

    this.http.get(url)
      .timeout(HTTP_TIMEOUT)
      .subscribe(response => {
        this.swarmInfoSource.next(response[this.currentSwarm].info);
      });
  }

  /**
   * Method to update the swarm name observable.
   * @param swarmName 
   */
  public updateSwarmName(swarmName: string): void {
    if (this.currentSwarm != swarmName) {
      this.currentSwarm = swarmName;
      this.swarmNameSource.next(swarmName);
    }
  }

  /**
   * Method to get swarm data between tsFrom and tsTo
   * @param swarmName 
   * @param tsFrom 
   * @param tsTo 
   */
  public getSwarmData(swarmName: string, tsFrom: number, tsTo: number): Observable<DataRange> {
    
    console.log('getting swarm data');

    let url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName);
    let params = {from: tsFrom.toString(), to: tsTo.toString()};

    return this.http.get(url, {params: params})
      .timeout(HTTP_TIMEOUT)
      .map((response: SwarmsObject) => {
        return this.formatGraphData({
          info: response[swarmName].info,
          data: response[swarmName].data,
          prediction: response[swarmName].prediction,
          fahrplan: response[swarmName].fahrplan,
          from: tsFrom,
          to: tsTo
        });
      })
  }

  /**
   * Method to format graph data
   * @param dataRange 
   */
  private formatGraphData(dataRange: DataRange): DataRange {

    const swarmData = dataRange.data;
    const predictionData = dataRange.prediction;

    // Correct all max discharge to negative values
    for (const key of SwarmService.OPPOSITE_KEYS) {
      swarmData[key] = swarmData[key].map(x => [x[0], -(x[1])]);
    }

    for (const key of SwarmService.PREDICTION_OPPOSITE_KEYS) {
      dataRange.prediction[key] = dataRange.prediction[key].map(x => [x[0], -(x[1])]);
    }

    return {
      info: dataRange.info,
      data: swarmData,
      prediction: predictionData,
      fahrplan: dataRange.fahrplan,
      from: dataRange.from,
      to: dataRange.to
    };
  }

  /**
   * Method to get the latest values of given swarm and update observable.
   * @param swarmName 
   */
  public doLatestSwarmDataFetch(swarmName: string): void {

    let url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName);
    
    this.http.get(url)
      .timeout(HTTP_TIMEOUT)
      .subscribe((response: SwarmsObject) => {
        let swarmData = response[swarmName].data;
        // Update observable
        this.latestSwarmDataSource.next(swarmData);
      }, error => {
        console.error("Could not get swarm data");
        console.error(error);
      });
  }

  /**
   * Method to get latest data from all Swarms. (also used to get list of available swarms)
   */
  public getSwarms(): Observable<SwarmsObject> {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA);
    return this.http.get(url)
      .timeout(HTTP_TIMEOUT)
      .map((response) => {
        return response["swarms"];
      });
  }

  /**
   * Method to get the latest swarms and subswarms in a tree structure
   */
  public getSwarmTrees(): Observable<Object[]> {
    
    return this
      .getSwarms()
      .map(swarmsData => {

        let oldSwarmList = Object.keys(swarmsData);
        let swarms = [];
        // Loop through the flat swarm list 'testswarm.testchild1.testchild2'
        for (let oldSwarm of oldSwarmList) {
          // Loop through each section of swarm name
          let currentSwarmList = swarms;
          for (let part of oldSwarm.split(".")) {
            // If swarm doesn't exist, create it
            let exists = false;
            for (let currentSwarm of currentSwarmList) {
              if (part == currentSwarm['name']) {
                exists = true;
                break;
              }
            }
            if (!exists) {
              currentSwarmList.push({
                name: part,
                managed: swarmsData[oldSwarm].info.managed,
                devices: swarmsData[oldSwarm].info.devices,
                children: [],
                childrenVisible: false,
                routerLink: '../' + oldSwarm,
                fullName: oldSwarm
              });
            }
            // Now we know it exists, find it's child array (could speed up by storing the index)
            for (let currentSwarm of currentSwarmList) {
              if (part == currentSwarm['name']) {
                // Assign current swarm to search as the child array
                currentSwarmList = currentSwarm['children'];
                break;
              }
            }
          }
        }
        return swarms;
    });
  }

  /**
   * Method to take control of provided swarm
   * @param swarmName 
   */
  public takeControlOfSwarm(swarmName: string): Observable<boolean> {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/operating');

    // This put doesn't require any data
    return this.http.put(url, {})
      .timeout(HTTP_TIMEOUT)
      .map(response => {
        return response["success"];
      });
  }

  /**
   * Method to return the current swarm name
   */
  public getCurrentSwarmName(): string {
    return this.currentSwarm;
  }

  /**
   * Method to add a device to a managed swarm
   * @param swarmName 
   * @param deviceName 
   */
  public addDeviceToManagedSwarm(swarmName: string, deviceName: string): Observable<object> {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/devices');
    let body = { device: deviceName };

    return this.http 
      .post(url, body);
  }

  /**
   * Method to set the device list of an unmanaged swarm (view)
   * @param swarmName 
   * @param devices 
   */
  public setUnmanagedSwarmDevices(swarmName: string, devices: string[]) {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName + '/devices');
    let body = { devices: devices };

    return this.http
      .put(url, body);
  }

  /**
   * Method to get the swarm info of a given swarm name
   * @param swarmName 
   */
  public getSwarmInfo(swarmName: string): Observable<object> {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName);

    return this.http
      .get(url);
  }

  /**
   * Method returns a list of devices for a given swarm
   * @param swarmName 
   */
  public getSwarmDevices(swarmName: string): Observable<string[]> {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName);

    return this.http
      .get(url)
      .map(response => {
        return response[swarmName].info.devices;
      });
  }

  /**
   * Method creates a new swarm by sending a POST request to the server
   * @param swarmName 
   * @param parentSwarmName 
   * @param view 
   */
  public createSwarm(swarmName: string, parentSwarmName: string, view: boolean): Observable<object> {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA);
    let name = parentSwarmName ? parentSwarmName + '.' + swarmName : swarmName;
    let body = {
      name: name,
      managed: !view,
    };

    return this.http
      .post(url, body);
  }

  /**
   * Method deletes a swarm given the swarm name
   * @param swarmName 
   */
  public deleteSwarm(swarmName: string): Observable<object> {
    const url = this.location.prepareExternalUrl(SwarmService.SWARMS_DATA + '/' + swarmName);

    return this.http
      .delete(url);
  }
}
