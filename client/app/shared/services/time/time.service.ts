import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';


@Injectable()
export class TimeService {

  private static ONE_HOUR = 1000 * 60 * 60;

  // Start 12 hours behind
  private static DEFAULT_START = Date.now() - TimeService.ONE_HOUR * 12;

  private tsCurrent = true;
  private startTsSource = new BehaviorSubject(TimeService.DEFAULT_START);

  private onAdjustedSource = new Subject();

  public onAdjusted = this.onAdjustedSource.asObservable();
  public currentStartTs = this.startTsSource.asObservable();

  constructor() { }

  public emitTime(): void {
    this.startTsSource.next(this.startTsSource.getValue())
  }

  public updateToCurrent(): void {
    this.startTsSource.next(Date.now() - (TimeService.ONE_HOUR * 12));
    if (!this.tsCurrent) {
      this.notifyAdjusted();
    }
    
    this.tsCurrent = true;
  }

  public setStartTs(from: number): void {
    this.tsCurrent = false;
    this.startTsSource.next(from);
  }

  public isTsCurrent(): boolean {
    return this.tsCurrent;
  }

  public jumpBackByHour(): void {
    this.tsCurrent = false;
    let currentStartTs = this.startTsSource.getValue();
    this.startTsSource.next(currentStartTs - TimeService.ONE_HOUR);
    this.notifyAdjusted();
  }

  public jumpForwardByHour(): void {
    this.tsCurrent = false;
    let currentStartTs = this.startTsSource.getValue();
    this.startTsSource.next(currentStartTs + TimeService.ONE_HOUR);

    // Notify that the time has been adjusted
    this.notifyAdjusted();
  }

  public notifyAdjusted(): void {
    this.onAdjustedSource.next();
  }
}