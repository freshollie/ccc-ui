import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP_TIMEOUT } from '../../../app.const';

export interface User {
  id: number,
  username: string,
  roles: string[],
  preferences: object
};

@Injectable()
export class UserManagementService {

  public static USERS_DATA = "/data/users";

  constructor(private http: HttpClient) { }

  /**
   * Method to get all users from the server
   */
  public getUsers() {
    return this.http.get(UserManagementService.USERS_DATA).timeout(HTTP_TIMEOUT);
  }

  /**
   * Method to create a user with given permissions
   * @param username 
   * @param password 
   * @param admin 
   * @param swarms 
   */
  public createUser(username: string, password: string, demo: boolean, admin: boolean, swarms: string[]) {

    // Create roles body
    let body = {
      username: username,
      password: password,
      roles: {}
    };

    for (let swarm of swarms) {
      body.roles[swarm] = {
        readonly: demo
      };
    }

    if (admin) {
      body.roles['admin'] = {
        readonly: demo
      };
    }
    
    return this.http.post(UserManagementService.USERS_DATA, body).timeout(HTTP_TIMEOUT);
  }

  /**
   * Method to delete a user with given ID
   * @param userId 
   */
  public deleteUser(userId: number) {

    let url = UserManagementService.USERS_DATA + '/' + userId;
    return this.http.delete(url).timeout(HTTP_TIMEOUT);
  }

  /**
   * Method to update user password
   * @param userId 
   * @param password 
   */
  public updateUserPassword(userId: number, password: string) {

    let url = UserManagementService.USERS_DATA + '/' + userId + '/password';
    let body = {password: password};

    return this.http.put(url, body).timeout(HTTP_TIMEOUT);
  }

  /**
   * Method to create a roles object of all accessible swarms, admin and demo privileges
   * @param userId 
   * @param demo 
   * @param admin 
   * @param swarms 
   */
  public updateUserRoles(userId: number, demo: boolean, admin: boolean, swarms: string[]) {

    let url = UserManagementService.USERS_DATA + '/' + userId + '/roles';
    let body = {roles: {}};

    for (let swarm of swarms) {
      body.roles[swarm] = {
        readonly: demo
      };
    }

    if (admin) {
      body.roles['admin'] = {
        readonly: demo
      };
    }

    return this.http.put(url, body).timeout(HTTP_TIMEOUT);
  }

}
