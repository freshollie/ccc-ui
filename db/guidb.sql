CREATE DATABASE guidb;

GRANT ALL PRIVILEGES ON guidb.* TO 'gui'@'%' IDENTIFIED BY 'catervaDb';

USE guidb;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) UNIQUE NOT NULL,
    hashedpass VARCHAR(255) NOT NULL
);

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    rolename VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE user_roles (
    user_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    readonly BOOLEAN NOT NULL,
    PRIMARY KEY (user_id, role_id)
);

CREATE TABLE user_sessions (
    sid VARCHAR(255) NOT NULL PRIMARY KEY,
    sess TEXT NOT NULL,
    expired timestamp NOT NULL
);

CREATE TABLE user_preferences (
    user_id INTEGER NOT NULL,
    preference_key VARCHAR(255) NOT NULL,
    preference_value VARCHAR(255) NOT NULL
);

CREATE TABLE operators (
    user_id INTEGER NOT NULL,
    swarmname VARCHAR(255) NOT NULL,
    PRIMARY KEY (swarmname)
);

CREATE TABLE active_alarms (
    alarm_id VARCHAR(255) PRIMARY KEY NOT NULL,
    alarm_time TIMESTAMP NOT NULL,
    alarm_source VARCHAR(255) NOT NULL,
    alarm_source_type VARCHAR(255) NOT NULL,
    alarm_type VARCHAR(255) NOT NULL,
    alarm_val INTEGER NOT NULL,
    muted BOOLEAN NOT NULL
);

CREATE TABLE device_preferences (
    sn VARCHAR(255) PRIMARY KEY NOT NULL,
    preferences_json TEXT
);
