/**
 * CCC-UI server
 * 
 * Written by Oliver Bell
 * 
 * (c) Caterva GmbH 2018
 */

const express = require("express");
const morgan = require("morgan");
const path = require("path");
const bodyParser = require("body-parser");

const config = require("./config.js");
const { createLogger } = require("./logging.js");
const sessionManager = require("./authentication/session-manager.js");
const authManager = require("./authentication/auth-manager.js");

// Server side routes
const authApi = require("./routes/auth.js");
const dataApi = require("./routes/data/index.js");

const log = createLogger("app");

// Express framework use for all routing
const app = express();

// Log connections to logger
app.use(morgan("short", {
  stream: {
    write: function(message){
      log.verbose(message.trim())
    }
  }
}));

// Provide all front end angular as a static interface in ../dist
app.use(["/", "/cc"], express.static(path.join(__dirname, "../dist")));

// Serverside routes need session and authentication
app.use(sessionManager.getSessionParser());
authManager.initialise(app);

// Automatically convert any incoming form data into json
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));

app.use(["/cc/auth", "/auth"], authApi);
app.use(["/cc/data", "/data"], dataApi);

// Angular requires that all pages direct to the index.html
app.use("*", function(req, res) {
  res.sendFile(path.join(__dirname, "../dist/index.html"));
});

module.exports = app;
