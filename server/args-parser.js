/**
 * Caterva GmbH 2018
 * 
 * Written by Oliver Bell
 * 
 * Argument parsing, called at start up
 */

const minimist = require("minimist");
const config = require("./config.js");

function exitUsage() {
  console.log(`Usage: node server/server.js [ARGS]

ARGS:
  --port <port> Default: 3000

  --db <address> Default: localhost

  --db-port <port> Default: 5321

  --db-user <dir> Directory to read SN device registry values, and the
            list of valid SN devices from.

            Default: /archive/swarmdata

  --swarmcomm-dir <dir> Default: /var/swarmcomm

  --swarm-struc <address> Default: http://localhost:14700

  --use-struc <true|false> Enable the swarm-struc reading
               Default: false

  --single-archive-mode <true|false> Default: false. Uses a single table for all archive data
                            Used for testing purposes

  --log-level <verbose|debug|info|error> Level of log detail. Default: info
  
  --log-dir <dir> Directory to create log files to. If none, not written. Default: none

  --readonly Set endpoints will no longer function if true. Default: false`);

  process.exit(1);
}

function parse() {
  const args = minimist(process.argv.slice(2));
  for (let argKey in args) {
    if (!argKey || argKey == "_") {
      continue;
    }

    switch (argKey) {
      case "help":
        exitUsage();

      case "port":
        config.port = parseInt(args["port"]);
        continue;

      case "db":
        config.mariadb.connection.host = args["db"];
        config.postgres.connection.host = args["db"];
        continue;
      
      case "db-port":
        config.mariadb.connection.port = args["db-port"];
        config.postgres.connection.port = args["db-port"];
        continue;
      
      case "db-user":
        config.mariadb.connection.user = args["db-user"];
        config.postgres.connection.user = args["db-user"];
        continue;
      
      case "db-pass":
        config.mariadb.connection.password = args["db-pass"];
        config.postgres.connection.password = args["db-pass"];
        continue;
      
      case "db-type":
        config.dbType = args["db-type"];
        continue;

      case "redis":
        config.redis.host = args["redis"];
        continue;
      
      case "redis-port":
        config.redis.port = args["redis-port"];
        continue;

      case "swarmdb":
        config.swarmdb = args["swarmdb"];
        continue;
      
      case "swarm-struc":
        config.swarmStruc = args["swarm-struc"];
        continue;
      
      case "registry":
        config.registry = args["registry"];
        continue;
      
      case "prediction":
        config.predictionApi = args["prediction"];
        continue;

      case "fahrplan":
        config.fahrplanApi = args["fahrplan"];
        continue;
        
      case "log-level":
        config.logLevel = args["log-level"];
        continue;
      
      default:
        console.error(argKey + " is not a valid start argument");
        console.log("");
        exitUsage();
    }
  }
}

module.exports = { parse: parse };