/**
 * Auth Manager provides some helper methods for storing login information inside
 * the session data.
 */
const passport = require("passport");
const bcrypt = require("bcrypt");
const { Strategy } = require("passport-local");
const userdataManager = require("../data/userdata-manager.js");
const guiDb = require("../data/db/gui-db.js");

const logging = require("../logging.js");

/**
 * Helper class provides useful information for the auth methods
 * of the ccc-ui
 */
function AuthManager() {
  const log = logging.createLogger("AuthManager");

  class AuthManagerError extends Error {};

  /**
   * The login method.
   * 
   * Called when passport wants to verify our user credentials.
   * 
   * A success stores the userId in the user session under the .id attribute.
   */
  function verifyCredentials(username, password, done) {
    log.debug(`Verify credentials for ${username}`);
    try {
      (async function() {
        // See if this user exists
        const credentials = await guiDb.getUserCredentials(username);

        // If it does, and the hashed password is correct we put the userId in the users session
        if (credentials && await bcrypt.compare(password, credentials.hashedPass)) {
          log.debug(`Login success for ${username}`);
          return done(null, {id: credentials.id});
        };

        log.debug(`Invalid credentials for ${username}`);
        return done(null, false);
      })();
    } catch (e) {
      log.error(`Error verifying credentials: ${e.message}`);
      return done(new AuthManagerError("Error verifying credentials"), false);
    }
  }

  /** 
   * Initalise how passport authenticates a user, and how it reads and saves a user
   * to the sessions table
   * 
   * The provided the created passport middleware is then applied to the given app
   **/
  this.initialise = function(app) {
    log.debug("Initialising on app");

    // When saving the user to the session, save the whole user object
    passport.serializeUser(function(user, done) {
      done(null, user);
    });

    // When session has found the user object from the sessions table, we pass this
    // into the express req.user value.
    passport.deserializeUser(function(sessionUser, done) {
      done(null, sessionUser)
    });

    // This method is how the passport module checks our credentials
    // when we try to log in
    passport.use(new Strategy(verifyCredentials));

    // Add passport to the app
    app.use(passport.initialize());
    app.use(passport.session());

    userdataManager.createDefaultCredentails();
  }

  /**
   * Express middleware to ensure the client is logged in before allowing
   * them to pass through
   */
  this.authCheck = async function(req, res, next) {
    if (!req.isAuthenticated()) {
      return res.status(401).send({ error: "Unauthorised" });
    }

    if (!req.user.id) {
      req.logout();
      return res.status(401).send({ error: "Unauthorised" });
    }
     
    log.silly(`Ensuring auth of user ${req.user.id}`)
    try {
      if (!await userdataManager.existsUser(req.user.id)) {
        log.warn(`User ${req.user.id} was logged in when user doesn't exist. Removing auth session`);
        req.logout();
        return res.status(401).send({ error: "Unauthorised" });
      }
    } catch (e) {
      return res.status(500).send({ error: e.message });
    }
    
    req.getUserId = function() {
      return req.user.id;
    };
    
    next();
  }

  this.adminCheck = async function(req, res, next) {
    if (!await userdataManager.isAdmin(req.getUserId())) {
      return res.status(403).send({ error: "Forbidden" }); 
    }
  
    next();
  };

  /**
   * If user returned, success
   */
  this.login = async function(req, res) {
    try {
      return await new Promise(function(resolve, reject) {
        passport.authenticate('local', function(err, user, info) {
          if (err) {
            return reject(err);
          }
  
          if (!user) {
            return resolve();
          }
  
          req.login(user, function(err) {
            if (err) {
              return reject(err);
            }
            
            resolve(user);
          })
        })(req, res);
      });
    } catch (e) {
      log.error("Error during login", e);
      throw new AuthManagerError("Error verifying login");
    }
  }

  this.logout = async function(req) {
    req.logout();
  }

  this.isAdmin = async function(req) {
    return await userdataManager.isAdmin(req.getUserId());
  }
}

module.exports = new AuthManager();
