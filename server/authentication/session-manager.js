/**
 * Session manager uses a table in the guiDB to convert session tokens to sesssion data.
 * 
 * Session tokens are stored in the client as a cookie. By storing session data in a
 * database instead of the client, session data cannot be manipulated by the client.
 */

const session = require("express-session");
const logging = require("../logging.js");

const KnexSessionStore = require("connect-session-knex")(session);
const guiDb = require("../data/db/gui-db.js");

function SessionManager() {
  const TABLE = "user_sessions";
  const log = logging.createLogger("SessionManager");

  // Secret used for sessions
  const SECRET = "ccc-ui-v2.0";

  const store = new KnexSessionStore({
    knex: guiDb.getConnector(),
    tablename: TABLE
  });

  const sessionParser = session({
    saveUninitialized: true,
    resave: false,
    secret: SECRET,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 14 // 2 weeks
    },
    store: store
  });

  this.getSessionParser = function() {
    return sessionParser;
  }
}

module.exports = new SessionManager();
