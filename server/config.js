const config = {
  logLevel: 'debug',
  
  port: 2000,
  
  dbType: "mysql",

  postgres: {
    client: 'pg',
    connection: {
      host: 'localhost',
      port: '5432',
      user: 'caterva',
      password: 'catervaDb',
      database: 'guidb'
    },
    pool: { min: 0, max: 5 }
  },

  mariadb: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      port: '3306',
      user: 'gui',
      password: 'catervaDb',
      database: 'guidb'
    },
    pool: { min: 0, max: 5 }
  },

  redis: {
    port: "6379",
    host: "localhost"
  },

  swarmdb: "http://localhost:8081",
  swarmStruc: "http://localhost:14700",
  registry: "http://localhost:8081",
  predictionApi: "http://localhost:3013",
  fahrplanApi: "http://localhost:4003",

  alarmspecDir: __dirname + "/alarmspec"
}

module.exports = config;
