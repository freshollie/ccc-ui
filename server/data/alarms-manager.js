const crypto = require("crypto");
const Alarm = require("./db/models/Alarm.js");
const swarmdb = require("./db/swarmdb.js");
const guiDb = require("./db/gui-db.js");
const logging = require("../logging.js");
const eventBroadcaster = require("../socket/event-broadcaster.js");

/**
 * Handles all aspects of the alarm system in the gui
 * 
 * All endpoints should use this manager for enquiring
 * or changing alarm data
 */
function AlarmsManager() {
  const ALARM_SOURCE_TYPE_SWARM = "swarm";
  const ALARM_SOURCE_TYPE_DEVICE = "device";

  const ALCC1_ALARM = "ALCC1";
  const LLN0_ALARM = "LLN0";
  const STATE_VAL = "stVal";

  // Alarm types which will persist unless cleared
  const PERSISTENT_ALARM_TYPES = [
    "SafAlm"
  ];

  const PERSISTENT_ALARM_EXCEPTIONS = [
    "IntradayAlm"
  ]

  const ALARM_CACHE_INTERVAL = 60000;

  const log = logging.createLogger("AlarmsManager");

  class AlarmsManagerError extends Error {}

  this.runAlarmRetrievalLoop = async function() {
    log.info("Starting alarm retreival loop");
    while (true) {
      try {
        await this.getAlarms();
      } catch (e) {};
      
      await new Promise(function(resolve) {
        setTimeout(resolve, ALARM_CACHE_INTERVAL)
      }); 
    }
  }

  function makeAlarmId(source, type, val) {
    return crypto.createHash('md5').update(source + type + val.toString()).digest("hex");
  }

  /** 
   * Swarmdb stores alarms as bitmaps eg 0000100010001
   * where each bit corresponds to an alarm of a certain level. 
   * 
   * This method will decode an integer into a list of its alarm values
   */
  function decodeAlarmBitmap(bitmap) {
    let bits = bitmap.toString(2);
    let alarmVals = [];
    let bitVal = 0;
    
    for (let i = (bits.length - 1); i > -1; i--) {
      if (bits[i] == "1") {
        alarmVals.push(bitVal);
      }
      bitVal++
    }

    return alarmVals;
  }

  /**
   * Make the data received from swarmdb into
   * alarm objects
   */
  function makeAlarmsFromSwarmdbData(swarmdbAlarmData) {
    let alarms = {};
    for (let alarmData of swarmdbAlarmData) {
      let sourceType = ALARM_SOURCE_TYPE_DEVICE;
      let source = alarmData.name;

      // We use the swarm for source if the alarm type is a swarm
      if (alarmData.id == 0) {
        sourceType = ALARM_SOURCE_TYPE_SWARM;
        source = alarmData.swarm;
      }

      for (let alarmVal of decodeAlarmBitmap(parseInt(alarmData.val))) {
        // Generate the alarm id from these attributes
        const alarmId = makeAlarmId(source, alarmData.alrm, alarmVal);

        // Use the alarmIds to keep the alarms unique
        alarms[alarmId] = 
          new Alarm(
            alarmId, 
            new Date(parseInt(alarmData.st) * 1000), // Alarm times are in epoch
            source, 
            sourceType, 
            alarmData.alrm,
            alarmVal, 
            false
          );
      }
    }
      
    return Object.values(alarms);
  }

  function makeNewBitmap(bitmap, alarmVal) {
    let bitmask = 1;
    bitmask <<= alarmVal;
    return bitmap &~ bitmask;
  }

  function getDatakey(alarm) {
    if (alarm.getSourceType() == ALARM_SOURCE_TYPE_DEVICE) {
      return `${LLN0_ALARM}.${alarm.getType()}.${STATE_VAL}`;
    } else {
      return `${ALCC1_ALARM}.${alarm.getType()}.${STATE_VAL}`;
    }
  }

  function getSwarmdbSource(alarm) {
    if (alarm.getSourceType() == ALARM_SOURCE_TYPE_DEVICE) {
      return alarm.getSource();
    } else {
      return `${alarm.getSource()}.${swarmdb.SWCONTROL}`;
    }
  }
  
  /**
   * Attempts to remove the given alarm from swarmdb, by resetting this
   * alarms bit value is the alarms bitmap inside the swarmdb datapoint
   * 
   * @param {Alarm} alarm 
   */
  async function removeAlarmFromSwarmdb(alarm) {
    log.debug(`Removing ${alarm.getDetailsString()} from swarmdb`);
    // Swarmdb needs to know the source and the alarm key
    const alarmDatakey = getDatakey(alarm);
    const swarmdbAlarmSource = getSwarmdbSource(alarm);
    
    // Assume the alarm bitmap is 0
    let alarmBitmap = 0;

    // Get the alarm data
    const swarmdbAlarmData = await swarmdb.getLastValues([swarmdbAlarmSource], [alarmDatakey]);
    
    if (swarmdbAlarmData[swarmdbAlarmSource] && swarmdbAlarmData[swarmdbAlarmSource][alarmDatakey]) {
      // We have data about this alarm
      const responseBitmap = swarmdbAlarmData[swarmdbAlarmSource][alarmDatakey].value.mv;
      
      if (responseBitmap != null) {
        alarmBitmap = responseBitmap;
      }
    }

    const newBitmap = makeNewBitmap(alarmBitmap, alarm.getVal());
    return await swarmdb.setData(swarmdbAlarmSource, {[alarmDatakey]: newBitmap});
  }

  /**
   * Returns a dict of alarm objects, mapping their ids to their alarm properties,
   * using the current active alarms database and the new alarms from swarmdb. 
   * 
   * This method will automatically put the new alarms into the active alarms db 
   * and remove any alarms which are no longer required in the db.
   */
  this.getAlarms = async function() {
    log.debug("Getting active alarms from swarmdb");

    // Get all the current data
    let currentAlarms, activeAlarms;
    try {
      [currentAlarms, activeAlarms] = await Promise.all([
          swarmdb.getAlarms(),
          guiDb.getActiveAlarms()
      ]);
    } catch (e) {
      if (e instanceof swarmdb.SwarmdbConnectorError) {
        log.error("Error getting alarms from swarmdb.", e);
        throw new AlarmsManagerError("Error retreiving alarms");
      } else {
        log.error("Database error. Could not query active alarms", e);
        throw new AlarmsManagerError("Database error");
      }
    }

    currentAlarms = makeAlarmsFromSwarmdbData(currentAlarms);

    log.debug(`Found ${currentAlarms.length} current alarms`);
    log.debug(`Found ${activeAlarms.length} already active alarms`);

    // The alarms output map
    const alarms = {};
    
    const newAlarms = [];
    const removedAlarms = [];

    // Find the potentially already active version of each alarm
    for (let alarm of currentAlarms) {
      let activeVersion;

      // Using the active alarms list find the 
      for (let i = 0; i < activeAlarms.length; i++) {
        let activeAlarm = activeAlarms[i];
        if (activeAlarm.getId() == alarm.getId()) {
          // This alarm is already active, so don't need to add it to the database
          activeAlarms.splice(i, 1);
          
          // Keep the active version of this alarm for returning
          activeVersion = activeAlarm;
          break;
        }
      }

      if (!activeVersion) {
        // This alarm is new, so needs to be added to active the database
        log.debug(`Got new alarm: ${alarm.getDetailsString()}`);
        newAlarms.push(alarm);
      } else {
        alarm = activeVersion;
      }

      alarms[alarm.getId()] = alarm;
    }

    // We now have a list of alarms which no longer exist in the db, but are still in the active alarms table.
    // Check this list for alarms which can be removed.
    for (const alarm of activeAlarms) {
      // is this not a persistent alarm?
      if ((PERSISTENT_ALARM_TYPES.indexOf(alarm.getType()) < 0 
          && alarm.getSourceType() != ALARM_SOURCE_TYPE_SWARM)
            || PERSISTENT_ALARM_EXCEPTIONS.indexOf(alarm.getType()) > -1) {
        // If not, then this alarm can go
        log.debug(`Active alarm removed: ${alarm.getDetailsString()}`);
        removedAlarms.push(alarm.getId());
      } else {
        // Otherwise, persistent alarms must be manually removed, so keep it
        log.debug(`Keeping persistent inactive alarm: ${alarm.getDetailsString()}`);
        alarms[alarm.getId()] = alarm;
      }
    }

    log.debug("Updating guiDB alarms");
    try {
      await Promise.all([
        guiDb.insertActiveAlarms(newAlarms),
        guiDb.removeActiveAlarms(removedAlarms)
      ]);
    } catch (e) {
      log.error("Error updating guidb with new alarm information", e);
      throw new AlarmsManagerError("Database error");
    }
    
    log.debug(`Returning ${Object.keys(alarms).length} alarms`);
    return alarms;
  }

  this.getAlarm = async function(alarmId) {
    return await guiDb.getActiveAlarm(alarmId);
  }

  this.cancelAlarm = async function(alarmId) {
    const alarm = await guiDb.getActiveAlarm(alarmId);

    if (!alarm) {
      throw new AlarmsManagerError(alarmId + " doesn't exist");
    }

    await removeAlarmFromSwarmdb(alarm);
    await guiDb.removeActiveAlarms([alarmId]);
    eventBroadcaster.broadcastAlarmChange(alarmId, { removed: true });
  }

  this.setAlarmMute = async function(alarmId, muted) {
    let alarm = await guiDb.getActiveAlarm(alarmId);

    if (!alarm) {
      throw new AlarmsManagerError(alarmId + " doesn't exist");
    }
    
    await guiDb.setAlarmMute(alarmId, muted);
    eventBroadcaster.broadcastAlarmChange(alarmId, { muted: muted });
  }

  this.isSwarmAlarm = function(alarm) {
    return alarm.getSourceType() == ALARM_SOURCE_TYPE_SWARM;
  }
}

const manager = new AlarmsManager();
manager.runAlarmRetrievalLoop();

module.exports = manager;
