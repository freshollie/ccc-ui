const userdataManager = require("./userdata-manager.js");
const alarmsManager = require("./alarms-manager.js");
const swarmsManager = require("./swarms-manager.js");
const devicesManager = require("./devices-manager.js");

const logging = require("../logging.js");

/**
 * Allows data endpoints to filter for data which
 * the user will have access to.
 * 
 * Each method requires a userId in order to find out
 * what data can be used.
 */
function DataAccessManager() {
  const log = logging.createLogger("DataAccessManager");

  class DataAccessManagerError extends Error {}

  /**
   * Can the given roles access the given swarm
   * 
   * This works by including any subswarms of any swarm
   * e.g. swarm.name has access to swarm.name.test and swarm.name.test1
   */
  function findSwarmInRoles(swarm, roles) {
    swarm = swarm.toLowerCase();
    
    for (let role in roles) {
      // We can access if the role equals the swarm
      if (swarm == role.toLowerCase()) {
        return { readonly: roles[role].readonly };
      // But also works if the role is the parent of the given swarm
      } else if (swarm.startsWith(role.toLowerCase() + ".")) {
        return { readonly: roles[role].readonly };
      }
    }

    return false;
  }

  /**
   * Get all the swarms the user can access, with their permissions (readonly...)
   */
  async function getSwarmPermissions(userId) {
    let swarms, roles, isAdmin;
    try {
      [swarms, roles, isAdmin] = await Promise.all([
        swarmsManager.getSwarms(),
        userdataManager.getRoles(userId),
        userdataManager.isAdmin(userId)
      ]);
    } catch (e) {
      log.error(`Error getting swarm access for ${userId}`, e);
      throw e;
    }

    // filter the list of all swarms for ones that only
    // this user has some sort of access to.
    const swarmsAccess = {};

    for (const swarm of swarms) {
      let access = { readonly: false };
      if (!isAdmin) {
        access = findSwarmInRoles(swarm, roles);
      } 

      if (!access) {
        continue;
      }

      swarmsAccess[swarm] = access;
    }

    return swarmsAccess;
  }

  async function getSwarmPermission(userId, swarm) {
    let exists, roles, isAdmin;
    try {
      [exists, roles, isAdmin] = await Promise.all([
        swarmsManager.existsSwarm(swarm),
        userdataManager.getRoles(userId),
        userdataManager.isAdmin(userId)
      ]);
    } catch (e) {
      log.error(`Error getting swarm access for ${userId}`, e);
      throw e;
    }

    // Swarm doesn't exist, so no permission
    if (!exists) {
      return null;
    }

    if (isAdmin) {
      return { readonly: false };
    }

    return findSwarmInRoles(swarm, roles);
  }

  async function getManagableSwarms(userId) {
    log.debug(`Getting managable swarms for user ${userId}`);
    const swarmAccess = await getSwarmPermissions(userId);
    const manageable = [];

    for (const swarm in swarmAccess) {
      if (!swarmAccess[swarm].readonly) {
        manageable.push(swarm);
      }
    }

    log.debug(`User ${userId} can manage ${manageable.length} swarm(s)`);
    return manageable;
  }

  async function getAccessableSwarms(userId) {
    return Object.keys(await getSwarmPermissions(userId));
  }

  /**
   * Return all swarms which this user can access, along with the data current info
   * and data for the swarms. 
   * 
   * @param {*} userId 
   */
  this.getSwarms = async function(userId) {
    log.debug(`Getting swarm permissions for user ${userId}`);
    const swarmPermissions = await getSwarmPermissions(userId);
    const swarms = Object.keys(swarmPermissions);

    log.debug(`User ${userId} can access ${swarms.length} swarm(s)`);
    log.debug(`Getting data for the accessable swarms`);
    const [swarmsData, swarmOperators] = await Promise.all([
      swarmsManager.getSwarmsData(swarms),
      swarmsManager.getOperators(swarms)
    ]);
    
    for (const swarm in swarmsData) {
      if (swarmsData[swarm].info && swarmsData[swarm].info.managed) {
        swarmsData[swarm].info.readonly = swarmPermissions[swarm].readonly;
        swarmsData[swarm].info.operator = swarmOperators[swarm] || {};
        swarmsData[swarm].info.operating = swarmOperators[swarm] ? swarmOperators[swarm].id == userId: false;
      }
    }

    return swarmsData;
  }

  this.getSwarmData = async function(userId, swarm, from, to) {
    const swarmAccess = await getSwarmPermission(userId, swarm);

    if (!swarmAccess) {
      return null;
    }

    const [swarmsData, swarmOperators] = await Promise.all([
      swarmsManager.getSwarmsData([swarm], from, to),
      swarmsManager.getOperators([swarm])
    ]);
    
    if (swarmsData[swarm].info.managed) {
      swarmsData[swarm].info.readonly = swarmAccess.readonly;
      swarmsData[swarm].info.operator = swarmOperators[swarm] || {};
      swarmsData[swarm].info.operating = swarmOperators[swarm] ? swarmOperators[swarm].id == userId: false;
    }
    return swarmsData[swarm];
  }


  this.getSwarmFahrplan = async function(userId, swarm, from, to) {
    if (!await this.canAccessSwarm(userId, swarm)) {
      return null;
    }

    return (await swarmsManager.getFahrplanEvents([swarm], from, to))[swarm];
  }
  
  this.getSwarmFahrplanEvent = async function(userId, swarm, id) {
    if (!await this.canAccessSwarm(userId, swarm)) {
      return null;
    }

    return await swarmsManager.getFahrplanEvent(swarm, id);
  }

  this.requestSwarmFahrplanEvent = async function(userId, swarm, request) {
    if (!await this.canManageSwarm(userId, swarm)) {
      return false;
    }

    if (!await this.canOperateSwarm(userId, swarm)) {
      log.debug("User is not operator, cannot create event");
      return false;
    }

    return await swarmsManager.requestFahrplanEvent(swarm, request);
  }

  this.updateSwarmFahrplanEvent = async function(userId, swarm, id, newType) {
    if (!await this.canManageSwarm(userId, swarm)) {
      return false;
    }

    if (!await this.canOperateSwarm(userId, swarm)) {
      log.debug("User is not operator, cannot update event");
      return false;
    }

    await swarmsManager.updateFahrplanEvent(swarm, id, newType);
    return true;
  }

  this.cancelSwarmFahrplanEvent = async function(userId, swarm, id, newType) {
    log.debug(`Received request to cancel fahrplan event ${swarm}:${id}`);
    if (!await this.canManageSwarm(userId, swarm)) {
      return false;
    }

    if (!await this.canOperateSwarm(userId, swarm)) {
      log.debug("User is not operator, cannot cancel event");
      return false;
    }

    await swarmsManager.cancelFahrplanEvent(swarm, id, newType);
    return true;
  }

  this.updateSwarmOperator = async function(userId, swarm) {
    if (!await this.canManageSwarm(userId, swarm)) {
      return false;
    }

    await swarmsManager.setOperator(swarm, userId);
    return true;
  }

  /**
   * Get a list of devices which the given 
   * user has access to
   */
  this.getDevices = async function(userId, swarm = null) {
    let swarms = [];

    if (swarm == null) {
      // We don't care what permissions we have for each swarm
      swarms = await getAccessableSwarms(userId);
    } else {
      // We only want devices in this swarm
      swarms = [swarm];
    }

    // A map of to which swarms and which devices they have
    const swarmsDevices = await swarmsManager.getSwarmDevices(swarms);
    
    // Map these values the other way around, so that a device gets
    // a list of swarms it belongs to
    const devicesSwarmsMap = {};
    for (const swarm of swarms) {
      for (const device of swarmsDevices[swarm]) {
        if (!devicesSwarmsMap[device]) {
          devicesSwarmsMap[device] = [];
        }

        devicesSwarmsMap[device].push(swarm);
      }
    }

    const devices = Object.keys(devicesSwarmsMap);
    const deviceData = await devicesManager.getDeviceData(devices);

    for (const device in deviceData) {
      deviceData[device].info.swarms = devicesSwarmsMap[device];
    }

    return deviceData;
  }

  this.getDeviceData = async function(userId, device, from, to) {
    if (!await this.canAccessDevice(userId, device)) {
      return null;
    }

    const [swarms, devicesData] = await Promise.all([
      devicesManager.getSwarmAssociations(device),
      devicesManager.getDeviceData([device], from, to)
    ]);

    const data = devicesData[device];
    data.info.swarms = swarms;

    return data;
  }

  this.getDevicePreferences = async function(userId, device) {
    if (!await this.canAccessDevice(userId, device)) {
      return null;
    }

    return await devicesManager.getDevicePreferences(device);
  }

  this.setDevicePreferences = async function(userId, device, preferences) {
    if (!await this.canManageDevice(userId, device)) {
      return false;
    }

    return await devicesManager.setDevicePreferences(device, preferences);
  }

  this.updateDevice = async function(userId, device, dataValues) {
    if (!await this.canManageDevice(userId, device)) {
      return false;
    }
  
    if (dataValues.pcp != null) {
      await devicesManager.updatePcpAdminState(device, dataValues.pcp)
    }

    if (dataValues.admin != null) {
      await devicesManager.updateAdminState(device, dataValues.admin)
    }

    return true;
  }

  /**
   * Return a list of alarms 
   * @param {*} userId 
   */
  this.getAlarms = async function(userId) {
    log.debug(`Retrieving alarms for user ${userId}`);

    let alarms = await alarmsManager.getAlarms();

    // No need to filter alarms for admins
    if (await userdataManager.isAdmin(userId)) {
      log.debug(`User ${userId} is admin, so returning all alarms`);
      return alarms;
    }

    // Find all devices and swarms the user has access to
    const accessableSwarms = await getAccessableSwarms(userId);
    const swarmDevices = await swarmsManager.getSwarmDevices(accessableSwarms);
    let accessableDevices = [];
    Object.values(swarmDevices).map(function(devices) {
      accessableDevices = accessableDevices.concat(devices);
    });

    // And then filter the alarms so that they only see the alarms they are allowed to see
    alarms = Object.keys(alarms)
      .filter(function(alarmId) {
        const alarm = alarms[alarmId];
        if (alarmsManager.isSwarmAlarm(alarm)) {
          return accessableSwarms.indexOf(alarm.getSource()) > -1;
        } else {
          return accessableDevices.indexOf(alarm.getSource()) > -1;
        }
      })
      .reduce(function(filteredAlarms, alarmId) {
        filteredAlarms[alarmId] = alarms[alarmId];
        return filteredAlarms;
      }, {});

    log.debug(`Returning ${Object.keys(alarms).length} for user ${userId} after alarm filter`);

    return alarms;
  }

  this.setAlarmMute = async function(userId, alarmId, muted) {
    if (!alarmId || !await this.canManageAlarm(userId, alarmId)) {
      // If we are admin, allow the alarm to be forceably muted (subswarm removed but alarm still active)
      if (!await userdataManager.isAdmin(userId)) {
        return false;
      }
    }
    
    await alarmsManager.setAlarmMute(alarmId, muted);
    return true;
  }

  this.cancelAlarm = async function(userId, alarmId) {
    if (!alarmId || !await this.canManageAlarm(userId, alarmId)) {
      // If we are admin, allow the alarm to be forceably removed (subswarm removed but alarm still active)
      if (!await userdataManager.isAdmin(userId)) {
        return false;
      }
    }

    await alarmsManager.cancelAlarm(alarmId);
    return true;
  }

  /**
   * Can the given user id access the given device.
   * 
   * If the device doesn't exist, false is returned
   */
  this.canAccessDevice = async function(userId, device) {  
    let deviceSwarms, accessableSwarms;
    log.debug(`Finding out device access for ${userId}`);
    try {
      [deviceSwarms, accessableSwarms] = await Promise.all([
        devicesManager.getSwarmAssociations(device), 
        getAccessableSwarms(userId)
      ]);
    } catch (e) {
      log.error(`Error getting device access for ${userId}`, e);
      throw new DataAccessManager("Unable get device access");
    }

    for (let swarm of deviceSwarms) {
      for (let accessableSwarm of accessableSwarms) {
        if (accessableSwarm.toLowerCase() == swarm.toLowerCase()) {
          log.debug(`${userId} can access ${device}`);
          return true;
        }
      }
    }
    log.debug(`${userId} cannot access ${device}`);
    return false;
  }

  this.canManageDevice = async function(userId, device) {
    let deviceSwarms, manageableSwarms;
    log.debug(`Finding out management to ${device} for ${userId}`);
    try {
      [deviceSwarms, manageableSwarms] = await Promise.all([
        devicesManager.getSwarmAssociations(device),  
        getManagableSwarms(userId)
      ]);
    } catch (e) {
      log.error(`Error getting device access for ${userId}`, e);
      throw new DataAccessManager("Unable get device access");
    }

    for (let swarm of deviceSwarms) {
      for (let manageableSwarm of manageableSwarms) {
        if (manageableSwarm.toLowerCase() == swarm.toLowerCase()) {
          log.debug(`${userId} can manage ${device}`);
          return true;
        }
      }
    }
    log.debug(`${userId} cannot manage ${device}`);
    return false;
  }

  /**
   * Can the given user access the given swarm?
   * 
   * If the swarm doesn't exist, false is returned
   */
  this.canAccessSwarm = async function(userId, swarm) {
    return (await getSwarmPermission(userId, swarm)) != null;
  }

  /**
   * Can the given user manage the given swarm?
   * 
   * If the swarm is not a manageable swarm, then no.
   * 
   */
  this.canManageSwarm = async function(userId, swarm) {
    // This swarm is not even managed, so how can we manage it
    if ((await swarmsManager.getManagedSwarms()).indexOf(swarm) < 0) {
      return false;
    }

    // Otherwise, see if this user has permission to manage this swarm
    return (await getManagableSwarms(userId)).indexOf(swarm) > -1;
  }

  this.canOperateSwarm = async function(userId, swarm) {
    const operator = (await swarmsManager.getOperators([swarm]))[swarm];

    if (!operator) {
      return false;
    }
    
    return operator.id == userId;
  }

  /**
   * Can the given user access the given swarm?
   * 
   * If the swarm doesn't exist, false is returned
   */
  this.canAccessAlarm = async function(userId, alarmId) {
    const alarm = await alarmsManager.getAlarm(alarmId);
    if (!alarm) {
      return false;
    }

    if (alarmsManager.isSwarmAlarm(alarm)) {
      return await this.canAccessSwarm(userId, alarm.getSource());
    } else {
      return await this.canAccessDevice(userId, alarm.getSource());
    }
  }

  /**
   * Can the user manage this alarm?
   * 
   * If the alarm doesn't exist, false is returned.
   */
  this.canManageAlarm = async function(userId, alarmId) {
    let alarm;

    try {
      alarm = await alarmsManager.getAlarm(alarmId);
    } catch (e) {
      log.error(`Unable to get ${alarmId} from alarms manager`, e);
      throw new DataAccessManagerError("Unable to get alarm data");
    }

    if (!alarm) {
      return false;
    }
    
    try {
      if (alarmsManager.isSwarmAlarm(alarm)) {
        return await this.canManageSwarm(userId, alarm.getSource());
      } else {
        return await this.canManageDevice(userId, alarm.getSource());
      }
    } catch (e) {
      log.error(`Could not find managability of ${alarmId}`, e);
      throw new DataAccessManagerError("Error fetching user data")
    }
  }
}

module.exports = new DataAccessManager();
