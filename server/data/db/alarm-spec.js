const fs = require("fs");
const logging = require("../../logging.js");
const config = require("../../config.js");


function AlarmSpec() {
  const ALARM_SPEC_COLUMNS = [];
  const log = logging.createLogger("AlarmSpec");

  const DEVICE_SPEC_FILE = "devicespec.csv";
  const SWARM_SPEC_FILE = "swarmspec.csv";

  let specData = {};

  /**
   * Parse, the alarm spec data
   * @param {string} specFileData 
   */
  function extractDataFromSpec(specFileData) {
    const spec = {"conversion": {}};

    for (let line of specFileData.split("\n")) {
      if (line.startsWith("#")) {
        continue;
      }

      // Extract the information from the line
      let [, alarmType, alarmId, legacyAlarm, enSummary, deSummary, deInstructions, enInstructions] = line.split(";");

      alarmId = parseInt(alarmId);
      
      if (!spec[alarmType]) {
        spec[alarmType] = {};
      }

      spec[alarmType][alarmId] = {en: [enSummary, enInstructions], de: [deSummary, deInstructions]};
      spec["conversion"][legacyAlarm] = [alarmType, alarmId];
    }

    return spec;
  }

  function readSpec() {
    log.info("Reading and parsing spec");
    specData = {};

    const deviceSpecData = fs.readFileSync(config.alarmspecDir + "/" + DEVICE_SPEC_FILE);
    const swarmSpecData  = fs.readFileSync(config.alarmspecDir + "/" + SWARM_SPEC_FILE);

    specData["device"] = extractDataFromSpec(deviceSpecData.toString());
    specData["swarm"] = extractDataFromSpec(swarmSpecData.toString());

    // Spec only exists for Swarm but alarms exist for both
    specData["device"]["CcAlm"] = specData["swarm"]["CcAlm"];

    log.info("Spec read");
  }
  
  this.getSpec = async function() {
    return specData;
  }

  readSpec();
}

const alarmSpec = new AlarmSpec();

module.exports = alarmSpec;