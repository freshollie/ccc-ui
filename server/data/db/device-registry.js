const request = require("request-promise");
const swarmdb = require("./swarmdb.js");
const config = require("../../config.js");
const logging = require("../../logging.js");

function DeviceRegistry() {
  const GET_REGISTRY_VALUE = "/GetRegistryValue";
  const GET_ALL_REGISTRY_VALUES = "/GetAllRegistryValues";
  const GET_DEVICES = "/GetDevices";

  this.KEY_INVERTER_TYPE = "inverterType";
  this.KEY_CAPACITY_RATING = "capacityRating";
  this.KEY_POWER_TYPE = "powerType";

  const TIMEOUT = 10000;

  const cache = {};

  const self = this;

  const log = logging.createLogger("DeviceRegistry");

  class DeviceRegistryError extends Error {};

  async function sleep(milliseconds) {
    return new Promise(function(resolve) {
      setTimeout(resolve, milliseconds);
    });
  }

  function parseRegistryData(data) {
    let registryData = {};
    for (let value of data["data"]) {
      registryData[value.name] = value.value;
    }

    return registryData;
  }

  async function cacheRegistryData(device) {
    let data = await performRegistryRequest(GET_ALL_REGISTRY_VALUES, { qs: { dp: device }, timeout: TIMEOUT });
    cache[device] = parseRegistryData(data);
  }

  async function doCacheCollection() {
    log.debug("Performing cache collection");

    let cacheCollectors = [];
    let devices;
    try {
      devices = await self.getDevices();
    } catch (e) {
      log.error("Could not get devices for cache collection", e);
      return;
    }
    
    for (let device of devices) {
      cacheCollectors.push(cacheRegistryData(device));
    }

    try {
      await Promise.all(cacheCollectors);
    } catch (e) {
      log.error("Not all cache collectors completed successfully", e);
    }
    
    log.debug(`Retrieved registry cache for ${devices.length} devices`);
  }

  this.startCacheCollectionLoop = async function() {
    log.info("Starting cache collection loop");
    while (true) {
      await doCacheCollection();
      await sleep(60000);
    }
  }

  async function performRegistryRequest(endpoint, options) {
    let response;
    let uri = config.registry + endpoint;
    try {
      response = await request.get(uri, options);
    } catch (e) {
      throw new DeviceRegistryError("Connection error: " + e.message);
    }

    if (response == "{}") {
      throw new DeviceRegistryError("Bad response: " + response);
    }

    let data;
    try {
      data = JSON.parse(response);
    } catch (e) {
      throw new DeviceRegistryError("Could not parse response: " + JSON.stringify(data));
    }

    return data;
  }

  this.getDevices = async function() {
    let data = await performRegistryRequest(GET_DEVICES);
    
    if (!data.devs) {
      log.error("Bad response from registry for device list: " + JSON.stringify(data));
      throw new DeviceRegistryError("Bad response");
    }    
    
    let devices = [];

    for (let device of data.devs) {
      devices.push(device.name);
    }
    return devices;
  }

  this.getInverterTypes = function() {
    
  }

  /**
   * Return the given registry values from the values cache for the given devices
   */
  this.getRegistryValues = function(devices, values) {
    let returnValues = {};
    for (let device of devices) {
      returnValues[device] = {};

      let deviceCacheValues = cache[device];
      // If we don't have a cache for this device, just put empty values
      if (!deviceCacheValues) {
        deviceCacheValues = {};
      }

      for (let value of values) {
        returnValues[device][value] = deviceCacheValues[value];
      }
    }

    return returnValues;
  }
}

const deviceRegistry = new DeviceRegistry();
deviceRegistry.startCacheCollectionLoop();

module.exports = deviceRegistry;
