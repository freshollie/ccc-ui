const request = require("request-promise");

const logging = require("../../logging.js");
const config = require("../../config.js");


function FahrplanApi() {
  const log = logging.createLogger("FahrplanApi");
  const FAHRPLANS_ENDPOINT = "/fahrplans/";

  const TIMEOUT = 3000;

  class FahrplanApiError extends Error {};
  class IllegalOperationError extends FahrplanApiError {
    constructor(error) {
      super();
      this.code = error.code;
      this.message = error.message;
    }
  };

  this.FahrplanApiError = FahrplanApiError;
  this.IllegalOperationError = IllegalOperationError;

  this.getEvents = async function(swarm, from = null, to = null, type = null) {
    const query = {};

    if (from != null) {
      query.from = from;
    }
    
    if (to != null) {
      query.to = to;
    }

    if (type != null) {
      query.type = type;
    }
    
    const url = `${config.fahrplanApi}${FAHRPLANS_ENDPOINT}${swarm}`;

    let response;
    try {
      response = await request.get(url, { qs: query, json: true, timeout: TIMEOUT });
    } catch (e) {
      if (e.response.body && e.response.body.error) {
        if (e.response.statusCode == 404) {
          // No fahrplan data for this swarm
          return [];
        } else if (e.response.statusCode == 422) {
          throw new IllegalOperationError(e.response.body.error);
        }
        
        throw new FahrplanApiError(e.response.body.error);
      }

      log.error(`Unexpected fahrplan API error: ${e.message}`);
      throw new FahrplanApiError("Unexpected fahrplan-api connection error");
    }

    return response[swarm];
  }

  this.requestEvent = async function(swarm, eventRequest) {
    const url = `${config.fahrplanApi}${FAHRPLANS_ENDPOINT}${swarm}`;

    let response;
    try {
      response = await request.post(url, { json: eventRequest, timeout: TIMEOUT });
    } catch (e) {
      if (e.response.body && e.response.body.error) {
        if (e.response.statusCode == 404) {
          // No such swarm
          return null;
        } else if (e.response.statusCode == 422) {
          throw new IllegalOperationError(e.response.body.error);
        }
        
        throw new FahrplanApiError(e.response.body.error);
      }

      log.error(`Unexpected fahrplan API error: ${e.message}`);
      throw new FahrplanApiError("Unexpected fahrplan-api connection error");
    }

    return response["success"];
  }

  this.updateEvent = async function(swarm, id, newType) {
    const url = `${config.fahrplanApi}${FAHRPLANS_ENDPOINT}${swarm}/${id}`;

    let response;
    try {
      response = await request.patch(url, { json: { type: newType } });
    } catch (e) {
      if (e.response.body && e.response.body.error) {
        if (e.response.statusCode == 404) {
          // No such event
          return null;
        } else if (e.response.statusCode == 422) {
          throw new IllegalOperationError(e.response.body.error);
        }
        
        throw new FahrplanApiError(e.response.body.error);
      }

      log.error(`Unexpected fahrplan API error: ${e.message}`);
      throw new FahrplanApiError("Unexpected fahrplan-api connection error");
    }

    return response["success"];
  }

  this.getEvent = async function(swarm, id) {
    const url = `${config.fahrplanApi}${FAHRPLANS_ENDPOINT}${swarm}/${id}`;

    let response;
    try {
      response = await request.get(url, { json: true, timeout: TIMEOUT });
    } catch (e) {
      if (e.response.body && e.response.body.error) {
        if (e.response.statusCode == 404) {
          // No such event
          return null;
        } else if (e.response.statusCode == 422) {
          throw new IllegalOperationError(e.response.body.error);
        }
        
        throw new FahrplanApiError(e.response.body.error);
      }

      log.error(`Unexpected fahrplan API error: ${e.message}`);
      throw new FahrplanApiError("Unexpected fahrplan-api connection error");
    }

    return response[id];
  }

  this.cancelEvent = async function(swarm, id) {
    const url = `${config.fahrplanApi}${FAHRPLANS_ENDPOINT}${swarm}/${id}`;

    let response;
    try {
      response = await request.delete(url, { json: true, timeout: TIMEOUT });
    } catch (e) {
      if (e.response.body && e.response.body.error) {
        if (e.response.statusCode == 404) {
          // No such event
          return null;
        } else if (e.response.statusCode == 422) {
          throw new IllegalOperationError(e.response.body.error);
        }
        
        throw new FahrplanApiError(e.response.body.error);
      }

      log.error(`Unexpected fahrplan API error: ${e.message}`);
      throw new FahrplanApiError("Unexpected fahrplan-api connection error");
    }

    return response["success"];
  }
}


module.exports = new FahrplanApi();
