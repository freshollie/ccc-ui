const knex = require("knex");

const config = require("../../config.js");
const logging = require("../../logging.js");
const Alarm = require("./models/Alarm.js");

/** 
 * Representation of the gui database.
 * 
 * Provides a simple API for querying and inserting data
 * into the database
 */
function GuiDB() {
  const TABLE_USERS = "users";
  const USERS_COLUMN_ID = "users.id";
  const USERS_COLUMN_USERNAME = "users.username";
  const COLUMN_ID = "id";
  const COLUMN_HASHEDPASS = "hashedpass";
  const COLUMN_USERNAME = "username";

  const TABLE_USER_ROLES = "user_roles";
  const USER_ROLES_COLUMN_ROLE_ID = "user_roles.role_id";
  const USER_ROLES_COLUMN_USER_ID = "user_roles.user_id";
  const USER_ROLES_COLUMN_READONLY = "user_roles.readonly";
  const COLUMN_ROLE_ID = "role_id";
  const COLUMN_USER_ID = "user_id";
  const COLUMN_READONLY = "readonly";

  const TABLE_ROLES = "roles";
  const ROLES_COLUMN_ID = "roles.id";
  const COLUMN_ROLENAME = "rolename";

  const ROLES_COLUMN_ROLENAME = "roles.rolename";

  const TABLE_USER_PREFERENCES = "user_preferences";
  const USER_PREFERENCES_COLUMN_PREFERENCE_KEY = "user_preferences.preference_key";
  const USER_PREFERENCES_COLUMN_PREFERENCE_VALUE = "user_preferences.preference_value";
  const USER_PREFERENCES_COLUMN_USER_ID = "user_preferences.user_id";
  const COLUMN_PREFERENCE_KEY = "preference_key";
  const COLUMN_PREFERENCE_VALUE = "preference_value";

  const TABLE_ACTIVE_ALARMS = "active_alarms";
  const COLUMN_ALARM_ID = "alarm_id";
  const COLUMN_ALARM_TIME = "alarm_time";
  const COLUMN_ALARM_SOURCE = "alarm_source";
  const COLUMN_ALARM_SOURCE_TYPE = "alarm_source_type";
  const COLUMN_ALARM_TYPE = "alarm_type";
  const COLUMN_ALARM_VAL = "alarm_val";
  const COLUMN_MUTED = "muted";

  const TABLE_OPERATORS = "operators";
  const COLUMN_SWARMNAME = "swarmname";
  const OPERATORS_COLUMN_USER_ID = "operators.user_id";
  const OPERATORS_COLUMN_SWARMNAME = "operators.swarmname";

  const TABLE_DEVICE_PREFERENCES = "device_preferences";
  const COLUMN_PREFERENCE_JSON = "preferences_json";
  const COLUMN_SN = "sn";


  const log = logging.createLogger("GuiDB");
  log.info("Initialising");

  const dbConfig = config.dbType == "postgres" ? config.postgres : config.mariadb;

  log.debug("Config: " + JSON.stringify(dbConfig));

  const db = knex(dbConfig);

  this.getConnector = function() {
    return db;
  }

  this.removeUser = async function(userId) {
    await db.delete().from(TABLE_USERS).where(USERS_COLUMN_ID, userId);
  }

  this.getUserDetails = async function(whereUserId=null) {
    let query = db.select([
                        USERS_COLUMN_ID,
                        USERS_COLUMN_USERNAME,
                        ROLES_COLUMN_ROLENAME, 
                        USER_ROLES_COLUMN_READONLY, 
                        USER_PREFERENCES_COLUMN_PREFERENCE_KEY, 
                        USER_PREFERENCES_COLUMN_PREFERENCE_VALUE
                    ])
                    .from(TABLE_USERS)
                    .leftJoin(TABLE_USER_ROLES, USER_ROLES_COLUMN_USER_ID, USERS_COLUMN_ID)
                    .leftJoin(TABLE_ROLES, ROLES_COLUMN_ID, USER_ROLES_COLUMN_ROLE_ID)
                    .leftJoin(TABLE_USER_PREFERENCES, USER_PREFERENCES_COLUMN_USER_ID, USER_ROLES_COLUMN_USER_ID);
    
    if (whereUserId != null) {
      query.where(USERS_COLUMN_ID, whereUserId);
    }

    const rows = await query;

    const details = {};
    for (const row of rows) {
      const userId = row[COLUMN_ID];
      
      // A new user id
      if (!details[userId]) {
        details[userId] = { id: userId, username: row[COLUMN_USERNAME], roles: {}, preferences: {} };
      }

      let userDetails = details[userId];

      const role = row[COLUMN_ROLENAME];
      if (role) {
        userDetails.roles[role] = { readonly: row[COLUMN_READONLY] };
      }
      
      const prefKey = row[COLUMN_PREFERENCE_KEY];

      if (prefKey) {
        userDetails.preferences[prefKey] = row[COLUMN_PREFERENCE_VALUE];
      }
    }

    if (whereUserId != null) {
      return details[whereUserId];
    }

    return Object.values(details);
  }

  this.existsUsername = async function(username) {
    const row = await db.select([COLUMN_ID]).from(TABLE_USERS).where(COLUMN_USERNAME, username.toLowerCase()).first();
    if (row) {
      return true;
    }

    return false;
  } 

  this.existsUser = async function(userId) {
    const row = await db.select([COLUMN_ID]).from(TABLE_USERS).where(COLUMN_ID, userId).first();
    if (row) {
      return true;
    }

    return false;
  }

  this.getUserRoles = async function(userId) {
    let rows = await db.select([ROLES_COLUMN_ROLENAME, USER_ROLES_COLUMN_READONLY])
                       .from(TABLE_USERS)
                       .join(TABLE_USER_ROLES, USERS_COLUMN_ID, USER_ROLES_COLUMN_USER_ID)
                       .join(TABLE_ROLES, USER_ROLES_COLUMN_ROLE_ID, ROLES_COLUMN_ID)
                       .where(USERS_COLUMN_ID, userId)
    
    let roles = {};

    if (rows) {
      for (let row of rows) {
        roles[row[COLUMN_ROLENAME]] = { readonly: row[COLUMN_READONLY] };
      }
    }

    return roles;
  }

  this.getRoleIdForRole = async function(rolename) {
    let row = await db.select(COLUMN_ID).from(TABLE_ROLES).where(COLUMN_ROLENAME, rolename.toLowerCase()).first();

    if (row) {
      return row[COLUMN_ID];
    }
  }

  this.getRoleForUser = async function(userId, rolename) {
    let row = await db.select([ROLES_COLUMN_ROLENAME, USER_ROLES_COLUMN_READONLY])
                      .from(TABLE_USERS)
                      .join(TABLE_USER_ROLES, USERS_COLUMN_ID, USER_ROLES_COLUMN_USER_ID)
                      .join(TABLE_ROLES, USER_ROLES_COLUMN_ROLE_ID, ROLES_COLUMN_ID)
                      .where(ROLES_COLUMN_ROLENAME, rolename.toLowerCase())
                      .where(USERS_COLUMN_ID, userId).first()
    if (row) {
      return {name: row[COLUMN_ROLENAME], readonly: row[COLUMN_READONLY]}
    }
  }

  this.createRole = async function(rolename) {
    return (await db.insert({[COLUMN_ROLENAME]: rolename.toLowerCase()}).into(TABLE_ROLES).returning(COLUMN_ID))[0];
  }

  this.getAllRoles = async function() {
    let rows = await db.select([COLUMN_ROLENAME]).from(TABLE_ROLES);

    let roles = [];
    for (let row of rows) {
      roles.push(row[COLUMN_ROLENAME]);
    }

    return roles;
  }

  this.addRoleToUser = async function(username, roleId, readonly) {
    await db.insert({ [COLUMN_USER_ID]: username, [COLUMN_ROLE_ID]: roleId, [COLUMN_READONLY]: readonly }).into(TABLE_USER_ROLES);
  }
  
  this.removeRoleFromUserId = async function(userId, roleId) {
    await db.delete().from(TABLE_USER_ROLES).where(COLUMN_ROLE_ID, roleId).where(COLUMN_USER_ID, userId);
  }

  this.removeAllRolesFromUserId = async function(userId) {
    await db.delete().from(TABLE_USER_ROLES).where(COLUMN_USER_ID, userId);
  }

  this.getUserCredentials = async function(username) {
    let row = await db.select().from(TABLE_USERS).where(COLUMN_USERNAME, username.toLowerCase()).first();

    if (row) {
      return { id: row[COLUMN_ID], hashedPass: row[COLUMN_HASHEDPASS] };
    }
  }

  this.insertUser = async function(username, hashedPass) {
    return (await db.insert({[COLUMN_USERNAME]: username.toLowerCase(), [COLUMN_HASHEDPASS]: hashedPass}).into(TABLE_USERS).returning(COLUMN_ID))[0];
  }

  this.updateUserPassword = async function(userId, hashedPass) {
    await db.update({[COLUMN_HASHEDPASS]: hashedPass}).table(TABLE_USERS).where(COLUMN_ID, userId);
  }

  this.updateUserPreferences = async function(userId, preferences) {
    await db.transaction(async function(trx) {
      for (const preferenceKey in preferences) {
        const preferenceValue = preferences[preferenceKey];
        await trx.delete().from(TABLE_USER_PREFERENCES).where(COLUMN_PREFERENCE_KEY, preferenceKey).where(COLUMN_USER_ID, userId);

        await trx.insert({
          [COLUMN_PREFERENCE_KEY]: preferenceKey, 
          [COLUMN_PREFERENCE_VALUE]: preferenceValue, 
          [COLUMN_USER_ID]: userId
        }).into(TABLE_USER_PREFERENCES);
      }
    });
  }

  this.getActiveAlarms = async function() {
    let rows = await db.select().from(TABLE_ACTIVE_ALARMS);

    let alarms = [];
    for (let row of rows) {
      alarms.push(new Alarm(row[COLUMN_ALARM_ID], 
                            row[COLUMN_ALARM_TIME], 
                            row[COLUMN_ALARM_SOURCE], 
                            row[COLUMN_ALARM_SOURCE_TYPE],
                            row[COLUMN_ALARM_TYPE], 
                            row[COLUMN_ALARM_VAL],
                            row[COLUMN_MUTED]));
    }

    return alarms;
  }

  this.setAlarmMute = async function(alarmId, muted) {
    await db.update({ [COLUMN_MUTED]: muted }).table(TABLE_ACTIVE_ALARMS).where(COLUMN_ALARM_ID, alarmId);
  }
  
  this.insertActiveAlarms = async function(alarms) {
    const currentAlarms = await this.getActiveAlarms();
    for (const alarm of alarms) {
      if (currentAlarms.indexOf(alarm) > -1) {
        continue;
      }
      try {
        await db.insert({
              [COLUMN_ALARM_ID]: alarm.getId(), 
              [COLUMN_ALARM_SOURCE]: alarm.getSource(), 
              [COLUMN_ALARM_TIME]: alarm.getTime(), 
              [COLUMN_ALARM_TYPE]: alarm.getType(),
              [COLUMN_ALARM_SOURCE_TYPE]: alarm.getSourceType(), 
              [COLUMN_ALARM_VAL]: alarm.getVal(), 
              [COLUMN_MUTED]: alarm.isMuted()
            }).into(TABLE_ACTIVE_ALARMS);
      } catch (e) {
        if (config.dbType != "postgres") {
          if (e.code != "ER_DUP_ENTRY") {
            throw e;
          }
        } else if (e.message.indexOf("violates") < -0) {
          throw e;
        }
      }
    }
  }

  this.getActiveAlarm = async function(alarmId) {
    let row = await db.select().from(TABLE_ACTIVE_ALARMS).where(COLUMN_ALARM_ID, alarmId).first();
    if (row) {
      return new Alarm(
        row[COLUMN_ALARM_ID], 
        row[COLUMN_ALARM_TIME], 
        row[COLUMN_ALARM_SOURCE], 
        row[COLUMN_ALARM_SOURCE_TYPE],
        row[COLUMN_ALARM_TYPE], 
        row[COLUMN_ALARM_VAL],
        row[COLUMN_MUTED]
      );
    }
  }

  this.removeActiveAlarms = async function(alarmIds) {
    await db.delete().from(TABLE_ACTIVE_ALARMS).whereIn(COLUMN_ALARM_ID, alarmIds);
  }

  this.getSwarmOperators = async function(swarmnames) {
    const rows = await db.select([OPERATORS_COLUMN_SWARMNAME, USERS_COLUMN_USERNAME, USERS_COLUMN_ID])
                         .from(TABLE_OPERATORS)
                         .innerJoin(TABLE_USERS, OPERATORS_COLUMN_USER_ID, USERS_COLUMN_ID)
                         .whereIn(OPERATORS_COLUMN_SWARMNAME, swarmnames);

    const swarms = {}; 
    for (const row of rows) {
      swarms[row[COLUMN_SWARMNAME]] = { id: row[COLUMN_ID], username: row[COLUMN_USERNAME] };
    }

    return swarms;
  }

  this.setSwarmOperators = async function(userId, swarmnames) {
    const currentOperators = await this.getSwarmOperators(swarmnames);
    await db.transaction(async function(trx) {
      for (const swarm of swarmnames) {
        if (!currentOperators[swarm]) {
          await trx.insert({ [COLUMN_USER_ID]: userId, [COLUMN_SWARMNAME]: swarm }).into(TABLE_OPERATORS);
        } else {
          await trx.update({ [COLUMN_USER_ID]: userId }).table(TABLE_OPERATORS).where(COLUMN_SWARMNAME, swarm);
        }
      }
    });
  }

  this.getDevicesPreferences = async function(devices) {
    const rows = await db.select([COLUMN_SN, COLUMN_PREFERENCE_JSON]).from(TABLE_DEVICE_PREFERENCES).whereIn(COLUMN_SN, devices);

    const preferences = {};

    for (const row of rows) {

      let devicePrefs = {};
      
      // Handle mariadb not using JSON as a column type
      if (config.dbType != "postgres") {
        if (row[COLUMN_PREFERENCE_JSON]) {
          devicePrefs = JSON.parse(row[COLUMN_PREFERENCE_JSON]);
        }
      } else {
        devicePrefs = row[COLUMN_PREFERENCE_JSON];
      }

      preferences[row[COLUMN_SN]] = devicePrefs;
    }

    return preferences;
  }

  this.setDevicePreferences = async function(device, preferences) {
    if (config.dbType != "postgres") {
      preferences = JSON.stringify(preferences);
    }

    try {
      await db
        .insert({ [COLUMN_SN]: device.toLowerCase(), [COLUMN_PREFERENCE_JSON]: preferences })
        .into(TABLE_DEVICE_PREFERENCES);
      return
    } catch (e) {
      if (config.dbType != "postgres") {
        if (e.code != "ER_DUP_ENTRY") {
          throw e;
        }
      } else if (e.message.indexOf("violates") < -0) {
        throw e;
      }

      await db
        .update({[COLUMN_PREFERENCE_JSON]: preferences})
        .table(TABLE_DEVICE_PREFERENCES)
        .where(COLUMN_SN, device.toLowerCase());
    }
  }
}

module.exports = new GuiDB();
