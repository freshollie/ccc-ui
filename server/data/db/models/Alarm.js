class Alarm {
  constructor(id, time, source, sourceType, type, val, muted) {
    this.id = id;
    this.time = time;
    this.source = source;
    this.sourceType = sourceType;
    this.type = type;
    this.val = val;
    this.muted = muted;
  }

  getId() {
    return this.id;
  }

  getTime() {
    return this.time;
  }

  getSource() {
    return this.source;
  }

  getSourceType() {
    return this.sourceType;
  }

  getType() {
    return this.type;
  }

  getVal() {
    return this.val;
  }

  isMuted() {
    return this.muted;
  }

  toJSON() {
    return { 
      time: this.time.getTime(), 
      source: this.source, 
      sourceType: this.sourceType, 
      type: this.type, 
      val: this.val, 
      muted: this.muted 
    }
  }

  toJsonString() {
    return JSON.stringify(this.toJSON());
  }

  getDetailsString() {
    return `Alarm<${this.id},${this.source}.${this.type}:${this.val},muted:${this.muted}>`
  }
}

module.exports = Alarm;
