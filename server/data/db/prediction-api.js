const fs = require("fs");
const request = require("request-promise");

const logging = require("../../logging.js");
const config = require("../../config.js");


function PredictionApi() {
  const log = logging.createLogger("PredictionApi");
  const PREDICTION_ENDPOINT = "/swarms/";

  const TIMEOUT = 3000;
  
  let cache = {};

  class PredictionApiError extends Error {};

  this.getPrediction = async function(swarm, from, to) {
    log.debug(`Getting prediction for ${swarm}`);

    const url = `${config.predictionApi}${PREDICTION_ENDPOINT}${swarm}`;
    const params = {
      from: from,
      to: to
    };

    log.debug(`Requesting ${url} ${JSON.stringify(params)}`);
    
    try {
      const data = await request.get(url, { qs: params, json: true, timeout: TIMEOUT });
      log.debug(`Got data`);
      return data[swarm];
    } catch (e) {
      throw new PredictionApiError("Unable to get prediction data: " + e.message);
    }
  }

  this.getPredictions = async function(swarms, from, to) {
    log.debug(`Getting data for ${swarms}`)
    const predictionRequests = [];
    for (const swarm of swarms) {
      predictionRequests.push(await this.getPrediction(swarm, from, to));
    }

    const predictions = await Promise.all(predictionRequests);

    const data = {};
    for (let i = 0; i < swarms.length; i++) {
      data[swarms[i]] = predictions[i];
    }

    log.debug(`Data retrieved`);
    return data;
  }
}

module.exports = new PredictionApi();
