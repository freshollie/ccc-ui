const request = require("request-promise");
const config = require("../../config.js");
const logging = require("../../logging.js");

function SwarmStrucConnector() {
  const GET_SWARMS = "/getSwarms?flat=true";
  const GET_MANANGED_SWARMS = "/getManagedSwarms";
  const GET_DEVICES = "/getDevices";
  const GET_ALL_SWARMS_FOR_DEVICE = "/getAllSwarms?";

  const CREATE_SWARM = "/createSwarm";
  const ADD_SWARM = "/addSwarm";
  const ADD_SN = "/addSN";
  const SET_SWARM = "/setSwarm";
  const DELETE_SWARM = "/deleteSwarm";
  const REMOVE_SWARM = "/removeSwarm";

  const GET_VIRTUAL_SN = "/getVirtualSN";

  const TIMEOUT = 3000;

  class SwarmStrucConnectorError extends Error {};

  class SwarmStrucError extends Error {};

  class InvalidNameError extends SwarmStrucError {};
  class InvalidParentError extends SwarmStrucError {};
  class InvalidSubswarmError extends SwarmStrucError {};
  
  class SwarmActiveError extends SwarmStrucError {};

  class SwarmNonExistentError extends SwarmStrucError {};
  class DeviceNonExistentError extends SwarmStrucError {};

  this.SwarmStrucError = SwarmStrucError;

  this.InvalidNameError = InvalidNameError;
  this.InvalidParentError = InvalidParentError;
  this.InvalidSubswarmError = InvalidSubswarmError;

  this.SwarmActiveError = SwarmActiveError;
  
  this.SwarmNonExistentError = SwarmNonExistentError;
  this.DeviceNonExistentError = DeviceNonExistentError;

  const log = logging.createLogger("SwarmStrucConnector");

  async function performRequest(request) {
    try {
      return await request;
    } catch (e) {
      if (e.response) {
        let errorResponse = e.response.body;
        throw new SwarmStrucError(errorResponse.error);
      }
      log.error("Error performing request", e);
      throw e
    }
  }

  this.getSwarms = async function() {
    log.silly("Retrieving swarms");

    let swarms;
    try {
      swarms = (await performRequest(request.get(config.swarmStruc + GET_SWARMS, { json: true, timeout: TIMEOUT })))["swarms"];
    } catch (e) {
      log.error("Could not retrieve swarms");
      throw new SwarmStrucConnectorError("Error retrieving swarms");
    }

    log.silly(`Got ${swarms.length} swarms`);
    return swarms;
  }

  this.getManagedSwarms = async function() {
    log.silly("retrieving managed top swarms");

    let swarms;
    try {
      swarms = (await performRequest(request.get(config.swarmStruc + GET_MANANGED_SWARMS, { json: true, timeout: TIMEOUT })))["swarms"];
    } catch (e) {
      log.error("Could not retrieve swarms");
      throw new SwarmStrucConnectorError("Error retrieving swarms");
    }

    log.silly(`Got ${swarms.length} managed top swarms`);
    return swarms;
  }

  this.getDevices = async function(swarm) {
    log.silly(`retrieving devices in ${swarm}`);

    let devices;
    try {
      devices = (await performRequest(request.get(config.swarmStruc + GET_DEVICES, { qs: { swarm: swarm}, json: true, timeout: TIMEOUT })))["devices"];
    } catch (e) {
      if (e instanceof SwarmStrucError) {
        if (e.message.indexOf("does not exist")) {
          throw new SwarmNonExistentError(e.message);
        }
        throw e;
      }
      log.error(`Unable to retrieve devices in ${swarm}`);
      throw new SwarmStrucConnectorError("Error retrieving device list for " + swarm);
    }

    log.silly(`Got ${devices.length} devices in ${swarm}`);

    return devices;
  }

  this.getSwarmsForDevice = async function(device) {
    let swarms;
    try {
      swarms = (await performRequest(request.get(config.swarmStruc + GET_ALL_SWARMS_FOR_DEVICE, { qs: { sn: device }, json: true, timeout: TIMEOUT })))["swarms"];
    } catch (e) {
      if (e instanceof SwarmStrucError) {
        if (e.message.indexOf("does not exist")) {
          throw new DeviceNonExistentError(e.message);
        }
        throw e;
      }
      log.error(`Unable to retrieve swarms for ${device}`);
      throw new SwarmStrucConnectorError("Error retrieving swarms for " + device);
    }

    return swarms;
  }

  /**
   * Create a managed swarm in swarm-struc. Throws an error if: the name is invalid,
   * the swarm already exists, or a swarm-struc connection error
   * 
   * @param {string} swarmname 
   */
  this.createSwarm = async function(swarmname) {
    try {
      const response = await performRequest(request.get(config.swarmStruc + CREATE_SWARM, { qs: { swarm: swarmname }, json: true, timeout: TIMEOUT }));
      // Subswarms will create a VSN
      if (response.vsn != null) {
        return response.vsn;
      } else {
        return null;
      }
    } catch (e) {
      if (e instanceof SwarmStrucError) {
        if (e.message.indexOf("Invalid") > -1) {
          throw new InvalidNameError("Bad swarm name");
        } else if (e.message.indexOf("doesn't exist") > -1) {
          throw new InvalidParentError(`${swarmname}'s parent doesn't exist`);
        } else if (e.message.indexOf("not a managed swarm") > -1) {
          throw new InvalidParentError(`${swarmname}'s parent is not managed`);
        }
        throw e;
      }

      log.error(`Unable to connect to swarm-struc: ${e.message}`);
      throw new SwarmStrucConnectorError("Could not create swarm");
    }
  }

  /**
   * Add a non-managed "view" swarm. E.G. a group to contain a list of devices.
   * @param {} swarmname 
   */
  this.addSwarm = async function(swarmname) {
    try {
      await performRequest(request.get(config.swarmStruc + ADD_SWARM, { qs: { swarm: swarmname }, json: true, timeout: TIMEOUT }));
    } catch (e) {
      if (e instanceof SwarmStrucError) {
        if (e.message.indexOf("Invalid") > -1) {
          throw new InvalidNameError("Bad swarm name");
        } else if (e.message.indexOf("doesn't exist") > -1) {
          throw new InvalidParentError(`${swarmname}'s parent doesn't exist`);
        }  else if (e.message.indexOf("is a managed swarm") > -1) {
          throw new InvalidParentError(`${swarmname}'s parent is managed`);
        }
        throw e;
      }

      throw new SwarmStrucConnectorError("Connection error");
    }
  }

  /**
   * Add a list of devices to the given non-managed swarm
   * 
   * @param {*} swarmname 
   * @param {*} devices 
   */
  this.addSN = async function(swarmname, devices) {
    log.silly(`Adding ${devices.length} device(s) to ${swarmname}`);
    return await performRequest(request.post(config.swarmStruc + ADD_SN, { body: { swarm: swarmname, sn: devices }, json: true, timeout: TIMEOUT }));
  }

  /**
   * Set the managed swarm for the given device
   * 
   * @param {string} swarmname 
   * @param {string} device 
   */
  this.setSwarm = async function(swarmname, device) {
    log.silly(`Setting managed swarm (${swarmname}) for ${device}`);
    return await performRequest(request.get(config.swarmStruc + SET_SWARM, { qs: { swarm: swarmname, sn: device }, json: true, timeout: TIMEOUT }));
  }

  this.deleteSwarm = async function(swarmname) {
    log.silly(`Deleting virtual swarm: ${swarmname}`);
    return await performRequest(request.get(config.swarmStruc + DELETE_SWARM, { qs: { swarm: swarmname }, json: true, timeout: TIMEOUT }));
  }

  this.removeSwarm = async function(swarmname) {
    log.silly(`Removing subswarm: ${swarmname}`);
    try {
      return await performRequest(request.get(config.swarmStruc + REMOVE_SWARM, { qs: { swarm: swarmname }, json: true, timeout: TIMEOUT }));
    } catch (e) {
      if (e instanceof SwarmStrucError) {
        if (e.message.indexOf("top level swarms") > -1) {
          throw new InvalidSubswarmError("A managed top level swarm cannot be removed");
        } else if (e.meesage.indexOf("empty") > -1) {
          throw new SwarmActiveError("A subswarm must be empty in order to be removed");
        }
      }

      throw e;
    }
  }

  this.getVirtualSN = async function(swarmname) {
    log.silly(`Getting virtual SN of: ${swarmname}`);
    try {
      return (await performRequest(request.get(config.swarmStruc + GET_VIRTUAL_SN, { qs: { swarm: swarmname }, json: true, timeout: TIMEOUT }))).vsn;
    } catch (e) {
      if (e instanceof SwarmStrucError) {
        if (e.message.indexOf("No VSN") > -1) {
          throw new InvalidSubswarmError(e.message);
        }
        throw e;
      }

      throw new SwarmStrucConnectorError("Connection error");
    }
  }
}

module.exports = new SwarmStrucConnector();
