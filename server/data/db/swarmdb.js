const request = require("request-promise");
const config = require("../../config.js");
const logging = require("../../logging.js");

function SwarmdbConnector() {
  this.DEVICE_DATAKEY_PCPSTATE = "LLN0.CcPCP.setVal";
  this.DEVICE_DATAKEY_ADMSTATE = "LLN0.Mod.ctlVal";
  this.DEVICE_DATAKEY_OPERSTATE = "LLN0.Mod.stVal";
  this.DEVICE_DATAKEY_ESSSTATE = "LLN0.EssState.stVal";

  this.DATAKEY_FCR = "FCR";
  this.DATAKEY_PFCR = "ITCI1.PFCR.mag";
  this.DATAKEY_PV = "ITCI1.BLPPV.mag";
  this.DATAKEY_HAUSHOLDCONSUMP = "ITCI1.PHH.mag";

  this.DATAKEY_GRIDCONNECTION = "ITCI1.PGRD.mag";

  this.DATAKEY_PMAXCHRG = "ZINV1.MaxChrgW.setMag.f";
  this.DATAKEY_POPT = "MBMS1.MaxChW.setMag.f";
  this.DATAKEY_PMAXDIS = "ZINV1.MaxDisW.setMag.f";

  this.DATAKEY_SOC = "MBMS1.Soc.stVal";
  this.DATAKEY_MAXSOC = "MBMS1.MaxSoc.setVal";
  this.DATAKEY_MINSOC = "MBMS1.MinSoc.setVal";
  
  this.DATAKEY_TEMP = "MBMS1.MaxTmp.mag";

  this.DATAKEY_PRLPOS = "CPOL1.PRLpos.power";
  this.DATAKEY_PRLNEG = "CPOL1.PRLneg.power";
  this.DATAKEY_CPOLOFFSET = "CPOL1.Offset.power";

  this.SWCONTROL = "swcontrl";

  const ALARMS_PROPERTY = "alarms";

  const GET_ALARMS = "/GetNewAlarms";
  const RESET_BIT = "/ResetBit";

  const GET_LAST_VALUES = "/v2/GetLastValues";
  const GET_DEVICE_VALUES = "/v2/GetValues";
  const GET_SWARM_VALUES = "/v2/GetValues";
  const SET_DATA = "/SetData";
  const GET_DEVICES = "/GetDevices";

  const TEN_DAYS = 1440 * 10;
  
  const log = logging.createLogger("SwarmdbConnector");

  const TIMEOUT = 3000;

  class SwarmdbConnectorError extends Error {};

  this.SwarmdbConnectorError = SwarmdbConnectorError;

  function calcRange(from, to) {
    return Math.round((to - from) / 60000)
  }

  async function doV2Request(swarmdbRequest) {
    let response;
    try {
      response = await swarmdbRequest;
    } catch (e) {
      if (e.response) {
        e.message = JSON.stringify(e.response.body.error); 
        if (!e.message) {
          e.message = "Unknown serverside error";
        }
      }

      log.error("Error getting values from swarmdb: " + e.message);
      throw new SwarmdbConnectorError("Unable to fullfill request: " + e.message);
    }
    
		return response["data"];
  } 

  /**
   * Get all active alarms from swarmDB for all devices and all swarms
   */
  this.getAlarms = async function() {
    let alarmData;
    try {
      alarmData = await request.get(config.swarmdb + GET_ALARMS, { json: true, timeout: TIMEOUT });
    } catch (e) {
      log.error("Error fetching alarms from swarmdb: " + e.message);
      throw new SwarmdbConnectorError(e.message);
    }

    if (alarmData[ALARMS_PROPERTY] == null) {
      throw new SwarmdbConnectorError("Bad swarmdb response: " + JSON.stringify(alarmData));
    }

    return alarmData[ALARMS_PROPERTY];
  }

  this.getLastValues = async function(sources, dataKeys) {
    const url = `${config.swarmdb}${GET_LAST_VALUES}`;

    const params = {
      data: dataKeys,
      devices: sources
    };

    return await doV2Request(request.get(url, { qs: params, json: true }));
  }

  this.getDeviceValues = async function(devices, dataKeys, from, to) {
    const url = `${config.swarmdb}${GET_DEVICE_VALUES}`;
    
    const params = {
      data: dataKeys,
      devices: devices,
      offset: from,
      range: calcRange(from, to)
    };

    if (params.range > TEN_DAYS) {
      throw new SwarmdbConnectorError("Range too large");
    }

    log.silly(`${url} ${JSON.stringify(params)}`);
    return await doV2Request(request.get(url, { qs: params, json: true }));
  }


  this.setData = async function(source, data) {
    const url = `${config.swarmdb}${SET_DATA}`;
    const params = Object.assign({ dp: source }, data);

    try {
      await request.get(url, { qs: params, timeout: TIMEOUT });
    } catch (e) {
      if (e.response) {
        e.message = e.response;
      }

      log.error("Error pushing data to swarmdb: " + e.message);
      throw new SwarmdbConnectorError("Error pushing data value");
    }

    return { success: true };
  }

  this.getSwarmValues = async function(swarms, dataKeys, from, to) {
    // At the moment this uses exactly the same method as get device values,
    // however it is assumed that in the future swarmdb will have different
    // endpoints for device data and swarm data
    const url = `${config.swarmdb}${GET_SWARM_VALUES}`;
    
    const params = {
      data: dataKeys,
      devices: swarms,
      offset: from,
      range: calcRange(from, to)
    };

    if (params.range > TEN_DAYS) {
      throw new SwarmdbConnectorError(`Range too large (${from} to ${to})`);
    }

    return await doV2Request(request.get(url, { qs: params, json: true, timeout: TIMEOUT }));
  }

  this.getDevices = async function() {
    const url = `${config.swarmdb}${GET_DEVICES}`;

    try {
      const data = await request.get(url, { json: true });

      if (data["devs"] == null) {
        log.error("Invalid device list response from swarmdb");
        throw new SwarmdbConnectorError("Invalid swarmdb response");
      }

      return data["devs"];
    } catch (e) {
      if (e.response) {
        e.message = e.response;
      }

      log.error("Error getting values from swarmdb: " + e.message);
      throw new SwarmdbConnectorError("Error getting values");
    }
  }
} 

module.exports = new SwarmdbConnector();
