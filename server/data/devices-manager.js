const swarmdb = require("./db/swarmdb.js");
const deviceRegistry = require("./db/device-registry.js");
const swarmStruc = require("./db/swarm-struc.js");
const logging = require("../logging.js");
const guiDb = require("./db/gui-db.js");
const eventBroadcaster = require("../socket/event-broadcaster.js");

function DevicesManager() {
  const log = logging.createLogger("DevicesManager");

  class DevicesManagerError extends Error {};

  const DEVICE_DATA = [
    swarmdb.DEVICE_DATAKEY_ADMSTATE,
    swarmdb.DEVICE_DATAKEY_ESSSTATE,
    swarmdb.DEVICE_DATAKEY_PCPSTATE,
    swarmdb.DATAKEY_TEMP,
    swarmdb.DATAKEY_CPOLOFFSET,
    swarmdb.DATAKEY_PRLPOS,
    swarmdb.DATAKEY_PRLNEG,
    swarmdb.DATAKEY_SOC,
    swarmdb.DATAKEY_FCR,
    swarmdb.DATAKEY_PFCR,
    swarmdb.DATAKEY_PV,
    swarmdb.DATAKEY_HAUSHOLDCONSUMP,
    swarmdb.DATAKEY_GRIDCONNECTION
  ]

  // A list of legal device preferences
  const DEVICE_PREFERENCE_KEYS = {
    "lock": Boolean,
    "note": String
  }

  const ADMINSTATE_OFF = 0;
	const ADMINSTATE_ON = 1;
	const ADMINSTATE_ON_NOPCP = 6;

	const FCRSTATE_OFF = 0;
	const PCPSTATE_ON = 1;

	const OPERSTATE_INACTIVE = 0;
	const OPERSTATE_ACTIVE = 1;	
	const OPERSTATE_FORCECHARGE = 2;
	const OPERSTATE_ISLANDMODE = 3;
	const OPERSTATE_NOPCP = 6;

  const ESSSTATE_DOWN = 0;
	const ESSSTATE_ALIVE = 1;
	const ESSSTATE_ALIVE_COMMERROR = 2;
	const ESSSTATE_HBERROR = 3;
  const ESSSTATE_ALIVE_NOPCP = 4;
  
	const STATE_INACTIVE = 0;
	const STATE_ACTIVE = 1;
	const STATE_ACTIVE_COMMERROR = 2;
	const STATE_HBERROR = 3;
  const STATE_ACTIVE_NOPCP = 4;

  const UNKNOWN_GEN = "SAFT_v1";
  const DEFAULT_CAP = 20000;

  const TYPE_VIRTUAL_ESS = "VirtualEss";

  /**
   * Calculate the device state from the operational state and ess state
   * values. These values are used to show the colour of the device in
   * in the interface.
   */
  function calculateDeviceState(deviceData) {
    // Operstate is a representation of the the operational state
    // from the device itsself, with essState being calculated by cc-logic
    const essState = deviceData[swarmdb.DEVICE_DATAKEY_ESSSTATE].value.mv,
          operState = deviceData[swarmdb.DEVICE_DATAKEY_OPERSTATE].value.mv;
    
    // CC logic says hearbeat error, or that the ess is down. The state is inactive
    if (essState == ESSSTATE_HBERROR || essState == ESSSTATE_DOWN) {
      return STATE_INACTIVE;
    }

    // Either of these values are no pcp, then our state is no PCP
    if (essState == ESSSTATE_ALIVE_NOPCP || operState == OPERSTATE_NOPCP) {
      return STATE_ACTIVE_NOPCP;
    }

    // The device must be active
    if (operState == OPERSTATE_ACTIVE || 
        operState == OPERSTATE_FORCECHARGE || 
        operState == OPERSTATE_ISLANDMODE) {
        return STATE_ACTIVE
    }

    return STATE_INACTIVE;
  }

  /**
   * Get the device generation from its registry data
   */
  function getGen(deviceRegistryData) {
    if (!deviceRegistryData) {
      return UNKNOWN_GEN;
    }

    const gen = deviceRegistryData[deviceRegistry.INVERTER_TYPE];

    if (gen == null) {
      return UNKNOWN_GEN;
    } else {
      return gen;
    }
  }

  function extractDeviceValues(deviceData, key) {
    if (!deviceData) {
      return [];
    }

    const data = deviceData[key];
    
    if (!data) {
      log.warn(`${key} missing from swarmdb response data`);
      return [];
    }

    // This was the response from a "latest" data request
    if (data.value) {
      return [[data.value.ts, data.value.mv]];
    }

    if (!data.values) {
      log.warn(`${key} missing from swarmdb response data`);
      return [];
    }
    
    // Go through each value and remove the ts and mv keys
    const values = [];
    for (let value of data.values) {
      values.push([value.ts, value.mv]);
    }

    return values;
  }

  /** 
   * Using the given data and device, generate the info for this device
   */
  function generateDeviceData(swarmdbData) {
    const deviceData = {};
    
    // Admin state is the state we wish the device to be in
    deviceData.admin = extractDeviceValues(swarmdbData, swarmdb.DEVICE_DATAKEY_ADMSTATE);

    // Pcp state is if we want PCP enabled for this device
    deviceData.pcp = extractDeviceValues(swarmdbData, swarmdb.DEVICE_DATAKEY_PCPSTATE);

    // State is the actual state the device is in
    deviceData.state = extractDeviceValues(swarmdbData, swarmdb.DEVICE_DATAKEY_ESSSTATE);

    deviceData.soc = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_SOC);
    deviceData.temp = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_TEMP);

    deviceData.prlPos = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_PRLPOS);
    deviceData.prlNeg = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_PRLNEG);
    deviceData.offset = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_CPOLOFFSET);

    deviceData.mesFcr = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_PFCR);
    deviceData.calcFcr = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_FCR);
    deviceData.pv = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_PV);

    deviceData.hhCons = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_HAUSHOLDCONSUMP);
    deviceData.grid = extractDeviceValues(swarmdbData, swarmdb.DATAKEY_GRIDCONNECTION);
    
    return deviceData;
  }

  /**
   * Get data about the given devices.
   * 
   * The from and to values can be given to get a range of data
   * @param {Array<string>} devicesList 
   * @param {Integer} [from] 
   * @param {Integer} [to] 
   */
  this.getDeviceData = async function(devicesList, from, to) {
    log.debug(`retrieving data from swarmdb for ${devicesList.length} device(s)`);

    const requests = [];
    if (!from || !to) {
      requests.push(swarmdb.getLastValues(devicesList, DEVICE_DATA));
    } else {
      requests.push(swarmdb.getDeviceValues(devicesList, DEVICE_DATA, from, to));
    }

    requests.push(guiDb.getDevicesPreferences(devicesList));

    let swarmdbDeviceData, devicePreferences;
    try {
      [swarmdbDeviceData, devicePreferences] = await Promise.all(requests);
    } catch (e) {
      log.error("Could not get data for devices:", e);
      throw new DevicesManagerError("Error getting device data");
    }

    log.debug(`Got data for ${devicesList.length} device(s)`);

    // Get the required registry data for these devices
    const registryDeviceData = deviceRegistry.getRegistryValues(
      devicesList, 
      [deviceRegistry.KEY_POWER_TYPE, deviceRegistry.KEY_CAPACITY_RATING]
    );

    log.debug("Extracting data");
    const devicesData = {};
    
    for (const device of devicesList) {
      const info = { cap: DEFAULT_CAP, type: UNKNOWN_GEN };

      if (registryDeviceData[device]) {
        const registryData = registryDeviceData[device];
        if (registryData[deviceRegistry.KEY_CAPACITY_RATING] != null) {
          info.cap = parseInt(registryData[deviceRegistry.KEY_CAPACITY_RATING]);
        }

        if (registryData[deviceRegistry.KEY_POWER_TYPE]) {
          info.type = registryData[deviceRegistry.KEY_POWER_TYPE];
        }

        info.preferences = devicePreferences[device] || {};
        info.virt = (info.type == TYPE_VIRTUAL_ESS);
      }

      const data = generateDeviceData(swarmdbDeviceData[device]);
      if (!swarmdbDeviceData[device]) {
        log.warn(`Missing ${device} from swarmdb response data`);
      }

      devicesData[device] = { info: info, data: data };
    } 

    log.debug("Data extracted");

    return devicesData
  }

  this.getSwarmAssociations = async function(device) {
    try {
      return await swarmStruc.getSwarmsForDevice(device);
    } catch (e) {
      if (e instanceof swarmStruc.DeviceNonExistentError) {
        // Device doesn't exist, no swarms
        return [];
      }
      log.error(`Could not get swarm associations for ${device}`, e);
      throw new DevicesManagerError("Unable to get swarms data");
    }
  }

  this.getManagerSwarm = async function(device) {
    try {
      return await swarmStruc.getSwarm(device);
    } catch (e) {
      log.error(`Could not get swarm for ${device}`, e);
      throw new DevicesManagerError("Unable to get swarms data");
    }
  }

  this.updatePcpAdminState = async function(device, value) {
    if (value != 0 && value != 1) {
      throw new Error("Invalid pcp admin value")
    }

    try {
      log.debug(`Updating admin PCP of ${device}: ${value}`);
      await swarmdb.setData(device, { [swarmdb.DEVICE_DATAKEY_PCPSTATE]: value });
      eventBroadcaster.broadcastDeviceChange(device, { pcp: [[new Date().getTime(), value]] });
    } catch (e) {
      log.error("Could not set device pcp admin state in swarmdb", e);
      throw new DevicesManagerError("Unable to set device data");
    }
  }

  this.updateAdminState = async function(device, value) {
    if (value != 0 && value != 1) {
      throw new Error("Invalid admin value")
    }

    try {
      log.debug(`Updating admin state of ${device}: ${value}`);
      await swarmdb.setData(device, { [swarmdb.DEVICE_DATAKEY_ADMSTATE]: value });
      eventBroadcaster.broadcastDeviceChange(device, { admin: [[new Date().getTime(), value]] });
    } catch (e) {
      log.error("Could not set device admin state in swarmdb", e);
      throw new DevicesManagerError("Unable to set device data");
    }
  }

  this.existsDevice = async function(device) {
    try {
      const devices = await swarmdb.getDevices();
      return devices.findIndex(function(deviceData) {
        return deviceData.name == device;
      }) > -1;
    } catch (e) {
      log.error("Could not get devices list from swarmdb", e);
      throw new DevicesManagerError("Unable to get device list");
    }
  }

  this.getDevices = async function() {
    try {
      const response = await swarmdb.getDevices();

      return response.map(function(value) {
        return value.name;
      });
    } catch (e) {
      log.error("Could not get devices list from swarmdb", e);
      throw new DevicesManagerError("Unable to get device list");
    }
  }

  this.getDevicePreferences = async function(device) {
    try {
      const preferences = await guiDb.getDevicesPreferences([device]);

      if (preferences != null) {
        return preferences[device];
      }

      return {};
    } catch (e) {
      log.error("Could not get device preferences from guiDb", e);
      throw new DevicesManagerError("Could not get device preferences");
    }
  }

  this.setDevicePreferences = async function(device, preferences) {
    for (const key in preferences) {
      if (DEVICE_PREFERENCE_KEYS[key] == null) {
        log.warn("Attempt to set illegal device preference: " + key);
        return false;
      } else if (!preferences[key] instanceof DEVICE_PREFERENCE_KEYS[key]) {
        log.warn("Attempt to set illegal device preference value: " + JSON.stringify(value));
        return false;
      } 
    }

    try {
      await guiDb.setDevicePreferences(device, preferences);
    } catch (e) {
      log.error(`Could not set preferences for ${device}`, e);
      throw new DevicesManagerError("Could not set preferences");
    }

    eventBroadcaster.broadcastDeviceChange(device, null, { preferences: preferences });
    return true;
  }
}

module.exports = new DevicesManager();
