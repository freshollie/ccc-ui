const guiDb = require("./db/gui-db.js");
const swarmdb = require("./db/swarmdb.js");
const predictionApi = require("./db/prediction-api.js");
const fahrplanApi = require("./db/fahrplan-api.js");
const swarmStruc = require("./db/swarm-struc.js");
const logging = require("../logging.js");
const eventBroadcaster = require("../socket/event-broadcaster.js");

function SwarmsManager() {
  const log = logging.createLogger("SwarmsManager");

  // List of data properties which are used 
  // to represent a swarm
  const SWARM_VALUES = [
    swarmdb.DATAKEY_FCR,
    swarmdb.DATAKEY_PFCR,
    swarmdb.DATAKEY_PV,
    swarmdb.DATAKEY_HAUSHOLDCONSUMP,
    swarmdb.DATAKEY_GRIDCONNECTION,
    swarmdb.DATAKEY_TEMP,
    swarmdb.DATAKEY_SOC,
    swarmdb.DATAKEY_MINSOC,
    swarmdb.DATAKEY_MAXSOC,
    swarmdb.DATAKEY_PMAXCHRG,
    swarmdb.DATAKEY_POPT,
    swarmdb.DATAKEY_PMAXDIS
  ]

  class SwarmsManagerError extends Error {};

  function convertPredictionData(predictionData) {
    const extractedData = {
      soc: [],
      maxPredSoc: [],
      minPredSoc: [],
      pv: [],
      hhCons: [],
      maxDis: [],
      maxChrg: [],
      optMaxChrg: [],
      mesFcr: []
    };

    if (predictionData) {
      for (const dp of predictionData) {
        extractedData.soc.push([dp.ts, dp.soc])
        extractedData.maxPredSoc.push([dp.ts, dp.soc_pi_p])
        extractedData.minPredSoc.push([dp.ts, dp.soc_pi_n])
        extractedData.pv.push([dp.ts, dp.pv])
        extractedData.hhCons.push([dp.ts, dp.hh])
        extractedData.maxDis.push([dp.ts, dp.ess_pmaxd])
        extractedData.maxChrg.push([dp.ts, dp.ess_pmaxc])
        extractedData.optMaxChrg.push([dp.ts, dp.batt_pmaxc])
        extractedData.mesFcr.push([dp.ts, dp.rl])
      }
    }

    return extractedData;
  }

  function convertFahrplanEvents(fahrplanEvents) {
    const parsedEvents = [];

    for (const event of fahrplanEvents) {
      parsedEvents.push({
        type: event.type,
        start: event.start,
        dur: event.dur
      });
    }

    return parsedEvents;
  }

  /**
   * Attempt to get a prediction for a swarm, returning nothing if this
   * was not possible. This is because prediction data might not exist for a swarm
   * 
   * @param {string[]} swarms 
   * @param {number} from 
   * @param {number} to 
   */
  async function tryGetPredictions(swarms, from, to) {
    try {
      return await predictionApi.getPredictions(swarms, from, to);
    } catch (e) {
      log.warn(`Could not get prediction for ${swarms}: ${e}`);
      return {}
    }
  }

  function extractSwarmValues(swarmdata, key) {
    if (!swarmdata) {
      return [];
    }

    const data = swarmdata[key];
    
    if (!data) {
      log.warn(`${key} missing from swarmdb response data`);
      return [];
    }

    // This was the response from a "latest" data request
    if (data.value) {
      return [[data.value.ts, data.value.mv]];
    }

    if (!data.values) {
      log.warn(`${key} missing from swarmdb response data`);
      return [];
    }
    
    // Go through each value and remove the ts and mv keys
    const values = [];
    for (let value of data.values) {
      values.push([value.ts, value.mv])
    }

    return values;
  }

  function makeSwcontrolSource(swarm) {
    return swarm + "." + swarmdb.SWCONTROL
  }

  /**
   * Return an association of swarms to the devices which they hold
   * 
   * @param {string[]} swarms 
   */
  this.getSwarmDevices = async function(swarms) {
    const deviceRequests = [];

    for (const swarm of swarms) {
      deviceRequests.push(swarmStruc.getDevices(swarm));
    }

    try {
      const deviceLists = await Promise.all(deviceRequests);
      // Map the swarms to their device lists
      return swarms.reduce((obj, k, i) => ({...obj, [k]: deviceLists[i] }), {})
    } catch (e) {
      log.error("Unable to get devices from swarm-struc", e);
      throw new SwarmsManagerError("Swarm-struc error");
    }
  }

  this.getSwarms = async function() {
    try {
      return await swarmStruc.getSwarms();
    } catch (e) {
      log.error("Error getting swarms from swarm-struc", e);
      throw new SwarmsManagerError("Unable to get swarms");
    }
  }

  this.existsSwarm = async function(swarm) {
    try {
      return (await swarmStruc.getSwarms()).indexOf(swarm) > -1;
    } catch (e) {
      log.error("Error getting swarms from swarm-struc", e);
      throw new SwarmsManagerError("Unable to check swarms");
    }
  }

  /**
   * Return a list of all swarms which are managed.
   * 
   * Including managed subswarms
   */
  this.getManagedSwarms = async function() {
    log.debug("Getting managed swarms");
    try {
      const [managedTopSwarms, allSwarms] = await Promise.all([
        swarmStruc.getManagedSwarms(),
        swarmStruc.getSwarms()
      ]);

      const allManagedSwarms = [];
      for (const swarm of allSwarms) {
        // The swarm is managed if it is part of the managed swarms, or it has a managed
        // swarm as its parent
        for (const managedTopSwarm of managedTopSwarms) {
          if (swarm == managedTopSwarm || swarm.startsWith(managedTopSwarm + ".")) {
            allManagedSwarms.push(swarm);
          }
        }
      }

      return allManagedSwarms;
    } catch (e) {
      log.error("Error getting managed swarms", e);
      throw new SwarmsManagerError("Unable to get managed swarms");
    }
  }

  this.createSwarm = async function(swarmname, managed) {
    try {
      if (managed) {
        log.debug(`Creating managed swarm ${swarmname}`);
        const vsn = await swarmStruc.createSwarm(swarmname);
      } else {
        log.debug(`Creating non-managed swarm ${swarmname}`);
        await swarmStruc.addSwarm(swarmname);
      }

      return true;
    } catch (e) {
      if (e instanceof swarmStruc.InvalidNameError || e instanceof swarmStruc.InvalidParentError) {
        log.debug(`Swarm creation invalid: ${e.message}`);
        throw e;
      }
      log.error(`Error creating swarm`, e);
      throw new SwarmsManagerError("Swarm-struc error");
    }
  }


  this.moveDevice = async function(swarm, device) {
    try {
      const response = await swarmStruc.setSwarm(swarm, device);
      return response.success;
    } catch (e) {
      log.error("Could not put ")
    }
  }

  /**
   * Set the devices which should be in a given virtual (non-managed) swarm
   * 
   * @param {*} swarm 
   * @param {*} devices 
   */
  this.setSwarmDevices = async function(swarm, devices) {
    try {
      // Remove all the devices from the swarm by deleting
      await swarmStruc.deleteSwarm(swarm);
      
      // Add the swarm again
      await swarmStruc.addSwarm(swarm);

      // and then add the devices to it
      const response = await swarmStruc.addSN(swarm, devices);
      return response.success;
    } catch (e) {
      throw new SwarmsManagerError("Unable to set devices of swarm");
    }
  }

  this.getSwarmsData = async function(swarms, from, to) {
    log.debug(`retrieving data for ${swarms.length} swarm(s)`);

    // Convert all swarms to their swcontrl sources.
    const swcontrolSources = [];
    for (const swarm of swarms) {
      swcontrolSources.push(makeSwcontrolSource(swarm));
    }

    const now = new Date().getTime();

    // Build up a set of promise data requests, which we then combine once all
    // have resolved
    const dataRequests = [this.getManagedSwarms(), this.getSwarmDevices(swarms), this.getVirtualSNs(swarms)];

    if (!from || !to) {
      dataRequests.push(swarmdb.getLastValues(swcontrolSources, SWARM_VALUES));
      dataRequests.push({});
      dataRequests.push({});
    } else {
      dataRequests.push(swarmdb.getSwarmValues(swcontrolSources, SWARM_VALUES, from, to));
      // Only 
      if (to > now) {
        dataRequests.push(tryGetPredictions(swarms, now, to));
      } else {
        dataRequests.push({});
      }

      dataRequests.push(this.tryGetFahrplanEvents(swarms, from, to));
    }

    let managedSwarms, swarmDevices, virtualSNs, swarmdbdata, predictionData, fahrplanData;
    try {
      // Unpack the finished promises
      [
        managedSwarms, 
        swarmDevices, 
        virtualSNs, 
        swarmdbdata, 
        predictionData, 
        fahrplanData 
      ] = await Promise.all(dataRequests);
    } catch (e) {
      log.error("Error retrieving data for swarm(s)", e);
      throw new SwarmsManagerError("Could not get data for swarm(s)");
    }

    log.debug(`Got data for ${swarms.length} swarms(s)`);

    // Loop through every swarm, generating the attributes from the swarmdb data
    const swarmsInfo = {};
    for (let i = 0; i < swarms.length; i++) {
      const swarm = swarms[i];
      const swcontrolSource = swcontrolSources[i];

      // Find out of this swarm is a managedSwarm
      const isManaged = (managedSwarms.indexOf(swarm) > -1);

      // The swcontrl keys are what comes back from the DB (Hopefully changed in the future)
      const swarmdata = swarmdbdata[swcontrolSource];
      const devices = swarmDevices[swarm];
      const vsn = virtualSNs[swarm];
      const swarmPrediction = convertPredictionData(predictionData[swarm]);
      const swarmFahrplan = fahrplanData[swarm] || [];

      swarmsInfo[swarm] = {
        info: {
          managed: isManaged,
          devices: devices,
          vsn: vsn
        },
        data: {
          maxDis: extractSwarmValues(swarmdata, swarmdb.DATAKEY_PMAXCHRG),
          maxChrg: extractSwarmValues(swarmdata, swarmdb.DATAKEY_PMAXDIS),
          optMaxChrg: extractSwarmValues(swarmdata, swarmdb.DATAKEY_POPT),
          soc: extractSwarmValues(swarmdata, swarmdb.DATAKEY_SOC),
          maxSoc: extractSwarmValues(swarmdata, swarmdb.DATAKEY_MAXSOC),
          minSoc: extractSwarmValues(swarmdata, swarmdb.DATAKEY_MINSOC),
          temp: extractSwarmValues(swarmdata, swarmdb.DATAKEY_TEMP),
          pv: extractSwarmValues(swarmdata, swarmdb.DATAKEY_PV),
          hhCons: extractSwarmValues(swarmdata, swarmdb.DATAKEY_HAUSHOLDCONSUMP),
          grid: extractSwarmValues(swarmdata, swarmdb.DATAKEY_GRIDCONNECTION),
          calcFcr: extractSwarmValues(swarmdata, swarmdb.DATAKEY_FCR),
          mesFcr: extractSwarmValues(swarmdata, swarmdb.DATAKEY_PFCR)
        },
        prediction: swarmPrediction,
        fahrplan: swarmFahrplan
      }

      if (!swarmdata) {
        log.warn(`Missing ${swcontrolSource} from swarmdb response data`);
      }
    }

    log.debug("Data extraction complete");
    return swarmsInfo;
  }

  this.setOperator = async function(swarm, operatorId) {
    const allSwarms = await this.getSwarms();

    // When the user takes control of a top swarm, they take control of all of it's
    // children
    const forSwarms = [swarm];
    for (const possibleSwarm of allSwarms) {
      if (possibleSwarm.toLowerCase().startsWith(`${swarm.toLowerCase()}.`)) {
        forSwarms.push(possibleSwarm);
      }
    }

    log.debug(`Setting operator (${operatorId}) for swarms ${forSwarms}`);

    await guiDb.setSwarmOperators(operatorId, forSwarms);
    eventBroadcaster.broadcastOperatorChange(forSwarms);
  }

  this.getOperators = async function(swarms) {
    try {
      return await guiDb.getSwarmOperators(swarms);
    } catch (e) {
      log.error(`Error fetching operators for swarms`, e);
      throw new SwarmsManagerError(`Operators-DB error`);
    }
  }

  async function tryGetVirtualSN(swarm) {
    try {
      return await swarmStruc.getVirtualSN(swarm);
    } catch (e) {
      // If the swarm was not a valid virtual swarm, then we just ignore the
      // error
      if (e instanceof swarmStruc.InvalidSubswarmError) {
        return undefined;
      }
      log.error(`Could not get virtual SN of ${swarm}: ${e.message}`);
      throw e;
    }
  }

  this.getVirtualSNs = async function(swarms) {
    log.debug(`Getting virtual sns for ${swarms.length} swarms`);

    const requests = [];
    for (const swarm of swarms) {
      requests.push(tryGetVirtualSN(swarm));
    }

    const virtualSNs = await Promise.all(requests);

    // Return a map of swarms to their virtual sns
    return swarms.reduce((obj, k, i) => ({...obj, [k]: virtualSNs[i] }), {})
  }

  this.removeSwarm = async function(swarm) {
    log.debug(`Removing ${swarm}`);

    try {
      const isManaged = (await this.getManagedSwarms()).indexOf(swarm) > -1;
      if (isManaged) {
        const response = await swarmStruc.removeSwarm(swarm);
        return response.success;
      } else {
        const response = await swarmStruc.deleteSwarm(swarm);
        return response.success;
      }
    } catch (e) {
      if (e instanceof swarmStruc.InvalidSubswarmError || e instanceof swarmStruc.SwarmActiveError) {
        log.debug(`${swarm} was not removed as did not comply with rules: ${e.message}`);
        throw e;
      }
      
      log.error(`Error removing swarm ${swarm}`, e);
      throw new SwarmsManagerError("Swarm-struc error");
    }
  }

  this.getFahrplanEvents = async function(swarms, from, to) {
    const requests = [];
    for (const swarm of swarms) {
      requests.push(fahrplanApi.getEvents(swarm, from, to));
    }

    const fahrplans = await Promise.all(requests);
    return swarms.reduce((obj, k, i) => ({...obj, [k]: fahrplans[i] }), {});
  }

  this.getFahrplanEvent = async function(swarm, id) {
    try {
      return await fahrplanApi.getEvent(swarm, id);
    } catch (e) {
      log.error(`Could get fahrplan event ${swarm}:${id} - ${e.message}`);
      throw new SwarmsManagerError("Fahrplan API error");
    }
  }

  this.requestFahrplanEvent = async function(swarm, request) {
    // The source cannot equal anything other than CHPP
    if (request.source && request.source != "CHPP" && request.source != "MANUAL") {
      return false;
    }

    // Trades cannot be requested through ccc-ui
    if (request.trade == true) {
      return false;
    }

    try {
      const success = await fahrplanApi.requestEvent(swarm, request);
      
      if (success) {
        eventBroadcaster.broadcastFahrplanChange(swarm);
      }
      return success;
    } catch (e) {
      if (e instanceof fahrplanApi.IllegalOperationError) {
        throw e;
      }
      log.error(`Could not request fahrplan event: ${request}: ${e.message}`);
      throw new SwarmsManagerError("Fahrplan API error");
    }

    return true;
  }

  this.updateFahrplanEvent = async function(swarm, id, newType) {
    let success = false;
    try {
      success = await fahrplanApi.updateEvent(swarm, id, newType);
    } catch (e) {
      if (e instanceof fahrplanApi.IllegalOperationError) {
        throw e;
      }
      log.error(`Could not update fahrplan event: ${swarm}/${id}: ${e.message}`);
      throw new SwarmsManagerError("Fahrplan API error");
    }

    if (!success) {
      throw new SwarmsManagerError("Could not update fahrplan event");
    }

    if (success) {
      eventBroadcaster.broadcastFahrplanChange(swarm);
    }
    return success;
  }

  this.cancelFahrplanEvent = async function(swarm, id, newType) {
    let success = false;
    try {
      success = await fahrplanApi.cancelEvent(swarm, id, newType);
    } catch (e) {
      if (e instanceof fahrplanApi.IllegalOperationError) {
        throw e;
      }
      log.error(`Could not update fahrplan event: ${swarm}/${id}: ${e.message}`);
      throw new SwarmsManagerError("Fahrplan API error");
    }

    if (!success) {
      throw new SwarmsManagerError("Could not update fahrplan event");
    }

    if (success) {
      eventBroadcaster.broadcastFahrplanChange(swarm);
    }
    return success;
  }

  this.tryGetFahrplanEvents = async function(swarms, from, to) {
    try {
      return await this.getFahrplanEvents(swarms, from, to);
    } catch (e) {
      if (e instanceof fahrplanApi.IllegalOperationError) {
        throw e;
      }
      log.warn(`Failed to get fahrplan events: ${e.message}`);
      return {};
    }
  }
}

module.exports = new SwarmsManager();
