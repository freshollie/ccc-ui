const guiDb = require("./db/gui-db.js");
const logging = require("../logging.js");
const bcrypt = require("bcrypt");

function UserdataManager() {
  const log = logging.createLogger("UserdataManager");

  // List of valid preferences, so that
  // we don't just give away storage
  const PREFERENCE_KEYS = [
    "language",
    "defaultSwarm",
    "alarmSounds"
  ];

  // Default credentials for testing
  const ADMIN_USERNAME = "admin";
  const DEFAULT_ADMIN_PASSWORD = "alfredolinguini";
  const ADMIN_ROLE = "admin";

  const HASH_SALT = 10;

  const self = this;

  class UserdataManagerError extends Error { };

  async function createPassHash(password) {
    return await bcrypt.hash(password, HASH_SALT);
  }

  async function ensureCredential(username, pass, roles) {
    if (!await guiDb.existsUsername(username)) {
      await guiDb.insertUser(username, await createPassHash(pass));
    }

    const userId = (await guiDb.getUserCredentials(username)).id;
    await self.setUserRoles(userId, roles);
  }
  /**
   * Check if the default admin credentials exist, and if not create the user
   */
  this.createDefaultCredentails = async function () {
    try {
      await ensureCredential(ADMIN_USERNAME, DEFAULT_ADMIN_PASSWORD, {[ADMIN_ROLE]: {}});
      await ensureCredential("test1", "test", {"DormantSwarm": {readonly : false}, "RealSwarm": { readonly: true } });
      await ensureCredential("eddie", "antdec", { [ADMIN_ROLE]:  {} });
      await ensureCredential("oliver", "hello123", { [ADMIN_ROLE]: {} });
    } catch (e) {
      log.error(`Error creating default credentials: ${e.message}`);
    }
  }

  this.existsUsername = async function (username) {
    try {
      return await guiDb.existsUsername(username);
    } catch (e) {
      log.error(`Error checking if ${username} exists in users`, e);
      return new UserdataManagerError("Database error");
    }
  }

  this.existsUser = async function (userId) {
    try {
      return await guiDb.existsUser(userId);
    } catch (e) {
      log.error(`Error checking if ${userId} exists in users`, e);
      return new UserdataManagerError("Database error");
    }
  }

  this.verifyPassword = async function (userId, password) {
    try {
      const username = (await guiDb.getUserDetails(userId)).username;
      const credentials = await guiDb.getUserCredentials(username);
      return await bcrypt.compare(oldPassword, credentials.hashedPass);
    } catch (e) {
      log.error(`Database error. Could not verify password for ${userId}`, e);
      throw new UserdataManagerError("Database error");
    }
  }

  this.setPassword = async function (userId, newPassword) {
    let hashedPass = await createPassHash(newPassword);
    try {
      await guiDb.updateUserPassword(userId, hashedPass);
      return true;
    } catch (e) {
      log.error(`Could not update password for ${userId} with ${hashedPass}`, e);
      throw new UserdataManagerError("Database error");
    }
  }

  this.createRole = async function (rolename) {
    try {
      return await guiDb.createRole(rolename);
    } catch (e) {
      log.error(`Could not create role ${rolename}`, e);
      throw new UserdataManagerError("Database error");
    }
  }

  this.getRoles = async function (userId = null) {
    try {
      return await guiDb.getUserRoles(userId);
    } catch (e) {
      log.error(`Failed to get roles for ${userId} from guidb`, e);
      throw new UserdataManagerError("Database");
    }
  }

  /**
   * return all the user details of the given user id
   */
  this.getUserDetails = async function (userId) {
    try {
      const details = await guiDb.getUserDetails(userId);

      if (details) {
        // The id is not required, as it is given as the map key
        delete details.id;
      }

      return details;
    } catch (e) {
      log.error(`Could not retrieve user details for ${userId}`, e);
      throw new UserdataManagerError("Database error. Could not retrieving user details");
    }
  }

  this.getUsers = async function () {
    let users;
    try {
      users = await guiDb.getUserDetails();
    } catch (e) {
      log.error("Could not get users from guiDB", e);
      throw new UserdataManagerError
    }

    const usersMap = {};

    for (const user of users) {
      usersMap[user.id] = user;
      delete user.id;
    }

    return usersMap;
  }

  this.isAdmin = async function (userId) {
    log.silly(`Checking if ${userId} is admin`);
    try {
      return (await guiDb.getRoleForUser(userId, ADMIN_ROLE)) != null;
    } catch (e) {
      log.error(`Failed to check admin privilages for ${userId}`, e);
      throw new UserdataManagerError("Database error. Could not retrieving user details");
    }
  }

  this.createNewUser = async function (username, password) {
    let hashedPass = await createPassHash(password);
    try {
      return await guiDb.insertUser(username, hashedPass);
    } catch (e) {
      log.error(`Could not create user ${username}, ${hashedPass}`, e);
      throw new UserdataManagerError("Error creating new user");
    }
  }

  this.removeAllRolesFromUser = async function (userId) {

    try {
      await guiDb.removeAllRolesFromUserId(userId);
    } catch (e) {
      log.error(`Could not remove all roles from ${userId}: `, e);
      throw new UserdataManagerError("Error removing all roles.");
    }
  }

  this.setUserRoles = async function (userId, roles) {
    if (userId == null) {
      throw new Error("UserId must be an int");
    }

    try {
      await this.removeAllRolesFromUser(userId);

      for (const role in roles) {
        let roleId = await guiDb.getRoleIdForRole(role);
        if (roleId == null) {
          roleId = await guiDb.createRole(role);
        }

        await guiDb.addRoleToUser(userId, roleId, roles[role].readonly || false);
      }
    } catch (e) {
      log.error(`Error while setting roles for the user Id: ${userId}`, e);
      throw new UserdataManagerError("Database error");
    }
  }

  this.savePreferences = async function (userId, preferences) {
    let validatedPreferences = {};
    for (let preferenceKey in preferences) {
      if (PREFERENCE_KEYS.indexOf(preferenceKey) > -1) {
        let preferenceValue = preferences[preferenceKey].toString();
        if (preferenceValue.length > 100) {
          continue;
        }

        validatedPreferences[preferenceKey] = preferenceValue;
      }
    }

    log.debug(`Updating preferences for ${userId}: ${JSON.stringify(validatedPreferences)}`);

    try {
      await guiDb.updateUserPreferences(userId, validatedPreferences);
    } catch (e) {
      log.error(`Could not update preferences for ${userId}, ${validatedPreferences}`, e);
      throw new UserdataManagerError("Database error");
    }
  }

  this.getPreferences = async function (userId) {
    try {
      const details = await guiDb.getUserDetails(userId);
      if (!details)
        return {};
      return details.preferences;
    } catch (e) {
      log.error(`Could not get preferences for ${userId}`, e);
      throw new UserdataManagerError("Database error");
    }
  }

  this.removeUser = async function (userId) {
    try {
      await guiDb.removeUser(userId);
    } catch (e) {
      log.error(`Could not remove user with Id: ${userId} Error:`, e);
      throw new UserdataManagerError("Database error");
    }
  }
}

module.exports = new UserdataManager();
