// import logging and use logging.createLogger(name) and then log.info, .debug...
const winston = require("winston");
const config = require("./config.js");

// Only the first logger should handle exceptions
let unhandledExceptions = true;

module.exports = {
  createLogger: function(label) {
    const log = new (winston.Logger)({
      transports: [
        new (winston.transports.Console) ({
          humanReadableUnhandledException: unhandledExceptions,
          timestamp: true,
          handleExceptions: unhandledExceptions,
          colorize: true,
          level: config.logLevel,
          label: label,
          prettyPrint: true
        })
      ]
    });

    // Only the first logger needs to catch unhandled exceptions
    unhandledExceptions = false;

    return log;
  }
}

module.exports.createLogger("base");
