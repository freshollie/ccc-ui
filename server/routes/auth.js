const { Router }  = require("express");
const authManager = require("../authentication/auth-manager.js");
const userdataManager = require("../data/userdata-manager.js");
const logging = require("../logging.js");

const log = logging.createLogger("/auth");

const router = Router();

router.post("/", async function(req, res) {
  try {
    let user = await authManager.login(req, res);
    if (user) {
      res.send({success: true});
    } else {
      res.status(401).send({ error: "Unauthorised" });
    }
  } catch (e) {
    res.status(500).send({success: false, error: e.message});
  }
});

// Anything past login requires auth check
router.use(authManager.authCheck);

router.get("/", async function(req, res) {
  try {
    res.send(await userdataManager.getUserDetails(req.getUserId()));
  } catch (e) {
    res.status(500).send({error: e.message});
  }
});

router.put("/password", async function(req, res) {
  let oldPassword = req.body.old;
  let newPassword = req.body.new;

  if (!oldPassword || !newPassword) {
    return res.status(422).send({ error: "Invalid parameters" });
  }

  try {
    if (!await userdataManager.verifyPassword(req.getUserId(), oldPassword)) {
      return res.status(401).send({ error: "Unauthorised" })
    }
    
    await userdataManager.setPassword(req.getUserId(), newPassword);
    
    res.send({success: true});
  } catch (e) {
    res.send({error: e.message});
  }
});

router.put("/preferences", async function(req, res) {
  let preferences = req.body.preferences;
  
  if (!preferences || !preferences instanceof Object) {
    return res.status(422).send({ error: "Invalid arguments" })
  }

  try {
    await userdataManager.savePreferences(req.getUserId(), preferences);
    res.send({ success: true });
  } catch (e) {
    res.status(500).send({error: e.message});
  }
});

router.get("/preferences", async function(req, res) {
  try {
    res.send({ preferences: await userdataManager.getPreferences(req.getUserId()) });
  } catch (e) {
    res.status(500).send({error: e.message});
  }
});

router.delete("/", function(req, res) {
  authManager.logout(req);
  res.send({ success: true });
});

router.all("*", function(req, res) {
  res.sendStatus(404);
});

module.exports = router;
