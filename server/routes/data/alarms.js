const { Router } = require("express");

const dataAccessManager = require("../../data/data-access-manager.js");
const alarmSpec = require("../../data/db/alarm-spec.js");
const logging = require("../../logging.js");

const log = logging.createLogger("/alarms");

const router = Router();

/**
 * Returns all alarms
 */
router.get("/", async function(req, res) {
  const dataType = req.query.data
  // Return all alarms this user can access, with the alarm spec if specified

  if (dataType && dataType != "alarms" && dataType != "spec") {
    return res.status(422).send({error: "type must be alarms or spec"});
  } 

  const dataRequests = [];

  if (!dataType || dataType == "alarms") {
    dataRequests.push(dataAccessManager.getAlarms(req.getUserId()));
  } else {
    dataRequests.push(null);
  }

  if (!dataType || dataType == "spec") {
    dataRequests.push(alarmSpec.getSpec());
  } else {
    dataRequests.push(null);
  }


  let alarms, spec;
  try {
    [alarms, spec] = await Promise.all(dataRequests);
  } catch(e) {
    return res.status(500).send({error: e.message});
  }

  const response = {};
  if (alarms) {
    response.alarms = alarms;
  }

  if (spec) {
    response.spec = spec
  }

  res.send(response);
});

router.get("/:alarmId", async function(req, res) {
  const alarmId = req.params.alarmId;
  try {
    if (!alarmId || !await dataAccessManager.canAccessAlarm(req.getUserId(), alarmId)) {
      return res.status(404).send({ error: "Not found" });
    }

    res.send({ [alarmId]: await alarmsManager.getAlarm(alarmId) });
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

/**
 *
 */
router.patch("/:alarmId", async function(req, res) {
  let alarmId = req.params.alarmId;
  let muted = req.body.muted;

  if (muted == null || typeof muted != 'boolean' || Object.keys(req.body) > 1) {
    return res.status(422).send({ error: "Invalid attribute" });
  }

  try {
    if (!await dataAccessManager.setAlarmMute(req.getUserId(), alarmId, muted)) {
      return res.status(404).send({ error: "Not found" });
    }

    res.send({success: true});
  } catch (e) {
    res.status(500).send({error: e.message});
  }
});

router.delete("/:alarmId", async function(req, res) {
  let alarmId = req.params.alarmId;

  try {
    if (!await dataAccessManager.cancelAlarm(req.getUserId(), alarmId)) {
      return res.status(404).send({ error: "Not found" });
    }

    res.send({success: true});
  } catch (e) {
    res.status(500).send({error: e.message});
  }
});

module.exports = router;
