const { Router }  = require("express");
const dataAccessManager = require("../../data/data-access-manager.js");

const router = Router();

router.get("/", async function(req, res) {
  const swarm = req.query.swarm;
  try {
    res.send({ devices: await dataAccessManager.getDevices(req.getUserId(), swarm) });
  } catch (e) {
    res.status(500).send({ error: e.message })
  }
});

router.get("/:device", async function(req, res) {
  const device = req.params.device;
  let from = req.query.from,
      to = req.query.to;

  if (from != null) {
    from = parseInt(from);
  }

  if (to != null) {
    to = parseInt(to);
  } else {
    to = new Date().getTime()
  }

  if (from > to) {
    return res.status(422).send({error: "Invalid time range"});
  }
  
  try {
    const deviceData = await dataAccessManager.getDeviceData(req.getUserId(), device, from, to);
    
    if (!deviceData) {
      return res.status(404).send({ error: "Not found" });
    }

    res.send({ [device]: deviceData });
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

router.get("/:device/preferences", async function(req, res) {
  const device = req.params.device;

  try {
    const preferences = await dataAccessManager.getDevicePreferences(req.getUserId(), device);
    if (!preferences) {
      // Invalid, or doesn't exist
      return res.status(404).send({ error: "No found"});
    }

    res.send({ preferences: preferences });
  } catch (e) {
    console.error(e);
    res.status(500).send({ error: e.message });
  }
});

router.put("/:device/preferences", async function(req, res) {
  const device = req.params.device;
  const preferences = req.body.preferences;

  try {
    const success = await dataAccessManager.setDevicePreferences(req.getUserId(), device, preferences);

    // No permission, or bad preferences
    if (!success) {
      return res.status(403).send({ "error": "Forbidden" });
    }

    res.send({ success: true });
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

/**
 * Update an attribute of the device
 */
router.patch("/:device", async function(req, res) {
  const device = req.params.device;

  if (!req.body.data) {
    return res.status(422).send({ error: "Invalid data" });
  }

  const pcp = req.body.data.pcp;
  const admin = req.body.data.admin;

  const updateData = {};

  if (pcp != null) {
    if (pcp != 0 && pcp != 1) {
      return res.status(422).send({ error: "PCP admin value" });
    }

    updateData.pcp = pcp;
  }

  if (admin != null) {
    if (admin != 0 && admin != 1) {
      return res.status(422).send({ error: "Admin state value" });
    }
    updateData.admin = admin;
  }

  try {
    if (await dataAccessManager.updateDevice(req.getUserId(), device, updateData)) {
      res.send({ success: true });
    } else {
      res.status(403).send({ error: "Forbidden" });
    }
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

module.exports = router;
