const { Router }  = require("express");
const authManager = require("../../authentication/auth-manager.js");
const swarms = require("./swarms.js");
const devices = require("./devices.js");
const alarms = require("./alarms.js");
const users = require("./users.js");

const router = Router();

router.use(authManager.authCheck);

router.use("/swarms", swarms);

router.use("/devices", devices);

router.use("/alarms", alarms);

router.use("/users", users);

router.get("/", function(req, res) {
  res.send({
    "/swarms": "swarms",
    "/devices": "devices",
    "/alarms": "alarms",
    "/users": "users",
  });
});

router.all("*", function(req, res) {
  res.status(404).send({ error: "Not found" });
});

module.exports = router;
