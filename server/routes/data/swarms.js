const { Router }  = require("express");
const dataAccessManager = require("../../data/data-access-manager.js");
const authManager = require("../../authentication/auth-manager.js");

const swarmsManager = require("../../data/swarms-manager.js");
const devicesManager = require("../../data/devices-manager.js");

const { IllegalOperationError } = require("../../data/db/fahrplan-api.js");

const logging = require("../../logging.js");
const log = logging.createLogger("/swarms");

const router = Router();

/**
 * Get information about all swarms
 */
router.get("/", async function(req, res) {
  try {
    // Query for all swarms latest data
    res.send({ swarms: await dataAccessManager.getSwarms(req.getUserId()) });
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

/**
 * Get information about a specific swarm
 */
router.get("/:swarm", async function(req, res) {
  let swarm = req.params.swarm;
  let from = req.query.from;
  let to = req.query.to;

  if (from != null) {
    from = parseInt(from);
  }

  if (to != null) {
    to = parseInt(to);
  } else {
    to = new Date().getTime()
  }

  if (from > to) {
    return res.status(422).send({error: "Invalid time range"});
  }
  
  try {
    const swarmData = await dataAccessManager.getSwarmData(req.getUserId(), swarm, from, to);
    if (swarmData == null) {
      return res.status(404).send({error: "Not found"});
    }

    res.send({ [swarm]: swarmData });
  } catch(e) {
    res.status(500).send({ error: e.message });
  }
});

/**
 * Used to take control of a swarm
 */
router.put("/:swarm/operating", async function(req, res) {
  const swarm = req.params.swarm;
  
  try {
    const success = await dataAccessManager.updateSwarmOperator(req.getUserId(), swarm);
    if (!success) {
      return res.status(403).send({ error: "Forbidden" });
    }

    res.send({ success: true });
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

router.get("/:swarm/fahrplan", async function(req, res) {
  let swarm = req.params.swarm;
  let from = req.query.from;
  let to = req.query.to;

  if (from != null) {
    from = parseInt(from);
  } else {
    from = new Date().getTime();
  }

  if (to != null) {
    to = parseInt(to);
  } 

  if (from > to) {
    return res.status(422).send({error: "Invalid time range"});
  }
  
  try {
    const fahrplanEvents = await dataAccessManager.getSwarmFahrplan(req.getUserId(), swarm, from, to);
    if (fahrplanEvents == null) {
      return res.status(404).send({error: "Not found"});
    }

    res.send({ fahrplan: fahrplanEvents });
  } catch(e) {
    res.status(500).send({ error: e.message });
  }
});

router.post("/:swarm/fahrplan", async function(req, res) {
  let swarm = req.params.swarm;
  const fahrplanRequest = req.body;

  try {
    const authorised = await dataAccessManager.requestSwarmFahrplanEvent(req.getUserId(), swarm, fahrplanRequest)

    if (!authorised) {
      return res.status(403).send({ error: "Forbidden" });
    }

    res.send({ success: true });
  } catch (e) {
    if (e instanceof IllegalOperationError) {
      return res.status(422).send({ error: e });
    }
    res.status(500).send( { error: e.message });
  }
});

router.get("/:swarm/fahrplan/:eventId", async function(req, res) {
  let swarm = req.params.swarm;
  const eventId = req.params.eventId;

  try {
    const event = await dataAccessManager.getSwarmFahrplanEvent(req.getUserId(), swarm, eventId);

    if (!event) {
      return res.status(404).send({ error: "Not found" });
    }

    res.send({ [eventId]: event });
  } catch (e) {
    if (e instanceof IllegalOperationError) {
      return res.status(422).send({ error: e });
    }
    res.status(500).send({ error: e.message });
  }
});

router.patch("/:swarm/fahrplan/:eventId", async function(req, res) {
  let swarm = req.params.swarm;
  const eventId = req.params.eventId;
  const newType = req.body.type;

  try {
    const success = await dataAccessManager.updateSwarmFahrplanEvent(req.getUserId(), swarm, eventId, newType);

    if (!success) {
      return res.status(403).send({ error: "Forbidden" });
    }

    res.send({ success: true });
  } catch (e) {
    if (e instanceof IllegalOperationError) {
      return res.status(422).send({ error: e });
    }
    res.status(500).send({ error: e.message });
  }
});

router.delete("/:swarm/fahrplan/:eventId", async function(req, res) {
  let swarm = req.params.swarm;
  const eventId = req.params.eventId;

  try {
    const success = await dataAccessManager.cancelSwarmFahrplanEvent(req.getUserId(), swarm, eventId);

    if (!success) {
      return res.status(403).send({ error: "Forbidden" });
    }

    res.send({ success: true });
  } catch (e) {
    if (e instanceof IllegalOperationError) {
      return res.status(422).send({ error: e });
    }
    res.status(500).send({ error: e.message });
  }
});

/*** Swarm Administration ***/

// Access to further endpoints requires admin privilages
router.use(authManager.adminCheck);

router.post("/", async function(req, res) {
  const swarmname = req.body.name;
  const managed = req.body.managed;

  if (typeof managed != "boolean") {
    return res.status(422).send({ error: "managed must be a boolean" });
  }

  if (typeof swarmname != "string") {
    return res.status(422).send({ error: "name must be a string" });
  }

  try {
    if (await swarmsManager.existsSwarm(swarmname)) {
      return res.status(422).send({ error: "Swarm already exists" });
    }

    res.send({ success: await swarmsManager.createSwarm(swarmname, managed) });
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

/**
 * Put a device in a swarm
 */
router.post("/:swarm/devices", async function(req, res) {
  const swarm = req.params.swarm;
  const device = req.body.device;

  if (typeof device != "string") {
    return res.status(422).send({ error: "device must be an string" });
  }

  try {
    const managedSwarms = await swarmsManager.getManagedSwarms();
    if (managedSwarms.indexOf(swarm) < 0) {
      return res.status(422).send({ error: `${swarm} is not a managed swarm` });
    }

    if (!await devicesManager.existsDevice(device)) {
      return res.status(422).send({ error: "Device doesn't exist" });
    }

    res.send({ success: await swarmsManager.moveDevice(swarm, device) });
  } catch (e) {

    res.status(500).send({ error: e.message });
  }
});

router.put("/:swarm/devices", async function(req, res) {
  const swarm = req.params.swarm;
  const devices = req.body.devices;

  if (devices != null) {
    if (!Array.isArray(devices)) {
      return res.status(422).send({ error: "devices must be an array of strings" });
    }

    for (const device of devices) {
      log.debug(typeof device);
      if (typeof device != "string") {
        return res.status(422).send({ error: "devices must be an array of strings" });
      }
    }

    try {
      if (!await swarmsManager.existsSwarm(swarm)) {
        return res.status(404).send({ error: "Not found" });
      }

      const managedSwarms = await swarmsManager.getManagedSwarms();
      if (managedSwarms.indexOf(swarm) > -1) {
        return res.status(422).send({ error: `Swarm must not be managed to set its devices` });
      }

      const validDevices = await devicesManager.getDevices();

      for (const device of devices) {
        if (validDevices.indexOf(device) < 0) {
          return res.status(422).send({ error: `${device} is not a valid device` });
        }
      }

      res.send({ success: await swarmsManager.setSwarmDevices(swarm, devices) });
    } catch (e) {
      log.error(e);
      res.status(500).send({ error: e.message });
    }
  } else {
    res.status(402).send({ error: "Invalid property to patch"});
  }
});

router.delete("/:swarm", async function(req, res) {
  const swarm = req.params.swarm;

  try {
    if (!await swarmsManager.existsSwarm(swarm)) {
      return res.status(404).send({ error: "Not found" });
    }

    res.send({ success: await swarmsManager.removeSwarm(swarm) });
  } catch (e) {
    res.status(500).send({ error: e.message });
  }
});

module.exports = router;
