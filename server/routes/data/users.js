const { Router }  = require("express");
const userdataManager = require("../../data/userdata-manager.js");
const authManager = require("../../authentication/auth-manager.js");
const logging = require("../../logging.js");

const log = logging.createLogger("/users");

const router = Router();

/**
 * Access to any of these endpoints requires admin privilages
 */
router.use(authManager.adminCheck);

router.get("/", async function(req, res) {
  try {
    const users = await userdataManager.getUsers();
    return res.send({users: users});
  } catch (e) {
    return res.status(500).send({ error: e.message });
  }
});

router.get("/:userId", async function (req, res) {
  const userId = req.params.userId;

  try {
    const details = await userdataManager.getUserDetails(userId);
    if (!details) {
      return res.status(404).send({ error: `Not found` });
    }

    res.send({ [userId]: details });
  } catch (e) {
    return res.status(500).send({ error: e.message });
  }
});

router.post("/", async function(req, res) {
  let username = req.body.username;
  let password = req.body.password;
  let roles = req.body.roles;

  if (!username || !password) {
    return res.status(422).send({ error: "Invalid parameters" });
  }

  try {
    if (await userdataManager.existsUsername(username)) {
      return res.status(422).send({ error: "User already exists" });
    }

    const userId = await userdataManager.createNewUser(username, password);
    if (roles) {
      await userdataManager.setUserRoles(userId, roles);
    }    

    res.send({success: true});
  } catch (e) {
    log.error("Error creating user", e);
    res.send({ error: e.message, success: false });
  }
});

router.delete("/:userId", async function(req, res) {
  const userId = req.params.userId;
  try {
    await userdataManager.removeUser(userId);
    res.send({success: true});
  } catch (e) {
    return res.status(500).send({ error: e.message });
  }
});

router.put("/:userId/password", async function(req, res) {
  let userID = req.params.userId;
  let newPassword = req.body.password;

  if (!userID || !newPassword) {
    return res.status(422).send({ error: "Invalid parameters" });
  }

  try {
    if (!await userdataManager.existsUser(userID)) {
      return res.status(422).send({ error: "User does not exist" })
    }

    await userdataManager.setPassword(userID, newPassword);
    res.send({success: true});
  } catch (e) {
    res.send({error: e.message});
  }
});

router.put("/:userId/roles", async function(req, res) {
  let userId = req.params.userId;
  let roles = req.body.roles;

  try {
    if (roles.length < 1 || !await userdataManager.existsUser(userId)) {
      return res.status(422).send({ error: "Invalid parameters" })
    }

    await userdataManager.setUserRoles(userId, roles);

    res.send({success: true});
  } catch (e) {
    log.error("Error setting user roles.", e);
    res.send({error: e.message});
  }
});

module.exports = router;