const http = require("http");

// Parse the input args
require("./args-parser.js").parse();

const logging = require("./logging.js");
const config = require("./config.js");

const eventBroadcaster = require("./socket/event-broadcaster.js");
const cccui = require("./app.js");

const server = http.createServer(cccui);
eventBroadcaster.initialise(server);

const log = logging.createLogger("server");

server.listen(config.port, function() {
  log.info(`CCC-UI running on port ${config.port}`)
});
