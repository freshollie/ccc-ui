const WebSocket = require("ws");
const logging = require("../logging.js");
const config = require("../config.js");
const redis = require("redis");
const sessionManager = require("../authentication/session-manager.js");

/**
 * Broadcast events to all connected clients, so that the UI stays consistent across all connected
 * clients.
 * 
 * Redis is used to handle communication between replicated UI nodes.
 */
function EventBroadcaster() {
  const log = logging.createLogger("EventBroadcaster");
  const sessionParser = sessionManager.getSessionParser();
  
  const EVENT_TYPE_ALARM = "alarm";
  const EVENT_TYPE_DEVICE = "device";
  const EVENT_TYPE_FAHRPLAN = "fahrplan";
  const EVENT_TYPE_OPERATOR = "operator";

  const EVENTS_CHANNEL = "events";
  let redisConnected = false;
  let socket;
  let publisher;
  let subscriber;

  /**
   * Verify that a client which is connecting has a
   * logged in session
   */
  function verifyClient(client, done) {
    sessionParser(client.req, {}, function() {
      const authed = (client.req.session && client.req.session.passport);
      if (authed) {
        log.debug("Client connected");
      }
      done(authed);
    });
  }

  /**
   * Initialise the publisher and subscriber connections to
   * Redis
   */
  function initialiseRedis() {
    log.info(`Connecting to redis: ${config.redis.host}, ${config.redis.host}`);

    // Make a publisher client that we broadcast our data to the chanel with
    publisher = redis.createClient(config.redis.port, config.redis.host);
    publisher.on("error", function(error) {
      log.error(`Redis publisher error: ${error.message}`);
      redisConnected = false;
    }).on("connect", function() {
      log.info("Redis publisher connected");
      redisConnected = true;
    });

    // Make a subscriber client that we receive events that we broadcast to
    // the channel
    subscriber = redis.createClient(config.redis.port, config.redis.host);
    subscriber.on("error", function(error) {
      log.error(`Redis subscriber error: ${error.message}`);
    }).on("connect", function() {
      log.info("Redis subscriber connected");
    });

    // Listen for events on the events channel we are publishing to 
    subscriber.subscribe(EVENTS_CHANNEL);
    subscriber.on("message", function(channel, eventString) {
      onEventBroadcast(eventString);
    });
  }

  this.initialise = function(server) {
    log.info("Initialising socket on socket");

    initialiseRedis();

    // The socket requires that the user has
    // a passport session to be able to connect to the socket
    socket = new WebSocket.Server({
      path: "/socket",
      verifyClient: verifyClient,
      server
    });    
  }

  function broadcastEvent(event) {
    const eventString = JSON.stringify(event);

    if (publisher != null && redisConnected) {
      publisher.publish(EVENTS_CHANNEL, eventString);
    } else {
      log.warn("Redis not available, broadcasting to clients directly");
      onEventBroadcast(eventString);
    }
  }

  function onEventBroadcast(eventString) {
    if (!socket) {
      return;
    }

    if (socket == null) {
      return;
    }

    let numClients = 0;
    for (let client of socket.clients) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(eventString);
        numClients++;
      }
    }

    log.debug(`Broadcast event to ${numClients} client(s)`);
  }

  this.broadcastAlarmChange = function(alarmId, change) {
    log.debug(`Broadcasting alarm change: ${alarmId}`);
    const event = {
      type: EVENT_TYPE_ALARM,
      data: {
        id: alarmId,
        muted: change.muted,
        removed: change.removed
      }
    };
    broadcastEvent(event);
  }

  this.broadcastFahrplanChange = function(swarm) {
    log.debug(`Broadcasting fahrplan change in: ${swarm}`);
    const event = {
      type: EVENT_TYPE_FAHRPLAN,
      data: {
        swarm: swarm
      }
    };
    broadcastEvent(event);
  }

  this.broadcastDeviceChange = function(deviceId, newData, newInfo) {
    log.debug(`Broadcasting device change: ${deviceId}`);

    const event = {
      type: EVENT_TYPE_DEVICE,
      
      data: {
        id: deviceId,
        newData: newData,
        newInfo: newInfo
      }
    };
    broadcastEvent(event);
  }

  this.broadcastOperatorChange = function(swarms) {
    log.debug(`Broadcasting operator change in ${swarms.length} swarms`);

    const event = {
      type: EVENT_TYPE_OPERATOR,
      data: {
        swarms: swarms
      }
    };
    broadcastEvent(event);
  }
}

module.exports = new EventBroadcaster();
